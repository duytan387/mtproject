jQuery(function($) {

	var owl = $('.list-slide');
    owl.owlCarousel({
        responsiveClass:true,
        autoplay : true, 
        slideSpeed : 2000,
        animateOut: 'fadeOut',
        margin:0,
        dots: false,
        loop: true, 
        nav:true,
        navText:['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
        items:1
    });

    $('.list-feedback').owlCarousel({
        responsiveClass:true,
        autoplay : false, 
        slideSpeed : 2000,
        margin:0,
        dots: false,
        loop: false, 
        nav:true,
        navText:['<i class="arrow-left" aria-hidden="true"></i>','<i class="arrow-right" aria-hidden="true"></i>'],
        items:1
    });

   $('.group-news .list-news, .box-course .list-image').owlCarousel({
        responsiveClass:true,
        autoplay : false, 
        slideSpeed : 2000,
        animateOut: 'fadeOut',
        margin:0, 
        dots: true,
        loop: false, 
        nav:false,
        items:1 
    });

    var body = $('body');
    $('.menu-btn').click(function(){
        if(body.hasClass('show-main-menu')){
            body.removeClass('show-main-menu');
        }
        else{
            body.addClass('show-main-menu');
        }
    });

    $(".btn-register").fancybox({
            maxWidth    : 800,
            maxHeight   : 600,
            fitToView   : false,
            width       : '100%',
            height      : '100%',
            autoSize    : false,
            closeClick  : false,
            openEffect  : 'none',
            closeEffect : 'none'
        });

    var h = 0;
    $('.content h2.news-title').each(function(i, desc){
        if($(desc).height() > h) h = $(desc).height();
    })
    $('.content h2.news-title').height(h);

    $.fancybox.defaults.btnTpl.counter = '<div><span data-fancybox-index></span> / <span data-fancybox-count></span></div>';
    $('[data-fancybox="images"]').fancybox({
      toolbar  : false,
      loop    : true,
      arrows  : true,
      infobar : true,
      margin  : [44,0,22,0],
      thumbs : {
        autoStart : true,
        axis : 'x'
      },
      arrows   : true
    })

    var total = $('.fancybox-thumbs__list').width() + 120;
    $('.fancybox-navigation').width(total);

    var poppy = localStorage.getItem('myPopup');
    if(!poppy){
        function PopUp(hideOrshow) {
            if(hideOrshow == 'hide'){
                $('.popup').css('display','none');
            }else{
                $('.popup').css('display','block');
            }
        }

        $(window).on('load',function(){
            setTimeout(function(){
              PopUp('show');
           },5000);
        })

        $('.popup-close').click(function (event) {
            event.preventDefault();
            $('.popup').addClass('hide');
            $('.popup').hide();
        });
        localStorage.setItem('myPopup','true');
    }
    $('.form-register .form-group select').selectpicker();
});