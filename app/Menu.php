<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Menu extends Model {

    protected $table = 'menu';

    //
    public function getListMenu($id_cate) {
        return DB::table('menu')
                        ->join('categorymenu', 'menu.id_catemenu', '=', 'categorymenu.id')
                        ->select('menu.*', 'categorymenu.title_vn as category_vn', 'categorymenu.title_en as category_en')
                        ->get();
    }

    public function getListCate($id_catemenu) {
        return DB::table('menu')
                        ->join('categorymenu', 'menu.id_catemenu', '=', 'categorymenu.id')
                        ->where('id_catemenu', $id_catemenu)
                        ->select('menu.*', 'categorymenu.title_vn as category_vn', 'categorymenu.title_en as category_en')
                        ->get();
    }

    public function getCateMenu($id) {
        return DB::table('categorymenu')
                        ->where('id', $id)
                        ->first();
    }

    public function getCate() {
        return DB::table('categorymenu')
                        ->get();
    }

    public function getOneCate($id) {
        return DB::table('categorymenu')
                        ->where('id', $id)
                        ->first();
    }

    public function getMenu($id) {
        return DB::table('menu')
                        ->where('id', $id)
                        ->first();
    }

    public function updateLinhVuc($id, $title_vn, $link_vn, $id_catemenu) {
        return DB::table('menu')
                        ->where('id', $id)
                        ->update([
                            'title_vn' => $title_vn,
                            'id_catemenu' => $id_catemenu,
                            'link_vn' => $link_vn,
        ]);
    }

    public function listIDLinhVuc() {
        return DB::table('menu')->lists('id');
    }

    public function setMenuLinhVuc($title_vn, $link_vn, $id_catemenu) {
        return DB::table('menu')
                        ->insertGetId([
                            'title_vn' => $title_vn,
                            'id_catemenu' => $id_catemenu,
                            'link_vn' => $link_vn,
        ]);
    }

    public function doDelLinhVuc($id) {
        DB::table('menu')
                ->where('id', $id)
                ->delete();
    }
	
	public function delCateMenu($id) {
        DB::table('categorymenu')
                ->where('id', $id)
                ->delete();
    }

}
