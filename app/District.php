<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    //
    protected $table='district';
    protected $fillable=['name','_prefix','province_id'];
	public function province()
  {
      return $this->belongsTo('App\Province');
  }
  public function ward()
  {
      return $this->hasMany('App\Ward');
  }
}
