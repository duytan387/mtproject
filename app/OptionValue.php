<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OptionValue extends Model
{
    protected $table = "option_value";
    protected $fillable = ['option_id', 'image', 'sort_order', 'name'];


	public function option(){
    	return $this->belongsTo('App\Option','option_id','id');
    }

}
