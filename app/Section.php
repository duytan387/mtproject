<?php



namespace App;



use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;



class Section extends Model {
    
    protected $table = 'section';



    //

    public function getListSection() {

        return DB::table('section')->orderBy('created_at', 'desc')->get();

    }



    public function getSectionHotline($id) {

        return DB::table('section')->whereId($id)->first();

    }



    public function getOneSection($id) {

        return DB::table('section')->whereId($id)->first();

    }



    public function getChosenSection($ids) {

        return DB::table('section')->whereIn('id', $ids)->get();

    }



    public function getListId() {

        return DB::table('section')->lists('id');

    }



    public function updateSection($lang, $id, $name, $title, $description, $image, $video, $link) {

        if ($lang == "en") {

            return DB::table('section')

                            ->where('id', $id)

                            ->update([

                                'name' => $name,

                                'title_en' => $title,

                                'description_en' => $description,

                                'image' => $image,

                                'link' => $link,

                                'video' => $video

            ]);

        } else {

            return DB::table('section')

                            ->where('id', $id)

                            ->update([

                                'name' => $name,

                                'title_vn' => $title,

                                'description_vn' => $description,

                                'image' => $image,

                                'link' => $link,

                                'video' => $video

            ]);

        }

    }



    public function viewSection($id) {

        return DB::table('section')->whereId($id)->first();

    }

    public function getListSectionSlug($slug){
        return DB::table('section')->where('name',$slug)->get();
    }
}

