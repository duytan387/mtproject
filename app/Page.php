<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Page extends Model {

    //
    protected $table = 'page';

    public function adminListPage() {
        return DB::table("page")->select('*')->orderBy('id', 'desc')->paginate(10);
    }

    public function adminViewPage($id) {
        return DB::table("page")->whereId($id)->first();
    }

    public function getListId() {
        return DB::table('page')->lists('id');
    }

    public function getListCollection() {
        // lay danh sach catepost, ngoai tru cate delete
        return DB::table("collection")->select("id", "title_vn", "title_en")->get();
    }

    public function getListSlugVN() {
        return DB::table('page')->lists('slug_vn');
    }

    public function countPage() {
        return DB::table('page')->count();
    }

    public function addPage($title_en, $title_vn, $slug_vn, $slug_en, $description_en, $description_vn, $content_en, $content_vn, $image, $seotitle, $seokeyword, $seodescription, $created_at) {
        $lastId = DB::table('page')->orderBy('id', 'desc')->first();
        return DB::table('page')->insertGetId([
                    'title_vn' => $title_vn,
                    'title_en' => $title_en,
                    'slug_vn' => $slug_vn,
                    'slug_en' => $slug_en,
                    'description_vn' => $description_vn,
                    'description_en' => $description_en,
                    'content_vn' => $content_vn,
                    'content_en' => $content_en,
                    'image' => $image,
                    'seotitle' => $seotitle,
                    'seokeyword' => $seokeyword,
                    'seodescription' => $seodescription,
                    'created_at' => $created_at
        ]);
    }

    public function delPage($id) {
        return DB::table('page')->where('id', $id)->delete();
    }

    public function updatePage($id, $lang, $title_en, $title_vn, $slug_en, $slug_vn, $description_en, $description_vn, $content_vn, $content_en, $image, $seotitle, $seokeyword, $seodescription, $updated_at) {
        if ($lang == "vn") {
            return DB::table('page')
                            ->where('id', $id)
                            ->update([
                                'title_vn' => $title_vn,
                                'slug_vn' => $slug_vn,
                                'description_vn' => $description_vn,
                                'content_vn' => $content_vn,
                                'image' => $image,
                                'seotitle' => $seotitle,
                                'seokeyword' => $seokeyword,
                                'seodescription' => $seodescription,
                                'updated_at' => $updated_at
            ]);
        } else {
            return DB::table('page')
                            ->where('id', $id)
                            ->update([
                                'title_en' => $title_en,
                                'slug_en' => $slug_en,
                                'description_en' => $description_en,
                                'content_en' => $content_en,
                                'image' => $image,
                                'seotitle' => $seotitle,
                                'seokeyword' => $seokeyword,
                                'seodescription' => $seodescription,
                                'updated_at' => $updated_at
            ]);
        }
    }

    public function getId($slug) {
        return DB::table('page')->select('id')->where('slug_vn', $slug)->first();
    }

    public function checkSlug($slug) {
        return DB::table('page')->where('slug_vn', $slug)->first();
    }

    public function getPage($slug) {
        return DB::table("page")->where('slug_vn', $slug)->first();
    }

    public function getPost($slug) {
        return DB::table("post")->where('slug_vn', $slug)->first();
    }

    public function getCate($slug) {
        return DB::table('post')->select('id_cate')->where('slug_vn', $slug)->first();
    }

    public function getPostRelated($post_slug, $id_cate, $limit) {
        return DB::table("post")
                        ->where('slug_vn', '!=', $post_slug)
                        ->where('id_cate', $id_cate)
                        ->limit($limit)
                        ->get();
    }

    public function updateSlugPage($id, $slug_vn, $slug_en) {
        if (session('locale') == "vn") {
            return DB::table('page')->where('id', $id)->update([
                        'slug_vn' => $slug_vn
            ]);
        } else {
            return DB::table('page')->where('id', $id)->update([
                        'slug_en' => $slug_en
            ]);
        }
    }

    public function getSlug($id, $lang) {
        if ($lang == "vn") {
            return DB::table('page')->where('id', $id)->select('slug_vn')->first();
        } else {
            return DB::table('page')->where('id', $id)->select('slug_en')->first();
        }
    }

    public function sitemap() {
        return DB::table("page")
                        ->select('page.*')
                        ->orderby('created_at', 'ASC')
                        ->get();
    }
    
    public function getPageMetaTag($slug_vn = "/"){
        return DB::table("page")
                        ->where('slug_vn', $slug_vn)
                        ->first();
    }
    
    public function getPostMetaTag($slug_vn){
        return DB::table("post")
                        ->where('slug_vn', $slug_vn)
                        ->first();
    }

}
