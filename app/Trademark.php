<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trademark extends Model
{
    protected $table = "trademark";
    protected $fillable = ['name', 'description', 'image', 'created_at', 'updated_at'];

    public function product(){
    	return $this->hasMany('App\Products','id_trademark','id');
    }
	
	
}
