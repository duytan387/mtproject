<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Collection extends Model {

	//
    public function getListCollection() {
        return DB::table('collection')->get();
    }
    public function viewCollectionPost($id) {
        return DB::table('collection')->whereId($id)->first();
    }
    
    public function editCollectionPost($id, $lang, $title, $description, $updated_at) {
        if ($lang == "en") {
            return DB::table('collection')
                            ->where('id', $id)
                            ->update([
                                'title_en' => $title,
                                'description_en' => $description,
                                'updated_at' => $updated_at
            ]);
        } else {
            return DB::table('collection')
                            ->where('id', $id)
                            ->update([
                                'title_vn' => $title,
                                'description_vn' => $description,
                                'updated_at' => $updated_at
            ]);
        }
    }
    
    public function addCollectionPost($lang, $title, $description, $created_at) {
        if ($lang == "en") {
            return DB::table('collection')->insertGetId([
                        'title_en' => $title,
                        'title_vn' => $title . " vn",
                        'description_en' => $description,
                        'created_at' => $created_at
            ]);
        } else {
            return DB::table('collection')->insertGetId([
                        'title_vn' => $title,
                        'title_en' => $title . " en",
                        'description_vn' => $description,
                        'created_at' => $created_at
            ]);
        }
    }
    
    public function delCollectionPost($id) {
        return DB::table('collection')->where('id', $id)->delete();
    }

}
