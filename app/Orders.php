<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model {

	//
    protected $table = "orders";
    protected $fillable = ['customer_id', 'order_number', 'payment_method', 'total_price', 'discount_percent', 'subtotal', 'order_date', 'status', 'order_type', 'notes', 'duedate', 'created_at', 'updated_at'];
    public function bill_detail(){
    	return $this->hasMany('App\OrderDetail','id_bill','id');
    }

    public function customer(){
    	return $this->belongsTo('App\Customer','customer_id','id');
    }

}
