<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Customer extends Model {
    //
    protected $table = "customer";
    protected $fillable = ['name', 'email', 'phone', 'address', 'district', 'position', 'comment', 'image', 'created_at', 'updated_at'];
    
    public function orders(){
    	return $this->hasMany('App\Orders','customer_id','id');
    }
    
    
    
    public function product_type(){
    	return $this->belongsTo('App\ProductType','id_type','id');
    }

    public function bill_detail(){
    	return $this->hasMany('App\BillDetail','id_product','id');
    }
    
    public function getAdminListCustomer() {
        return DB::table("customer")->orderBy('created_at','DESC')->get();
    }

    

    public function getHomeList()
    {
        return DB::table("customer")->orderBy('orderby', 'desc')->get();
    }



    public function delCustomer($id) {
        return DB::table('customer')->whereId($id)->delete();
    }



    public function getListId() {
        return DB::table('customer')->lists('id');
    }
    
    public function listCustomer() {
        return DB::table("customer")->orderBy('created_at','DESC')->get();
    }



    public function viewCustomer($id) {
        return DB::table('customer')->whereId($id)->first();
    }



    public function updateCustomer($id, $name, $position, $comment, $image, $updated_at) {
        return DB::table('customer')
                        ->where('id', $id)
                        ->update([
                            'image' => $image,
                            'position' => $position,
                            'comment' => $comment,
                            'name' => $name,
                            'updated_at' => $updated_at
        ]);
    }



    public function addCustomer($name, $position, $comment, $image, $created_at) {
        return DB::table('customer')->insertGetId([
            'name' => $name,
            'position' => $position,
            'comment' => $comment,
            'image' => $image,
            'created_at' => $created_at
        ]);
    }
}

