<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductLanguage extends Model
{
    //
    protected $table='products_language';
    protected $fillable=['product_id','language_id','name','slug','description','content','unit'];
    public $timestamps=false;
}
