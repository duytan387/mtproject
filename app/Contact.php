<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Contact extends Model {

    //
    public function add($name, $email, $phone, $subject, $message, $currentdate) {
        return DB::table('contact')->insert([
                    'fullname' => $name,
                    'email' => $email,
                    'phone' => $phone,
                    'subject' => $subject,
                    'message' => $message,
                    'created_at' => $currentdate
        ]);
    }

    public function contactList() {
        return DB::table('contact')->orderby('created_at', 'desc')->get();
    }

    public function delContact($id) {
        return DB::table('contact')->where('id', $id)->delete();
    }

    public function countContactCurrent() {
        return DB::table('contact')
                        ->select(DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d") as currentdate'))
                        ->lists('currentdate');
//                ->toSql()
    }

    public function getContact($id) {
        return DB::table('contact')->whereId($id)->first();
    }
    
    public function getListId()
    {
        return DB::table('contact')->lists('id');
    }

    public function doUpdate($id, $fullname, $email, $subject, $message, $updated_at) {
        return DB::table('contact')
                        ->where('id', $id)
                        ->update([
                            'fullname' => $fullname,
                            'email' => $email,
                            'subject' => $subject,
                            'message' => $message,
                            'updated_at' => $updated_at
        ]);
    }

}
