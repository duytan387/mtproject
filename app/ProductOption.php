<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOption extends Model
{
    protected $table = "product_option";
    protected $fillable = ['product_id', 'option_id', 'value', 'required'];

    public function productOptionValue(){
    	return $this->hasMany('App\ProductOptionValue','product_option_id','id');
    }
	
	
}
