<?php



namespace App;



use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;



class Setting extends Model {
    //
    protected $table = 'setting';
    protected $fillable = ['name', 'config'];
    public function setMail() {
        return DB::table('setting')->whereId(1)->first();
    }
    
    public function getSetting($id){
        return DB::table('setting')->whereId($id)->first();
    }
    
    public function updateSocial($id, $facebook, $youtube, $googleplus, $twitter, $linkedin, $instagram){
        return DB::table('setting')
                        ->where('id', $id)
                        ->update(['config' => ''
                            . '{"facebook":"' . $facebook . '",'
                            . '"youtube":"' . $youtube . '",'
                            . '"googleplus":"' . $googleplus . '",'
                            . '"linkedin":"' . $linkedin . '",'
                            . '"instagram":"' . $instagram . '",'
                            . '"twitter":"' . $twitter . '"}'

        ]);
    }
    public function updateHeader($id, $logo, $hotline, $email){
        return DB::table('setting')
                        ->where('id', $id)
                        ->update(['config' => ''
                            . '{"logo":"' . $logo . '",'
                            . '"hotline":"' . $hotline . '",'
                            . '"email":"' . $email . '"}'
        ]);
    }
    
    public function updateFooter($id, $copyright, $development, $phone, $address, $link){
        return DB::table('setting')
                        ->where('id', $id)
                        ->update(['config' => ''
                            . '{"copyright":"' . $copyright . '",'
                            . '"development":"' . $development . '",'
                            . '"phone":"' . $phone . '",'
                            . '"address":"' . $address . '",'
                            . '"link":"' . $link . '"}'

        ]);
    }

    public function updateMail($name, $title, $from, $cc) {

        return DB::table('setting')

                        ->where('id', 1)

                        ->update(['config' => ''

                            . '{"name":"' . $name . '",'

                            . '"title":"' . $title . '",'

                            . '"from":"' . $from . '",'

                            . '"cc":"' . $cc . '"}'

        ]);

    }



    public function getMail() {

        return DB::table('setting')->whereId(1)->first();

    }



    public function getAds() {

        return DB::table('setting')->whereId(2)->first();

    }



    public function setAdvertising($name, $config) {

        return DB::table('setting')

                        ->where('id', 2)

                        ->update([

                            'name' => $name,

                            'config' => $config

        ]);

    }

    

    public function getAdvertising()

    {

        return DB::table('setting')->whereId(2)->first();

    }



}

