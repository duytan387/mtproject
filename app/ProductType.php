<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $table = "type_products";
    protected $fillable = ['name', 'slug', 'parent_id', 'description', 'image', 'created_at', 'updated_at'];

    public function product(){
    	return $this->hasMany('App\Products','id_type','id');
    }
	public function language()
	{
		return $this->hasMany('App\TypeProductsLanguage');
	    
	}
	public function getCategoryProductFromID($id){
		$data=$this->where('id',$id)->get();
		return $data;
	}
	public function category_tree($id,$title,&$results){
        
        $data = ProductType::where('parent_id',$id)->get();
             
        if($data){
            foreach($data as $item){ 
                if(!$title){
                    $title_sub=$item->name;
                }
                else{
                    $title_sub=$title.' -> '.$item->name;
                }
                
                  $results[]=[
                        'id'=>$item->id,
                        'name'=>$item->name,
                        'parent_id'=>$item->parent_id,
                        'nametree'=>$title_sub,
                        'description'=>$item->description,
                        'created_at'=>$item->created_at,
                        'updated_at'=>$item->updated_at,
                        ];
                 $this->category_tree($item->id,$title_sub,$results);
            }
        }
		
    }
}
