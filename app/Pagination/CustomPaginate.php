<?php

namespace App\Pagination;

use Illuminate\Pagination\BootstrapThreePresenter;
use Illuminate\Pagination\Paginator;

class CustomPaginate extends BootstrapThreePresenter {

    public function render() {
        if ($this->hasPages()) {
            return sprintf(
                    '<ul class="pagination">%s %s %s</ul>',
//                $this->getFirst(),
                    $this->getButtonPre(),
//                $this->getButton(),
                    $this->getLinks(),
                    $this->getButtonNext()
//                $this->getLast()
            );
        }
        return "";
    }

    public function getLast() {
        $url = $this->paginator->url($this->paginator->lastPage());
        $url = $this->customLink($url);
        $btnStatus = '';


        if ($this->paginator->lastPage() == $this->paginator->currentPage()) {
            $btnStatus = 'disabled';
        }

//        return $btn = "<li class=\"page-item\"><a class=\"page-link\" href='" . $url . "'>Last </a></li>";
        return $btn = "<li><a href='" . $url . "' aria-label=\"Next\"><i class=\"fa fa-angle-double-right\"></i></a></li>";
    }

    public function getLinks() {
        $html = '';
        
        if (is_array($this->window['first'])) {
            $html .= $this->getUrlLinks($this->window['first']);
        }

        if (is_array($this->window['slider'])) {
            $html .= $this->getDots();
            $html .= $this->getUrlLinks($this->window['slider']);
        }
        if (is_array($this->window['last'])) {
            $html .= $this->getDots();
            $html .= $this->getUrlLinks($this->window['last']);
        }
        $html = $this->customLink($html);
        return $html;
    }

    public function getFirst() {
        $url = $this->paginator->url(1);
        $url = $this->customLink($url);
        $btnStatus = '';


        if (1 == $this->paginator->currentPage()) {
            $btnStatus = 'disabled';
        }
//        return $btn = "<li class=\"page-item\"><a   class=\"page-link\" href='" . $url . "'>First</a></li>";
        return $btn = "<li><a href='" . $url . "' aria-label=\"Previous\"><i class=\"fa fa-angle-double-left\"></i></a></li>";
    }

    public function getButtonPre() {
        $url = $this->paginator->previousPageUrl();
        $url = $this->customLink($url);
        $btnStatus = '';


        if (empty($url)) {
            $btnStatus = 'disabled';
        }
//        return $btn = "<li class=\"page-item\"><a class=\"page-link\" href='" . $url . "'>Previous </a></li>";
        return $btn = "<li><a href='" . $url . "' aria-label=\"Previous\"><i class=\"fa fa-angle-double-left\"></i></a></li>";
    }

    public function getButtonNext() {
        $url = $this->paginator->nextPageUrl();
//        echo "<pre>";
//        print_r($this->paginator->appends('key','value'));
//        echo "</pre>";
//        exit;
        $url = $this->customLink($url);
//        echo "<pre>";
//        print_r($url);
//        echo "</pre>";
//        exit;
        $btnStatus = '';


        if (empty($url)) {
            $btnStatus = 'disabled';
        }
//        return $btn = " <li class=\"page-item\"><a class=\"page-link\"  href='" . $url . "'>Next </a></li>";
        return $btn = "<li><a href='" . $url . "' aria-label=\"Next\"><i class=\"fa fa-angle-double-right\"></i></a></li>";
    }

    public function getButton() {
        $page = $this->paginator->currentPage();
        $lastPage = $this->paginator->lastPage();
//        $btn = " <li class=\"page-item\"><input type=\"number\" class=\"input-page\" placeholder=\"".$page."\" min=\"0\"></li>";
//        $btn.=" <li class=\"page-item\"><span class=\"cl_gray page-link\">of ".$lastPage."</span></li>";
        $btn = "<li class=\"a\"><a href='" . $page . ">" . $page . "'</a></li>";
        return $btn;
    }

    public function customLink($link) {
        $url = str_replace('list/?', 'list?', $link);
        return $url;
    }

}
