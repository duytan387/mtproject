<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\Users;
use Session;

class AdminMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $auth;
    public function __construct(Users $users, Guard $auth) {
        $this->users = $users;
        $this->auth = $auth;
//        echo "<pre>";
//        print_r($this->users->getUserGroup(1));
//        echo "</pre>";
//        exit;
    }
    public function handle($request, Closure $next) {
        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('admin/login');
            }
        }
//        $user = $this->users->getUserGroup(1);
        
        if($this->auth->user()->group_id === 1){
//            echo "<pre>";
//            print_r($next($request));
//            echo "</pre>";
//            exit;
            if(!Session::has('roles'))
                Session::put('roles', 1);
            return $next($request);
        }
        else
            return redirect()->route("dashboard");
    }

}
