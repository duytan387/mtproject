<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Contact;
use App\Banner;
use Illuminate\Support\Facades\Session;
use DateTime;
use Mail;

class ContactController extends AppController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct(Banner $banner, Contact $contact) {
        $this->banner = $banner;
        $this->contact = $contact;
    }

    public function index() {
        //
        return view('pages.contact', [
            'bannerslide' => $this->banner->getBannerTop(1),
            'listscreen' => $this->screenshot->getHomeList(),
            'metapage' => $this->page->adminViewPage(11)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function old_create(Request $request) {
        if (!$request->isMethod('post') || !$request->isMethod('POST')) {
            return response()->json(['fail' => '<p>Lỗi! Vui lòng thử lại</p>']);
        }
        $email = $request->input('email');
        $name = $request->input('name');
        $phone = $request->input('phone');
        $subject = $request->input('subject');
        $message = $request->input('message');
//        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
//            return response()->json(['fail' => '<p>Email không đúng định dạng</p>']);
//        }

        //
//        echo "dsada";exit;
//        $validator = Validator::make($request->all(), [
//                    'name' => 'required|max:255',
//                    'email' => 'required|email|max:255',
//                    'phone' => 'required|max:12',
//                    'services' => 'required|max:255',
//                    'message' => 'required'
//        ]);
//
//        if ($validator->fails()) {
//            return response()->json(['success' => '<p>aaaaVui lòng thông tin</p>']);
//        }
        $currentdate = date("Y-m-d h:i:s");
        $this->contact->add($name, $email, $phone, $subject, $message, $currentdate);
        return response()->json(['success' => '<p>Cảm ơn bạn đã liên hệ! Chúng tôi sẽ phản hồi trong thời gian sớm nhất.</p>']);
        // setting email
//        $mail = new Setting();        $sendmail = $mail->getMail();
//        $config = json_decode($sendmail->config);				/*echo "<br>";		print_r($config);		echo "<br>";		echo $config->cc;		echo $config->from;		if(empty($config->cc))		{			echo "dsada";		}		exit;*/
        // 
//        $data = array(
//            'name' => $name,
//            'email' => $email,
//            'title' => $config->title,
//            'from' => $config->from,
//            'cc' => $config->cc,
//            'sender' => $config->name
//        );
//
//        Mail::send('emails.contact', $data, function($message) use ($data){
//            $message->from($data['from'], $data['sender']);
//            $message->to($data['email'], $data['name']);			if(!empty($data['cc']))			{				$message->cc($data['cc']);			}			
//           $message->subject($data['title']);
//        });
//        Session::flash('form', 'Đã nhận được thông tin. Chúng tôi sẽ liên hệ bạn trong thời gian sớm.');
//        return redirect('/lien-he');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function create(Request $request) {
        //
        if (!$request->isMethod('post') || !$request->isMethod('POST')) {
            return response()->json(['result' => '<p>Lỗi! Vui lòng thử lại</p>']);
        }
        $rules = [
            'name' => 'required|max:255',
            'phone' => 'required|numeric|phone_count',
            'message' => 'required|max:255',
            'email' => 'required|email'
        ];
        $messages = [
            'name.required' => 'Vui lòng nhập Họ Tên',
            'name.max' => 'Họ Tên quá dài',
            'phone.required' => 'Vui lòng nhập số điện thoại',
            'phone.phone_count' => 'Số điện thoại không đúng',
            'phone.numeric' => 'Vui lòng nhập chính xác số điện thoại',
            'message.required' => 'Nội dung liên hệ của bạn là gì?',
            'email.required' => 'Vui lòng nhập email',
            'email.email' => 'Email không đúng định dạng'
        ];
        // them validate phone_count
        Validator::extend('phone_count', function($attribute, $value, $parameters) {
            return strlen($value) >= 10 && strlen($value) < 11;
        });

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
//            return redirect()->back()->withErrors($validator);
            return response()->json(['result' => $validator->messages(), 'hoten' => $request->input('name'), 'phone' => $request->input('phone'), 'message' => $request->input('message'), 'email' => $request->input('email'), 'subject' => $request->input('subject')]);
        }
//        $email = $request->input('email');
//        if (empty($request->input('name')) || !is_numeric($request->input('phone')) || empty($request->input('course'))) {
//            return response()->json(['result' => '<p>Vui lòng điền đầy đủ thông tin</p>', 'status' => '']);
//        }
        else {
            $name = $request->input('name');
            $phone = $request->input('phone');
            $message = $request->input('message');
            $subject = $request->input('subject');
            $email = $request->input('email');
            $currentdate = date("Y-m-d h:i:s");
            $this->contact->add($name, $email, $phone, $subject, $message, $currentdate);
            // Neu co status khac rong thi dang ky thanh cong
            return response()->json(['result' => '<p>Cảm ơn bạn đã liên hệ! Chúng tôi sẽ phản hồi trong thời gian sớm nhất.</p>', 'hoten' => '', 'phone' => '', 'message' => '']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

}
