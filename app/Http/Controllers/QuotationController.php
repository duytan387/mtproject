<?php



namespace App\Http\Controllers;



use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\Banner;

use App\Screenshot;

use App\Post;

use App\Page;

use Illuminate\Http\Request;



class QuotationController extends AppController {



    /**

     * Display a listing of the resource.

     *

     * @return Response

     */

    public function __construct(Page $page, Post $post, Banner $banner, Screenshot $screenshot) {

        $this->banner = $banner;

        $this->post = $post;

        $this->screenshot = $screenshot;
        $this->page = $page;

    }



    public function index() {

        //

        return view('pages.quotation', [

            'bannerslide' => $this->banner->getBannerTop(1),

            'listscreen' => $this->screenshot->getHomeList(),

            'listproject' => $this->post->getProjectCate(7),

            'getbanner' => $this->banner->getBanner(4),

            'quotation' => $this->post->getPost(47),

            'metapage' => $this->page->adminViewPage(10)

        ]);

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return Response

     */

    public function create() {

        //

    }



    /**

     * Store a newly created resource in storage.

     *

     * @return Response

     */

    public function store() {

        //

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return Response

     */

    public function show($id) {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return Response

     */

    public function edit($id) {

        //

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  int  $id

     * @return Response

     */

    public function update($id) {

        //

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return Response

     */

    public function destroy($id) {

        //

    }



}

