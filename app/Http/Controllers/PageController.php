<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Screenshot;
use App\Post;
use App\Page;
use App\Section;
use App\Gallery;
use App\Customer;
use App\ProductType;
use Session;
use Illuminate\Support\Facades\Route;
use Illuminate\Pagination\Paginator;

class PageController extends AppController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct(Page $page, Post $post, Banner $banner, Screenshot $screenshot, Section $section, Gallery $gallery, Customer $customer) {
        parent::__construct();
        $this->banner = $banner;
        $this->post = $post;
        $this->page = $page;
        $this->screenshot = $screenshot;
        $this->section = $section;
        $this->gallery = $gallery;
        $this->customer = $customer;
    }

    public function index($slug) {
        //
        switch ($slug) {
            case 've-chung-toi':
                return view("pages.about", [
                    'bannerslide' => $this->banner->getBannerTop(1),
                    'getpage' => $this->page->getPage($slug),
                    'getmember' => $this->section->getOneSection(17),
                    'getpopup' => $this->banner->getBanner(3),
                    'getmeta' => $this->page->getPageMetaTag($slug)
                ]);
                break;
            case 'thu-ngo':
                return view("pages.about", [
                    'bannerslide' => $this->banner->getBannerTop(1),
                    'getpage' => $this->page->getPage($slug),
                    'getmember' => $this->section->getOneSection(17),
                    'getpopup' => $this->banner->getBanner(3),
                    'getmeta' => $this->page->getPageMetaTag($slug)
                ]);
                break;
            case 'gallery-khach-hang':
                return view("pages.gallerykhachhang", [
                    'bannerslide' => $this->banner->getBannerTop(1),
                    'getgallery' => $this->gallery->listGalleryFE(100),
                    'getpage' => $this->page->getPage($slug),
                    'getmeta' => $this->page->getPageMetaTag($slug),
                    'getpopup' => $this->banner->getBanner(3),
                    'cateslug' => 'du-an'
                ]);
                break;
            case 'dich-vu':
                return view("pages.services", [
                    'bannerslide' => $this->banner->getBannerTop(1),
                    'listservice' => $this->post->listOneCatePost("dich-vu", 0),
                    'getpage' => $this->page->getPage($slug),
                    'getgallery' => $this->gallery->listGalleryFE(10),
                    'listcustomer' => $this->customer->listCustomer(),
                    'getpostfirst' => $this->post->postFirst("dich-vu"),
                    'getpostfeature' => $this->post->postFeature("dich-vu",3),
                    'getpopup' => $this->banner->getBanner(3),
                    'getmeta' => $this->page->getPageMetaTag($slug)
                ]);
                break;
            case 'tin-tuc':
                return view("pages.news", [
                    'bannerslide' => $this->banner->getBannerTop(1),
                    'listnews' => $this->post->listOneCatePost("tin-tuc", 9),
                    'getpage' => $this->page->getPage($slug),
                    'getmeta' => $this->page->getPageMetaTag($slug),
                    'cateslug' => 'tin-tuc'
                ]);
                break;
            case 'tuyen-dung':
                return view("pages.career", [
                    'bannerslide' => $this->banner->getBannerTop(1),
                    'listcareer' => $this->post->listOneCatePost("tuyen-dung", 9),
                    'getpage' => $this->page->getPage($slug),
                    'getmeta' => $this->page->getPageMetaTag($slug),
                    'cateslug' => 'tuyen-dung'
                ]);
                break;
            case 'lien-he':
                return view("pages.contact", [
                    'bannerslide' => $this->banner->getBannerTop(1),
                    'getpage' => $this->page->getPage($slug),
                    'getmeta' => $this->page->getPageMetaTag($slug),
                ]);
                break;
            case 'banner':
                return view("layouts.banner", [
                    'bannerslide' => $this->banner->getBannerTop(1),
//                    'getpage' => $this->page->getPage($slug),
//                    'getmeta' => $this->page->getPageMetaTag($slug),
//                    'getlistservice' => $this->post->getContactListService('dich-vu')
                ]);
                break;
            case 'bang-gia-banh-trung-thu-kinh-do':
                $listCate = ProductType::all();
                return view("pages.quotation", [
                    'bannerslide' => $this->banner->getBannerTop(1),
                    'getpage' => $this->page->getPage($slug),
                    'getmeta' => $this->page->getPageMetaTag($slug),
                    'listCate' => $listCate
                ]);
                break;
			case 'chiet-khau-banh-trung-thu-kinh-do-2019':
                $listCate = ProductType::all();
                return view("pages.quotation", [
                    'bannerslide' => $this->banner->getBannerTop(1),
                    'getpage' => $this->page->getPage($slug),
                    'getmeta' => $this->page->getPageMetaTag($slug),
                    'listCate' => $listCate
                ]);
                break;
            case 'sitemap.xml':
                $posts = $this->post->sitemap();
                $pages = $this->page->sitemap();
                return response()->view('vendor.sitemap', compact('posts','pages'))->header('Content-Type', 'text/xml');
                break;
            default :
                return abort(404);
                break;
        }
    }

    /**

     * Show the form for creating a new resource.

     *

     * @return Response

     */
    public function detail($slug) {
        if(!$this->post->getPost($slug)){
            return abort(404);
        }
        $post = $this->post->getPost($slug);
//        echo $post->count_views;exit;
        $key = 'myblog' . $post->id;
//        echo Session::has($key);exit;
        if (!Session::has($key)) {
            $count_views = $post->count_views + 1;
            $this->post->setView($post->id, $count_views);
            Session::put($key, 1);
        }
//        Session::forget($key);

        $getcate = $this->post->getCate($slug);
        
//        
        return view('pages.' . Route::currentRouteName(), [
            'bannerslide' => $this->banner->getBannerTop(1),
            'getpost' => $post,
            'getmeta' => $this->post->getPostMetaTag($slug),
            'getpopup' => $this->banner->getBanner(3),
            'getpostrelated' => $this->post->getPostRelated($slug, $getcate->id_cate, 3),
            'cate_title' => $getcate->catetitle_vn
        ]);
    }

    /**

     * Store a newly created resource in storage.

     *

     * @return Response

     */
    public function store() {

        //
    }

    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return Response

     */
    public function show($id) {

        //
    }

    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return Response

     */
    public function edit($id) {

        //
    }

    /**

     * Update the specified resource in storage.

     *

     * @param  int  $id

     * @return Response

     */
    public function update($id) {

        //
    }

    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return Response

     */
    public function destroy($id) {

        //
    }

}
