<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Products;
use App\ProductType;
use App\Page;
use App\Cart;
use App\Banner;
//use App\Setting;
use Session;

class ProductController extends AppController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct(Products $product, Page $page, Banner $banner) {
        parent::__construct();
        $this->product = $product;
        $this->page = $page;
        $this->banner = $banner;
    }

    public function index(Request $request) {
        //


        if ($request->input('sort_by') === 'all' || $request->input('sort_by') === 'title-ascending') {
            $listProduct = Products::with('product_type')->orderBy('name', 'asc')->get()->toArray();
        } elseif ($request->input('sort_by') === 'title-descending') {
            $listProduct = Products::with('product_type')->orderBy('name', 'asc')->get()->toArray();
        } elseif ($request->input('sort_by') === 'price-ascending') {
            $listProduct = Products::with('product_type')->orderBy('unit_price', 'asc')->get()->toArray();
        } elseif ($request->input('sort_by') === 'price-descending') {
            $listProduct = Products::with('product_type')->orderBy('unit_price', 'desc')->get()->toArray();
        } elseif ($request->input('sort_by') === 'created-ascending') {
            $listProduct = Products::with('product_type')->orderBy('created_at', 'asc')->get()->toArray();
        } elseif ($request->input('sort_by') === 'created-descending') {
            $listProduct = Products::with('product_type')->orderBy('created_at', 'desc')->get()->toArray();
        } else {
            $listProduct = Products::with('product_type')->orderBy('unit_price', 'desc')->get()->toArray();
        }
//        echo "<pre>";
//        print_r($listProduct);
//        echo "</pre>";
//        exit;

        $category = ProductType::all();

        return view('product.list', [
            'bannerslide' => $this->banner->getBannerTop(1),
            'getPage' => $this->page->getPage("banh-trung-thu-kinh-do"),
            'listProduct' => $listProduct,
            'category' => $category,
            'getmeta' => $this->page->getPageMetaTag()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($cate_slug, $slug) {
        //
        $viewItem = Products::where('slug', $slug)->firstOrFail()->toArray();
        $listCate = ProductType::all();
        $relatedItem = Products::where('slug', '!=', $slug)->take(4)->get()->toArray();
        return view('product.view', [
            'viewItem' => $viewItem,
            'listCate' => $listCate,
            'relatedItem' => $relatedItem
        ]);
    }

    public function showCate(Request $request, $slug) {
//        exit();
        $categoryProduct = ProductType::where('slug', $slug)->firstOrFail()->toArray();
//        echo "<pre>";
//        print_r($categoryProduct);
//        echo "</pre>";
//        exit;
        $listCate = ProductType::all();
        if ($request->input('sort_by') === 'all' || $request->input('sort_by') === 'title-ascending') {
            $listProduct = Products::where('id_type', $categoryProduct['id'])->orderBy('name', 'asc')->get()->toArray();
        } elseif ($request->input('sort_by') === 'title-descending') {
            $listProduct = Products::where('id_type', $categoryProduct['id'])->orderBy('name', 'asc')->get()->toArray();
        } elseif ($request->input('sort_by') === 'price-ascending') {
            $listProduct = Products::where('id_type', $categoryProduct['id'])->orderBy('unit_price', 'asc')->get()->toArray();
        } elseif ($request->input('sort_by') === 'price-descending') {
            $listProduct = Products::where('id_type', $categoryProduct['id'])->orderBy('unit_price', 'desc')->get()->toArray();
        } elseif ($request->input('sort_by') === 'created-ascending') {
            $listProduct = Products::where('id_type', $categoryProduct['id'])->orderBy('created_at', 'asc')->get()->toArray();
        } elseif ($request->input('sort_by') === 'created-descending') {
            $listProduct = Products::where('id_type', $categoryProduct['id'])->orderBy('created_at', 'desc')->get()->toArray();
        } else {
            $listProduct = Products::where('id_type', $categoryProduct['id'])->orderBy('created_at', 'desc')->get()->toArray();
        }

//        $listProduct = Products::where('id_type',$categoryProduct['id'])->orderBy('created_at', 'desc')->get()->toArray();
        return view('product.listcategory', [
            'listProduct' => $listProduct,
            'categoryProduct' => $categoryProduct,
            'listCate' => $listCate
        ]);
    }

    public function addToCart(Request $req, $id) {
        $product = Products::find($id);
        $oldCart = Session('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($product, $id);
        $req->session()->put('cart', $cart);
        return redirect()->back();
    }

    public function getDelItemCart($id) {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);
        if (count($cart->items) > 0) {
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }
        return redirect()->back();
    }
    
    public function updateItemCart($id) {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->updateCart($id);
        if (count($cart->items) > 0) {
            Session::put('cart', $cart);
        } else {
            Session::forget('cart');
        }
        return redirect()->back();
    }
    
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

}
