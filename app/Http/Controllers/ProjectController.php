<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Banner;
use App\Screenshot;
use App\Post;
use App\Page;
use App\Section;
use App\Collection;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProjectController extends AppController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct(Page $page, Post $post, Banner $banner, Screenshot $screenshot, Section $section) {
        $this->banner = $banner;
        $this->post = $post;
        $this->screenshot = $screenshot;
        $this->section = $section;
        $this->page = $page;
    }

    public function index(Request $request) {
        //

//        if ($request->ajax()) {
//            return response()->json(view('layouts.postajax', ['listproject' => $this->post->listPost(7, 12)])->render());
//        }
        return view('pages.project', [
            'bannerslide' => $this->banner->getBannerTop(1),
            'listscreen' => $this->screenshot->getHomeList(),
            'listproject' => $this->post->listPost('du-an'),
            'sectionproject' => $this->section->getOneSection(8),
			'cateslug' => 'du-an',
            'metapage' => $this->page->adminViewPage(9)
        ]);
    }
    
    public function projectListCate($cateslug)
    {
//        echo $cateslug;exit;
//        if ($request->ajax()) {
//            return response()->json(view('layouts.postajax', ['listproject' => $this->post->listPostProjectCate(7, $cateslug, 1)])->render());
//        }
        return view('pages.project', [
            'bannerslide' => $this->banner->getBannerTop(1),
            'listscreen' => $this->screenshot->getHomeList(),
            'listproject' => $this->post->listPostProjectCate($cateslug, 12),
            'sectionproject' => $this->section->getOneSection(8),
			'cateslug' => $cateslug,
            'metapage' => $this->page->adminViewPage(9)
        ]);
    }

    public function projectDetail($slug) {
        //
        $id_collection = $this->post->getCollection($slug);
        
//        $this->post->listCollectionProject($slug,$id_collection);
        
        
        return view('pages.projectdetail', [
            'bannerslide' => $this->banner->getBannerTop(1),
            'listscreen' => $this->screenshot->getHomeList(),
            'listproject' => $this->post->getProjectCate(7),
            'getbanner' => $this->banner->getBanner(2),
            'getprojectcollection' => $this->post->listCollectionProject($slug, $id_collection->id_collection),
            'getprojectdetail' => $this->post->getPostSlug($slug),

        ]);
        
    }
    
   

    

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

}
