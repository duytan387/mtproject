<?php
namespace App\Http\Controllers;
use App\Banner;
use App\Section;
use App\Setting;
use App\Post;
use App\Page;
use App\Products;
use App\ProductType;

class HomeController extends AppController {
    /*
      |--------------------------------------------------------------------------
      | Home Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */
    /**
     * Create a new controller instance.
     * @return void
     */

    public function __construct(Post $post, Banner $banner, Section $section, Page $page, Setting $setting) {
        parent::__construct();
        $this->banner = $banner;
        $this->section = $section;
        $this->post = $post;
        $this->page = $page;
        $this->setting = $setting;
    }

    public function index() {
//        echo "dsada";exit;
        $listProduct = Products::with('product_type')->where('neew',1)->orderBy('unit_price', 'desc')->get()->toArray();
        return view('pages/home',[
            'bannerslide' => $this->banner->getBannerTop(1),
            'gethome' => $this->page->getPage("/"),
            'getabout' => $this->section->getOneSection(3),
            'trackads' => $this->getTrack(),
            'listProduct' => $listProduct,
            'firstnews' => $this->post->getPostFeature("tin-tuc"),
            'listnews' => $this->post->listCategoryPost("tin-tuc"),
            'getmeta' => $this->page->getPageMetaTag()
        ]);
    }

    public function getBanner($id_banner)
    {
        $viewbanner = new Banner();
        return $viewbanner->getBanner($id_banner);
    }
    
    public function getServices()
    {
        $section = new Section();
        return $section->getSectionServices();
    }

    public function getTrack()
    {
        $track = new Setting();
        return $track->getAdvertising();
    }
    
    public function setNewsletter(Request $request){
        
    }
    
    public function getSource(){
//        return view('layouts.banner',[
//            "aa" => "dsadasdsa"
//        ]);
//        return response()->json(['success' => '<p>Cảm ơn bạn đã liên hệ! Chúng tôi sẽ phản hồi trong thời gian sớm nhất.</p>']);
        return response()->view('layouts.banner',[
            'bannerslide' => $this->banner->getBannerTop(1)
        ])->header('Content-Type', 'text/plain');
    }
}

