<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Setting;
use App\Categorypost;
use Session;
use App\Cart;
use App\Section;
use Illuminate\View\View;

class AppController extends Controller {

    public function __construct() {
        $mainmenu = Menu::where('id_catemenu', 3)->orderby('orderby', 'asc')->get();
        $menudichvu = Menu::where('id_catemenu', 1)->get();
        $menuquicklink = Menu::where('id_catemenu', 2)->get();
        $services = Categorypost::where(['categorypost.slug_vn' => 'dich-vu'])->join('post', 'categorypost.id', '=', 'post.id_cate')->select('post.title_vn', 'post.slug_vn', 'post.description_vn', 'post.image')->orderby('post.orderby', 'asc')->get();
        $header = Setting::where('id', 4)->firstOrFail()->toArray();
        $social = Setting::where('id', 3)->firstOrFail()->toArray();
        $footer = Setting::where('id', 5)->firstOrFail()->toArray();
        $tracking = Setting::where('id', 2)->firstOrFail()->toArray();
        if (Session('cart')) {
            $oldCart = Session::get('cart');
            $cart = new Cart($oldCart);
            view()->share(['cart' => Session::get('cart'), 'product_cart' => $cart->items, 'totalPrice' => $cart->totalPrice, 'totalQty' => $cart->totalQty]);
        }
        view()->share('menudichvu', $menudichvu);
        view()->share('menuquicklink', $menuquicklink);
        view()->share('services', $services);
        view()->share('mainmenu', $mainmenu);
        view()->share('header', $header);
        view()->share('social', $social);
        view()->share('footer', $footer);
        view()->share('tracking', $tracking);
    }

}

?>