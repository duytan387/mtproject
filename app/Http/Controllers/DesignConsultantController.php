<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Banner;
use App\Screenshot;
use App\Page;

class DesignConsultantController extends AppController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	 public function __construct(Banner $banner, Screenshot $screenshot, Page $page) {
        $this->banner = $banner;
        $this->screenshot = $screenshot;
        $this->page = $page;
    }

	public function index($slug)
	{
		if($this->page->checkSlug($slug) == null){
			return redirect('/');
		}
		$id = $this->page->getID($slug);
		return view('pages.designconsultant', [
            'bannerslide' => $this->banner->getBannerTop(1),
            'listscreen' => $this->screenshot->getHomeList(),
            'metapage' => $this->page->adminViewPage($id->id),
        ]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
