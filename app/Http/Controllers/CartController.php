<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Session;
use App\Customer;
use App\Orders;
use App\OrderDetail;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CartController extends AppController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        //
        if (Session::has('cart')) {
            return view('pages.cart');
        }
        return redirect()->route('sanpham');
    }

    public function getCheckout() {
        if (Session::has('cart')) {
            return view('pages.checkout');
        }
        return redirect()->route('sanpham');
    }

    public function setPayment(Request $request) {
        if (!$request->isMethod('post') || !$request->isMethod('POST')) {
            return response()->json(['result' => '<p>Lỗi! Vui lòng thử lại</p>']);
        }
        if (!Session::has('cart')) {
            return redirect('/');
        }
        $input = array();
        $input_order = array();
        $cart = Session::get('cart');
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email',
            'phone' => 'required|numeric|phone_count',
            'address' => 'required|max:255',
            'district' => 'required',
            'status' => 'required|max:100'
        ];
        $messages = [
            'name.required' => 'Vui lòng nhập Họ Tên',
            'name.max' => 'Họ Tên quá dài',
            'email.required' => 'Vui lòng nhập Email',
            'email.email' => 'Không đúng định dạng Email',
            'phone.required' => 'Vui lòng nhập số Điện thoại',
            'phone.phone_count' => 'Số điện thoại không đúng',
            'address.required' => 'Vui lòng nhập địa chỉ',
            'address.max' => 'Địa chỉ quá dài',
            'district.required' => 'Vui lòng chọn Quận',
            'status.required' => 'Lỗi! Vui lòng thử lại',
            'status.max' => 'Lỗi! Vui lòng thử lạis'
        ];
        // them validate phone_count
        Validator::extend('phone_count', function($attribute, $value, $parameters) {
            return strlen($value) >= 10 && strlen($value) < 11;
        });

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
//            return redirect()->back()->withErrors($validator);
            return response()->json(['result' => $validator->messages(), 'name' => $request->input('name'), 'phone' => $request->input('phone'), 'email' => $request->input('email'), 'district' => $request->input('district'), 'address' => $request->input('address'), 'message' => $request->input('message')]);
        } else {

            $input["name"] = $request->input('name');
            $input["email"] = $request->input('email');
            $input["phone"] = $request->input('phone');
            $input["address"] = $request->input('address');
            $input["district"] = $request->input('district');
            $input["comment"] = $request->input('message');
            $input["created_at"] = date("Y-m-d H:i:s");
            $result = Customer::create($input);

            $input_order['customer_id'] = $result->id;
            $input_order['order_number'] = "";
            $input_order['payment_method'] = $request->input('payment_method');
            $input_order['total_price'] = $cart->totalPrice;
            $input_order['order_date'] = date("Y-m-d H:i:s");
            $input_order['notes'] = $request->input('message');
            $input_order['created_at'] = date("Y-m-d H:i:s");
            $input_order['status'] = $request->input('status');
            $result_order = Orders::create($input_order);
            
            

            foreach ($cart->items as $key => $value) {
                $orderDetail = new OrderDetail;
                $orderDetail->order_id = $result_order->id;
                $orderDetail->product_id = $key;
                $orderDetail->quantity = $value['qty'];
                $orderDetail->unit_price = ($value['price'] / $value['qty']);
                $orderDetail->created_at = date("Y-m-d H:i:s");
                $orderDetail->save();
            }
            Session::forget('cart');

            if ($result && $result_order) {
                return response()->json(['result' => '<p>Đặt hàng thành công!</p>', 'name' => '', 'phone' => '', 'email' => '', 'address' => '', 'district' => '']);
            } else {
                return response()->json(['result' => '<p>Lỗi! Vui lòng thử lại.</p>', 'name' => $request->input('name'), 'phone' => $request->input('phone'), 'email' => $request->input('email'), 'district' => $request->input('district'), 'message' => $request->input('message')]);
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function orderSuccess() {
        //
        return view('product.notify',[
            
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

}
