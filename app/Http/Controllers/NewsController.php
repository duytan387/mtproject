<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Banner;
use App\Screenshot;
use App\Post;
use App\Page;
use App\Section;
use App\Tag;
use Illuminate\Http\Request;

class NewsController extends AppController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct(Page $page,Post $post, Banner $banner, Screenshot $screenshot, Section $section, Tag $tag) {
        $this->banner = $banner;
        $this->post = $post;
        $this->screenshot = $screenshot;
        $this->section = $section;
        $this->tag = $tag;
        $this->page = $page;
    }

    public function index() {
        //
        return view('pages.news', [
            'bannerslide' => $this->banner->getBannerTop(1),
            'listscreen' => $this->screenshot->getHomeList(),
            'listnews' => $this->post->listPost01(3, 12),
            'newsfeature' => $this->post->listPostNewsFeature(),
            'sectionnews' => $this->section->getOneSection(7),
            'listtag' => $this->tag->listTag(),
            'metapage'=>$this->page->adminViewPage(5)
        ]);
    }
    
    public function newsDetail($slug, $id = null) {
        //
//        echo $slug;
//        echo "<br>".$id;
//        exit;
        return view('pages.newsdetail', [
            'bannerslide' => $this->banner->getBannerTop(1),
            'listscreen' => $this->screenshot->getHomeList(),
            'listproject' => $this->post->getProjectCate(7),
            'newsfeature' => $this->post->listPostNewsFeature(),
            'getbanner' => $this->banner->getBanner(4),
            'newsdetail' => $this->post->getPostSlug($slug, $id),
            'listtag' => $this->tag->listTag()
        ]);
    }
    
    public function newsCareer(){
        return view('pages.news', [
            'bannerslide' => $this->banner->getBannerTop(1),
            'listscreen' => $this->screenshot->getHomeList(),
            'listnews' => $this->post->listPost01(2, 12),
            'newsfeature' => $this->post->listPostNewsFeature(),
            'sectionnews' => $this->section->getOneSection(10),
            'listtag' => $this->tag->listTag(),
             'metapage'=>$this->page->adminViewPage(5)
        ]);
    }
    
    public function newsOther(){
        return view('pages.news', [
            'bannerslide' => $this->banner->getBannerTop(1),
            'listscreen' => $this->screenshot->getHomeList(),
            'listnews' => $this->post->listPost01(17, 12),
            'newsfeature' => $this->post->listPostNewsFeature(),
            'sectionnews' => $this->section->getOneSection(9),
            'listtag' => $this->tag->listTag(),
            'metapage'=>$this->page->adminViewPage(5)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

}
