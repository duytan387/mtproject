<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Video;
use Session;
use App\Users;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class VideoController extends AppController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
        if (!is_int($id) || $id <= 0) {
            $id = (int) $id;
        }
        
        $video = new Video();
        //  lay danh sach id

        $listid = $video->getListId();
        // kiem tra id co ton tai trong table

        if (!in_array($id, $listid, true)) {
            return redirect("admin/dashboard");
        }
        return view('admin/video/view', [
            'viewvideo' => $this->viewVideo($id)
        ]);
    }
    
    public function viewVideo($id) {
        $video = new Video();
        return $video->viewVideo($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Request $request, $id) {
        //
        if (!is_int($id) || $id <= 0)
            $id = (int) $id;
        $rules = [
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'url' => 'required|max:255',
            'path' => 'required|max:255'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        $title = $request->input('title');
        $path = $request->input('path');
        $description = $request->input('description');

        if ($request->hasFile('background')) {
            $background = time() . "-" . $request->file('background')->getClientOriginalName();
            $destinationPath = public_path() . '/images/upload/video';
            $request->file('background')->move($destinationPath, $background);
        } else {
            $background = $request->input('url');
        }
        $updated_at = date("Y-m-d H:i:s");

        // neu loi xuat ra "The title may not be greater than 255 characters."
        $update = new Video();
        $update->updateVideo($id, $title, $description, $path, $background, $updated_at);

        Session::flash('status', 'Cập nhật thành công.');
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

}
