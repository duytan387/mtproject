<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Products;
use App\ProductType;
use App\Trademark;
use App\OrderDetail;
use App\Option;
use App\OptionValue;
use App\ProductOption;
use App\ProductOptionValue;
use App\TypeProductsLanguage;
use App\ProductLanguage;
use App\OptionLanguage;
use App\OptionValueLanguage;
use App\TrademarkLanguage;
use Session;
use Illuminate\Http\Request;

class ProductController extends Controller {

	protected $model;
    public function __construct() {
        $this->model=new ProductType();
	}
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request) {
        //
		$listProductType = ProductType::all();
		if ($request->has('id_type')) {
			if($request->input('id_type') > 0){
				$listProduct = Products::with('product_type')->with('trademark')->where('id_type', $request->input('id_type'))->orderBy('name', 'asc')->get()->toArray();
			}else{
				$listProduct = Products::with('product_type')->with('trademark')->orderBy('created_at', 'desc')->get()->toArray();
			}
        } else {
            $listProduct = Products::with('product_type')->with('trademark')->orderBy('created_at', 'desc')->get()->toArray();
        }
        // $listProduct = Products::with('product_type')->with('trademark')->orderBy('created_at', 'desc')->get()->toArray();
       // echo "<pre>";
       // print_r($listProduct);
       // echo "</pre>";
       // exit;
		$this->model=new ProductType();
        return view('admin.product.list', [
            'listProduct' => $listProduct,
			'listProductType' => $listProductType
        ]);
    }
	
	public function search(Request $request) {
        //
		if (!$request->isMethod('POST') || !$request->isMethod('post')) {
            Session::flash('status', 'Error! Try agains');
            return redirect()->back();
        }
        if(!$request->has('keyword')){
			return redirect()->route('listproduct');
		}
		#echo $request->input('keyword');exit;
		$listProductType = ProductType::all();
		$listProduct = Products::with('product_type')->with('trademark')->where('name', 'LIKE', '%'.$request->input('keyword').'%')->get()->toArray();
		return view('admin.product.list',[
			'listProduct' => $listProduct,
			'listProductType' => $listProductType,
			'keyword' => $request->input('keyword')
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
        $listProductType = ProductType::all();
        $listTrademark = Trademark::all();
		$categories=[];
		$producttype = new ProductType();
        $producttype->category_tree(0,'',$categories);
        return view('admin.product.add', [
            'categories' => $categories,
            'listProductType' => $listProductType,
            'listTrademark' => $listTrademark
        ]);
    }

    public function doAdd(Request $request) {
        if (!$request->isMethod('POST') || !$request->isMethod('post')) {
            Session::flash('status', 'Error! Try agains');
            return redirect()->back();
        }
		//bo sung language
		foreach(config('app.locales') as $key=>$code){
            $rules['language-'.$key.'-name']= 'required';
        }
		$this->validate($request,$rules);
		$language=[];
        foreach($request->all() as $k=>$v){
            if(strpos($k,'language')!==false){
                $str=explode('-', $k);
                $language[$str[1]][$str[2]]=$v;
            }
        }
		
        $input = array();
//        $rules = [
//            'name' => 'required|max:255',
//            'description' => 'required',
//            'unit_price' => 'required'
//        ];
//        $message = [
//            'name.required' => 'Không được để trống',
//            'name.max' => 'Tên sản phẩm quá dài',
//            'description.max' => 'Không được để trống',
//            'unit_price.required' => 'Không được để trống'
//        ];
//
//        $validator = Validator::make($request->all(), $rules, $message);
//        if ($validator->fails()) {
//            return redirect()->back()->withErrors($validator->errors());
//        }
        if ($request->hasFile('image')) {
            $image = time() . "-" . $request->file('image')->getClientOriginalName();
            $destinationPath = public_path() . '/images/upload/product';
            $request->file('image')->move($destinationPath, $image);
        } else {
            $image = "defaultimage.jpg";
        }
//echo "dsada";exit;
        $input["name"] = $request->input('language-1-name');
        $input["slug"] = !empty($request->input('language-1-slug'))?$request->input('language-1-slug'):str_slug($request->input('language-1-name'));
        $input["description"] = $request->input('language-1-description');
        $input["content"] = $request->input('language-1-content');
        $input["unit_price"] = $request->input('unit_price');
        $input["promotion_price"] = $request->input('promotion_price');
        $input["unit"] = $request->input('unit');
        $input["sku"] = $request->input('sku');
        $input["feature"] = ($request->input('feature') == "on") ? 1 : 0;
        $input["neew"] = ($request->input('neew') == "on") ? 1 : 0;
        $input["id_type"] = $request->input('id_type');
        $input["id_trademark"] = $request->input('id_trademark');
        $input["date_price"] = $request->input('date_price');
        $input["created_at"] = date("Y-m-d H:i:s");
        $input["updated_at"] = date("Y-m-d H:i:s");
        $input["image"] = $image;
        $result = Products::create($input);
//        echo "<pre>";
//        print_r($result);
//        echo "</pre>";
//        exit;
        if ($result) {
            Session::flash('status', 'Success!');
			//save table: category language, by newbie ana
			if($language){
				foreach($language as $lang_id=>$item){
					$item['language_id'] = $lang_id;
					$item['product_id'] = $result['id'];
					ProductLanguage::create($item);
				}
		   }
			//add product option , by newbie ana
			$input["product_option"] = $request->input('product_option');
			if(isset($input['product_option'])){
			foreach ($input['product_option'] as $product_option) {
				
				if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
					if (isset($product_option['product_option_value'])) {
						//product_option
						$po['product_id'] = $result['id'];// id product vua tao 
						$po['option_id'] = $product_option['option_id'];
						$po['required'] = $product_option['required'];
						$addproduct_option = ProductOption::create($po);
						
						
						foreach ($product_option['product_option_value'] as $product_option_value) {
							
							//product_option_value
							$pov['product_option_id'] = $addproduct_option['id'];//id product_option vua tao 
							$pov['product_id'] = $result['id'];
							$pov['option_id'] = $product_option['option_id'];
							$pov['option_value_id'] = $product_option_value['option_value_id'];
							$pov['sku'] = $product_option_value['sku'];
							$pov['quantity'] = $product_option_value['quantity'];
							$pov['subtract'] = $product_option_value['subtract'];
							$pov['price'] = $product_option_value['price'];
							$pov['price_prefix'] = $product_option_value['price_prefix'];
							$pov['points'] = $product_option_value['points'];
							$pov['points_prefix'] = $product_option_value['points_prefix'];
							$pov['weight'] = $product_option_value['weight'];
							$pov['weight_prefix'] = $product_option_value['weight_prefix'];
							$addproduct_option_value = ProductOptionValue::create($pov);
						}
					}
				} else {
					
					//product_option type=file
					$po['product_id'] = $result['id'];// id product vua tao 
					$po['option_id'] = $product_option['option_id'];
					$po['value'] = $product_option['value'];
					$po['required'] = $product_option['required'];
					$addproduct_option = ProductOption::create($po);
				}
			}
			}
            return redirect('admin/product/view/' . $result['id']);
        } else {
            Session::flash('status', 'Failed!');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
        $viewItem = Products::find($id);
        $listProductType = ProductType::all();
        $listTrademark = Trademark::all();
		$product_options = $this->getProductOptions($id);
		$option_values = array();

		foreach ($product_options as $product_option) {
			if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
				if (!isset($option_values[$product_option['option_id']])) {
					$option_values[$product_option['option_id']] = $this->getOptionValues($product_option['option_id']);
				}
			}
		}
		$viewProduct = Products::join('products_language','products_language.product_id','=','products.id')->where('products.id',$id)->select('*','products.id as main_product_id')->get();
        return view('admin.product.view', [
            'id' => $id,
            'viewItem' => $viewItem,
            'viewProduct' => $viewProduct,
            'listTrademark' => $listTrademark,
            'listProductType' => $listProductType,
            'product_options' => $product_options,
            'option_values' => $option_values
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Request $request, $id) {
        //
        $viewItem = Products::find($id);
        if (!$viewItem) {
            Session::flash('status', 'Error! Try agains');
            return redirect()->back();
        }
        if (!$request->isMethod('POST') || !$request->isMethod('post')) {
            Session::flash('status', 'Error! Try agains');
            return redirect()->back();
        }
		//bo sung language
		foreach(config('app.locales') as $key=>$code){
            $rules['language-'.$key.'-name']= 'required';
        }
		$this->validate($request,$rules);
		$language=[];
        foreach($request->all() as $k=>$v){
            if(strpos($k,'language')!==false){
                $str=explode('-', $k);
                $language[$str[1]][$str[2]]=$v;
            }
        }
        $rules = [
            'name_vn' => 'required|max:255',
            'description_vn' => 'required',
            'unit_price' => 'required'
        ];
        $message = [
            'name_vn.required' => 'Không được để trống',
            'name_vn.max' => 'Tên sản phẩm quá dài',
            'description_vn.max' => 'Không được để trống',
            'unit_price.required' => 'Không được để trống'
        ];

        //$validator = Validator::make($request->all(), $rules, $message);
        // if ($validator->fails()) {
            // Session::flash('status', 'Please complete all field below');
            // return redirect()->back()->withErrors($validator->errors());
        // }
        if ($request->hasFile('image')) {
            $image = time() . "-" . $request->file('image')->getClientOriginalName();
            $destinationPath = public_path() . '/images/upload/product';
            $request->file('image')->move($destinationPath, $image);
        } else {
            $image = $request->input('old_image');
        }

        $viewItem->name = $request->input('language-1-name');
        $viewItem->slug = empty($request->input('language-1-slug')) ? str_slug($request->input('language-1-name')) : $request->input('language-1-slug');
        $viewItem->description = $request->input('language-1-description');
        $viewItem->content = $request->input('language-1-content');
        $viewItem->unit_price = $request->input('unit_price');
        $viewItem->promotion_price = $request->input('promotion_price');
        $viewItem->unit = $request->input('unit');
        $viewItem->sku = $request->input('sku');
        $viewItem->feature = ($request->input('feature') == "on") ? 1 : 0;
        $viewItem->neew = ($request->input('neew') == "on") ? 1 : 0;
        $viewItem->id_type = $request->input('id_type');
        $viewItem->id_trademark = $request->input('id_trademark');
        $viewItem->date_price = $request->input('date_price');
        $viewItem->updated_at = date("Y-m-d H:i:s");
        $viewItem->image = $image;
        $viewItem->save();
        if ($viewItem) {
            Session::flash('status', 'Success!');
			//update product option, by newbie ana
			$data = $request->all();
			// echo "<pre>";
			// print_r($data);
			// echo "</pre>";exit;
			$this->editProductOption($id, $data);
			//save table: product language, xoá ghi lại 
			ProductLanguage::where('product_id',$id)->delete();
			if($language){
				foreach($language as $lang_id=>$item){
					$item['language_id'] = $lang_id;
					$item['product_id'] = $id;
					ProductLanguage::create($item);
				}
		   }
        } else {
            Session::flash('status', 'Failed!');
        }
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function category() {
        //
        $listCategory = ProductType::orderBy('created_at', 'desc')->get()->toArray();
		foreach($listCategory as $key=>$list){
			$findparent = ProductType::find($list['parent_id']);	
			$listCategory[$key]['parent_name'] = '';
			if($findparent)$listCategory[$key]['parent_name'] = $findparent->name;
		}

		return view('admin.product.listcategory')->with('listCategory',$listCategory);
    }

    public function viewCategory($id) {
        $viewCategory1 = ProductType::find($id);
        $viewCategory = ProductType::join('type_products_language','type_products_language.id_type','=','type_products.id')->where('type_products.id',$id)->select('*','type_products.id as type_products_id')->get();
		$listCategory = ProductType::orderBy('created_at', 'desc')->get()->toArray();
       // echo "<pre>";
       // print_r($viewCategory1);
       // echo "</pre>";
       // exit;
//        $listCategory = ProductType::select('id','name')->orderBy('created_at','desc')->get()->toArray();
        
		$categories=[];
		$producttype = new ProductType();
        $producttype->category_tree(0,'',$categories);

		return view('admin.product.viewcategory', [
            'id' => $id,
            'viewCategory1' => $viewCategory1,
            'categories' => $categories,
            'listCategory' => $listCategory,
            'viewCategory' => $viewCategory
        ]);
    }

    public function editCategory(Request $request, $id) {
        $viewItem = ProductType::find($id);
        
        if (!$request->isMethod('POST') || !$request->isMethod('post')) {
            Session::flash('status', 'Error! Try agains');
            return redirect()->back();
        }
		//bo sung language
		foreach(config('app.locales') as $key=>$code){
            $rules['language-'.$key.'-name']= 'required';
        }
		$this->validate($request,$rules);
		$language=[];
        foreach($request->all() as $k=>$v){
            if(strpos($k,'language')!==false){
                $str=explode('-', $k);
                $language[$str[1]][$str[2]]=$v;
            }
        }
        if ($request->hasFile('image')) { 
            $image = time() . "-" . $request->file('image')->getClientOriginalName();
            $destinationPath = public_path() . '/images/upload/product';
            $request->file('image')->move($destinationPath, $image);
        } else {
            $image = $request->input('old_image');
        }
        $viewItem->name = $request->input('language-1-name');
        $viewItem->parent_id = $request->input('parent_id');
        $viewItem->slug = !empty($request->input('language-1-slug'))?$request->input('language-1-slug'):str_slug($request->input('language-1-name'));
        $viewItem->description = $request->input('language-1-description');
        $viewItem->updated_at = date("Y-m-d H:i:s");
        $viewItem->image = $image;
        $viewItem->save();
        if ($viewItem) {
            Session::flash('status', 'Success!');
			//save table: category language, xoá ghi lại 
			TypeProductsLanguage::where('id_type',$id)->delete();
			if($language){
				foreach($language as $lang_id=>$item){
					$item['language_id'] = $lang_id;
					$item['id_type'] = $id;
					TypeProductsLanguage::create($item);
				}
		   }
        } else {
            Session::flash('status', 'Failed!');
        }
        return redirect()->back();
    }

    public function addCategory() {
		$listCategory = ProductType::orderBy('created_at', 'desc')->get()->toArray();
        $categories=[];
		$producttype = new ProductType();
        $producttype->category_tree(0,'',$categories);
        return view('admin.product.addcategory',[
            'aa' => 'a',
			'listCategory' => $listCategory,
			'categories' => $categories
        ]);
    }
    public function doAddCategory(Request $request){
        if (!$request->isMethod('POST') || !$request->isMethod('post')) {
            Session::flash('status', 'Error! Try agains');
            return redirect()->back();
        }
		//bo sung language
		foreach(config('app.locales') as $key=>$code){
            $rules['language-'.$key.'-name']= 'required';
        }
		$this->validate($request,$rules);
		$language=[];
        foreach($request->all() as $k=>$v){
            if(strpos($k,'language')!==false){
                $str=explode('-', $k);
                $language[$str[1]][$str[2]]=$v;
            }
        }
		
		
        $input = array();
        if ($request->hasFile('image')) {
            $image = time() . "-" . $request->file('image')->getClientOriginalName();
            $destinationPath = public_path() . '/images/upload/product';
            $request->file('image')->move($destinationPath, $image);
        } else {
            $image = "defaultimage.jpg";
        }
        $input["name"] = $request->input('language-1-name');
        $input["slug"] = !empty($request->input('language-1-slug'))?$request->input('language-1-slug'):str_slug($request->input('language-1-name'));
		$input["description"] = $request->input('language-1-description');
		
        $input["parent_id"] = $request->input('parent_id');        
        $input["created_at"] = date("Y-m-d H:i:s");
        $input["image"] = $image;
        $result = ProductType::create($input);
//        echo "<pre>";
//        print_r($result);
//        echo "</pre>";
//        exit;
        if ($result) {
            Session::flash('status', 'Success!');
			//save table: category language
			if($language){
				foreach($language as $lang_id=>$item){
					$item['language_id'] = $lang_id;
					$item['id_type'] = $result['id'];
					TypeProductsLanguage::create($item);
				}
		   }
            return redirect('admin/product/category/' . $result['id']);
        } else {
            Session::flash('status', 'Failed!');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
        $viewItem = Products::find($id);
		//kiem tra id product co ton tai trong order
		$check = OrderDetail::where('product_id',$id)->get();
		
		if(count($check)==0)
			$result = $viewItem->delete(); 
        if (isset($result)) {
            Session::flash('status', 'Success!');
			TypeProductsLanguage::where('id_type',$id)->delete();//del product language
        } else {
            Session::flash('status', 'Failed! This item maybe using');
        }
        
        return redirect()->back();
    }
    
    public function destroyCategory($id) {
        //
        $viewItem = ProductType::find($id);
		$check = Products::where('id_type',$id)->get();
		
		if(count($check)==0)
			$result = $viewItem->delete();
        if (isset($result)) {
            Session::flash('status', 'Success!');
			TypeProductsLanguage::where('id_type',$id)->delete();
        } else {
            Session::flash('status', 'Failed! This item maybe using!');
        }
        return redirect()->back();
    }
	public function trademark() {
        //
        $listTrademark = Trademark::orderBy('created_at', 'desc')->get()->toArray();


		return view('admin.product.listtrademark')->with('listTrademark',$listTrademark);
    }
	public function addTrademark() {
		
        return view('admin.product.addtrademark',[
            'aa' => 'a'
        ]);
    }
    public function doAddTrademark(Request $request){
        if (!$request->isMethod('POST') || !$request->isMethod('post')) {
            Session::flash('status', 'Error! Try agains');
            return redirect()->back();
        }
		//bo sung language
		foreach(config('app.locales') as $key=>$code){
            $rules['language-'.$key.'-name']= 'required';
        }
		$this->validate($request,$rules);
		$language=[];
        foreach($request->all() as $k=>$v){
            if(strpos($k,'language')!==false){
                $str=explode('-', $k);
                $language[$str[1]][$str[2]]=$v;
            }
        }
        $input = array();
        if ($request->hasFile('image')) {
            $image = time() . "-" . $request->file('image')->getClientOriginalName();
            $destinationPath = public_path() . '/images/upload/product';
            $request->file('image')->move($destinationPath, $image);
        } else {
            $image = "defaultimage.jpg";
        }
        $input["name"] = $request->input('language-1-name');
        $input["description"] = $request->input('language-1-description');
        $input["created_at"] = date("Y-m-d H:i:s");
        $input["image"] = $image;
        $result = Trademark::create($input);
//        echo "<pre>";
//        print_r($result);
//        echo "</pre>";
//        exit;
        if ($result) {
            Session::flash('status', 'Success!');
			//save table: trademark language
			if($language){
				foreach($language as $lang_id=>$item){
					$item['language_id'] = $lang_id;
					$item['trademark_id'] = $result['id'];
					TrademarkLanguage::create($item);
				}
		   }
            return redirect('admin/product/trademark/' . $result['id']);
        } else {
            Session::flash('status', 'Failed!');
            return redirect()->back();
        }
    }
	public function viewTrademark($id) {
        $viewTrademark = Trademark::find($id);
		$listTrademark = Trademark::orderBy('created_at', 'desc')->get()->toArray();
		$viewTrademarkLanguage = Trademark::join('trademark_language','trademark_language.trademark_id','=','trademark.id')->where('trademark.id',$id)->select('*','trademark.id as main_trademark_id')->get();
        return view('admin.product.viewtrademark', [
            'id' => $id,
            'listTrademark' => $listTrademark,
            'viewTrademarkLanguage' => $viewTrademarkLanguage,
            'viewTrademark' => $viewTrademark
        ]);
    }

    public function editTrademark(Request $request, $id) {
        $viewItem = Trademark::find($id);
        
        if (!$request->isMethod('POST') || !$request->isMethod('post')) {
            Session::flash('status', 'Error! Try agains');
            return redirect()->back();
        }
		//bo sung language
		foreach(config('app.locales') as $key=>$code){
            $rules['language-'.$key.'-name']= 'required';
        }
		$this->validate($request,$rules);
		$language=[];
        foreach($request->all() as $k=>$v){
            if(strpos($k,'language')!==false){
                $str=explode('-', $k);
                $language[$str[1]][$str[2]]=$v;
            }
        }
		
        if ($request->hasFile('image')) {
            $image = time() . "-" . $request->file('image')->getClientOriginalName();
            $destinationPath = public_path() . '/images/upload/product';
            $request->file('image')->move($destinationPath, $image);
        } else {
            $image = $request->input('old_image');
        }
        $viewItem->name = $request->input('name');
        $viewItem->description = $request->input('description');
        $viewItem->updated_at = date("Y-m-d H:i:s");
        $viewItem->image = $image;
        $viewItem->save();
        if ($viewItem) {
            Session::flash('status', 'Success!');
			//save table: trademark language, xoá item cũ ghi lại
			TrademarkLanguage::where('trademark_id',$id)->delete();
			if($language){
				foreach($language as $lang_id=>$item){
					$item['language_id'] = $lang_id;
					$item['trademark_id'] = $id;
					TrademarkLanguage::create($item);
				}
		   }
        } else {
            Session::flash('status', 'Failed!');
        }
        return redirect()->back();
    }
	public function destroyTrademark($id) {
        
        $viewItem = Trademark::find($id);
		$check = Products::where('id_trademark',$id)->get();
		
		if(count($check)==0)
			$result = $viewItem->delete(); 
        if (isset($result)) {
            Session::flash('status', 'Success!');
			TrademarkLanguage::where('trademark_id',$id)->delete();
        } else {
            Session::flash('status', 'Failed! This item maybe using');
        }
        return redirect()->back();
    }
	public function optionautocomplete(Request $request) {
		$json = array();
		$get=$request->all();
		if (isset($get['filter_name'])) {


			$options = Option::all();
			if (!empty($get['filter_name'])) {
				$options = Option::where('name',$get['filter_name'])->get();
			}

			foreach ($options as $option) {
				$option_value_data = array();
				
				if ($option['type'] == 'select' || $option['type'] == 'radio' || $option['type'] == 'checkbox' || $option['type'] == 'image') {
					$option_values = OptionValue::where('option_id',$option['id'])->get();
					$destinationPath = public_path() . '/images/upload/product/';
					foreach ($option_values as $option_value) {
						if (is_file($destinationPath . $option_value['image'])) {
							$image = $option_value['image'];
						} else {
							$image = "default.png";
						}

						$option_value_data[] = array(
							'option_value_id' => $option_value['id'],
							'name'            => strip_tags(html_entity_decode($option_value['name'], ENT_QUOTES, 'UTF-8')),
							'image'           => $image
						);
					}
					
					$sort_order = array();

					foreach ($option_value_data as $key => $value) {
						$sort_order[$key] = $value['name'];
					}

					array_multisort($sort_order, SORT_ASC, $option_value_data);
				}

				$type = '';

				if ($option['type'] == 'select' || $option['type'] == 'radio' || $option['type'] == 'checkbox') {
					$type = 'Choose';
				}

				if ($option['type'] == 'text' || $option['type'] == 'textarea') {
					$type = 'Input';
				}

				if ($option['type'] == 'file') {
					$type = 'File';
				}

				if ($option['type'] == 'date' || $option['type'] == 'datetime' || $option['type'] == 'time') {
					$type = 'Date';
				}

				$json[] = array(
					'id'    => $option['id'],
					'name'         => strip_tags(html_entity_decode($option['name'], ENT_QUOTES, 'UTF-8')),
					'category'     => $type,
					'type'         => $option['type'],
					'option_value' => $option_value_data
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		//return json_encode($json);
		return $json;
	}
	
	public function getProductOptions($product_id){
		$product_option_data = array();
		
		$product_option_query = ProductOption::select('*','product_option.id as product_option_id')->join('option','option.id','=','product_option.option_id')->where('product_id',$product_id)->get();
		
		foreach ($product_option_query as $product_option) {
			$product_option_value_data = array();
			
			$product_option_value_query =ProductOptionValue::join('option_value','option_value.id','=','product_option_value.option_value_id')->where('product_option_value.product_option_id','=',$product_option['product_option_id'])->get();
		
			foreach ($product_option_value_query as $product_option_value) {
				$product_option_value_data[] = array(
					'product_option_value_id' => $product_option_value['product_option_value_id'],
					'option_value_id'         => $product_option_value['option_value_id'],
					'sku'                => $product_option_value['sku'],
					'quantity'                => $product_option_value['quantity'],
					'subtract'                => $product_option_value['subtract'],
					'price'                   => $product_option_value['price'],
					'price_prefix'            => $product_option_value['price_prefix'],
					'points'                  => $product_option_value['points'],
					'points_prefix'           => $product_option_value['points_prefix'],
					'weight'                  => $product_option_value['weight'],
					'weight_prefix'           => $product_option_value['weight_prefix']
				);
			}

			$product_option_data[] = array(
				'product_option_id'    => $product_option['product_option_id'],
				'product_option_value' => $product_option_value_data,
				'option_id'            => $product_option['option_id'],
				'name'                 => $product_option['name'],
				'type'                 => $product_option['type'],
				'value'                => $product_option['value'],
				'required'             => $product_option['required']
			);
		}
			// echo "<pre>";
			// print_r($product_option_data);
			// echo "</pre>";exit;
		return $product_option_data;
	}
	public function getOptionValues($option_id) {
		$option_value_data = array();
		$option_value_query = OptionValue::where('option_id',$option_id)->get();		

		foreach ($option_value_query as $option_value) {
			$option_value_data[] = array(
				'option_value_id' => $option_value['id'],
				'name'            => $option_value['name'],
				'image'           => $option_value['image'],
				'sort_order'      => $option_value['sort_order']
			);
		}
			// echo "<pre>";
			// print_r($option_value_data);
			// echo "</pre>";exit;
		return $option_value_data;
	}
	public function editProductOption($product_id, $data) {
		
		$viewItem = ProductOption::where('product_id',$product_id)->delete();
		
		$viewItem = ProductOptionValue::where('product_id',$product_id)->delete();	 
			
		if (isset($data['product_option'])) {
			foreach ($data['product_option'] as $product_option) {
				if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
					if (isset($product_option['product_option_value'])) {
						
						$po['product_id'] = $product_id; 
						$po['option_id'] = $product_option['option_id'];
						$po['required'] = $product_option['required'];
						$addproduct_option = ProductOption::create($po);
						

						foreach ($product_option['product_option_value'] as $product_option_value) {
							
							//product_option_value
							$pov['product_option_id'] = $addproduct_option['id'];//id product_option vua tao 
							$pov['product_id'] = $product_id;
							$pov['option_id'] = $product_option['option_id'];
							$pov['option_value_id'] = $product_option_value['option_value_id'];
							$pov['sku'] = $product_option_value['sku'];
							$pov['quantity'] = $product_option_value['quantity'];
							$pov['subtract'] = $product_option_value['subtract'];
							$pov['price'] = $product_option_value['price'];
							$pov['price_prefix'] = $product_option_value['price_prefix'];
							$pov['points'] = $product_option_value['points'];
							$pov['points_prefix'] = $product_option_value['points_prefix'];
							$pov['weight'] = $product_option_value['weight'];
							$pov['weight_prefix'] = $product_option_value['weight_prefix'];
							$addproduct_option_value = ProductOptionValue::create($pov);
						}
					}
				} else {
					
					//product_option type=file
					$po['product_id'] = $product_id;// id product_option vua tao 
					$po['option_id'] = $product_option['option_id'];
					$po['value'] = $product_option['value'];
					$po['required'] = $product_option['required'];
					$addproduct_option = ProductOption::create($po);
				}
			}
		}		

	}
}
