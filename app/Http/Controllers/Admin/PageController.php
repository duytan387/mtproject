<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\RedirectResponse;
use App\Page;
use Session;

class PageController extends AppController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct(Page $page) {
        $this->page = $page;
        //$this->getlistcate = $this->page->getListCate();		
        $this->getlistcollection = $this->page->getListCollection();
        $this->countPage = $this->page->countPage();
        $this->defaultimage = "defaultimage.jpg";
        // $this->getlistslugvn = $this->page->getListSlugVN();
        // $this->getlistslugen = $this->page->getListSlugEN();
        //$this->menu = $this->getListMenu();
    }

    public function index() {
        //
        return view('admin/page/list', [
            'listpage' => $this->page->adminListPage()
        ]);
    }

    public function add() {
        return view('admin/page/add');
    }

    public function doAdd(Request $request) {
        if (!$request->isMethod("POST") || !$request->isMethod("post")) {
            Session::flash('status', 'Error!');
            return redirect()->back();
        }
        if (session('locale') == "vn") {
            $rules = [
                'title_vn' => 'required|max:255',
                    //'description_vn' => 'required|max:255'
//                'category' => 'required',
            ];

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors());
            }

            $title_vn = $request->input('title_vn');
            $title_en = "";
            $slug_vn = empty($request->input('slug_vn')) ? removeDau($request->input('title_vn')) : $request->input('slug_vn');
            $slug_en = "";
            $description_vn = $request->input('description_vn');
            $description_en = "";
            $content_vn = $request->input('content_vn');
            $content_en = "";
        } else {
            $rules = [
                'title_en' => 'required|max:255',
                    //'description_en' => 'required|max:255'
//                'category' => 'required',
            ];

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors());
            }
            $title_en = $request->input('title_en');
            $title_vn = "";
            $slug_en = empty($request->input('slug_en')) ? removeDau($request->input('title_en')) : $request->input('slug_en');

            $slug_vn = "";
            $description_en = $request->input('description_en');
            $description_vn = "";
            $content_en = $request->input('content_en');
            $content_vn = "";
        }
//            exit;
        $seotitle = $request->input('seotitle');
        $seokeyword = $request->input('seokeyword');
        $seodescription = $request->input('seodescription');
        //$category = $request->input('category');		$collection = $request->input('collection');
        // $feature = ($request->input('feature') == "on") ? 1 : 0;
        $created_at = date("Y-m-d H:i:s");
        if ($request->hasFile('image')) {
            $image = time() . "-" . $request->file('image')->getClientOriginalName();
            $destinationPath = public_path() . '/images/upload/page';
            $request->file('image')->move($destinationPath, $image);
        } else {
            $image = $this->defaultimage;
        }
        $id = $this->page->addPage($title_en, $title_vn, $slug_vn, $slug_en, $description_en, $description_vn, $content_en, $content_vn, $image, $seotitle, $seokeyword, $seodescription, $created_at);
        $this->page->updateSlugPage($id, $slug_vn, $slug_en);
        Session::flash('status', 'Thêm trang thành công');
        return redirect('admin/page/view/' . $id);

        Session::flash('status', 'Lỗi! Đã xảy ra sự cố. Vui lòng thử lại!');
        return redirect()->back();
    }

    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
        // kiem tra id co ton tai trong table post
        if (!in_array($id, $this->page->getListId())) {
            return redirect("admin/page/list");
        }

        return view('admin/page/view', [
            'view' => $this->page->adminViewPage($id),
            'listcollection' => $this->getlistcollection
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Request $request, $id) {
        //
        //
//        echo "Dsadas";exit;
        if ($request->isMethod("POST") || $request->isMethod("post")) {
            if (session('locale') == "vn") {
                $rules = [
                    'title_vn' => 'required|max:255',
//            'feature' => 'required',
//            'image' => 'required'
                ];

                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors());
                }

                $title_vn = $request->input('title_vn');
                $title_en = "";
//                $slug = $request->input('slug_vn');
//                if (in_array($slug, $this->getlistslugvn)) {
//                    echo "<br>" . $slug .= "-" . $this->countpost;
//                }
                $slug_vn = !empty($request->input('slug_vn')) ? str_slug($request->input('slug_vn')) : str_slug($request->input('title_vn'));
                $slug_en = "";
                $description_vn = $request->input('description_vn');
                $description_en = "";
                $content_vn = $request->input('content_vn');
                $content_en = "";
            } else {
                $rules = [
                    'title_en' => 'required|max:255',
//            'feature' => 'required',
//            'image' => 'required'
                ];

                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors());
                }

                $title_en = $request->input('title_en');
                $title_vn = "";
                $slug_en = !empty($request->input('slug_en')) ? str_slug($request->input('slug_en')) : str_slug($request->input('title_en'));
                $slug_vn = "";
                $description_en = $request->input('description_en');
                $description_vn = "";
                $content_en = $request->input('content_en');
                $content_vn = "";
            }

            if ($request->hasFile('image')) {
                $image = time() . "-" . $request->file('image')->getClientOriginalName();
                $destinationPath = public_path() . '/images/upload/page';
                $request->file('image')->move($destinationPath, $image);
            } else {
                $image = $request->input('path');
            }
            $seotitle = $request->input('seotitle');
            $seokeyword = $request->input('seokeyword');
            $seodescription = $request->input('seodescription');
            $updated_at = date("Y-m-d H:i:s");

//        exit;
            // neu loi xuat ra "The title may not be greater than 255 characters."

            $this->page->updatePage($id, session('locale'), $title_en, $title_vn, $slug_en, $slug_vn, $description_en, $description_vn, $content_vn, $content_en, $image, $seotitle, $seokeyword, $seodescription, $updated_at);
            Session::flash('status', 'Cập nhật trang thành công.');
            return redirect()->back();
        }
        Session::flash('status', 'Lỗi! Đã xảy ra sự cố. Vui lòng thử lại!');
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
        $this->page->delPage($id);
        Session::flash('status', 'Xóa trang thành công');
        return redirect('admin/page/list');
    }

}
