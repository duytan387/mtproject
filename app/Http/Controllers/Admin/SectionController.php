<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Section;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class SectionController extends AppController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct(Section $section) {
        $this->section = $section;
    }

    public function index() {
        //
        return view('admin/section/list', [
            'sectionlist' => $this->section->getListSection()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
        //  lay danh sach id
        $listid = $this->section->getListId();
        // kiem tra id co ton tai trong table post

        if (!in_array($id, $listid)) {
            return redirect("admin/section/list");
        }

        return view('admin/section/view', [
            'viewsection' => $this->section->viewSection($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Request $request, $id) {
        //
//        echo $title = $request->input('title_en');
//            echo $description = $request->input('description_en');
//            exit;
        if (!$request->isMethod("POST") || !$request->isMethod("post")) {
            Session::flash('status', 'Error!');
            return redirect("admin/section/list");
        }
        $listid = $this->section->getListId();
        // kiem tra id co ton tai trong table post

        if (!in_array($id, $listid)) {
            Session::flash('status', 'error');
            return redirect("admin/section/list");
        }

        if (session('locale') == 'en') {
            $title = $request->input('title_en');
            $description = $request->input('description_en');
            $rules = [
                'title_en' => 'required'
            ];
        } else {
            $title = $request->input('title_vn');
            $description = $request->input('description_vn');
            $rules = [
                'title_vn' => 'required'
            ];
        }
       
        $name = $request->input('name');
        $video = $request->input('video');
        $link = $request->input('link');
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        if ($request->hasFile('image')) {
            $image = time() . "-" . $request->file('image')->getClientOriginalName();
            $destinationPath = public_path() . '/images/upload/section';
            $request->file('image')->move($destinationPath, $image);
        } else {
            $image = $request->input('url');
        }

        $this->section->updateSection(Session('locale'), $id, $name, $title, $description, $image, $video, $link);

        Session::flash('status', 'Cập nhật thành công.');
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

    public function viewService($id) {
        $section = new Section();
        return $section->viewService($id);
    }

}
