<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Banner;
use Session;
use Illuminate\Support\Facades\Validator;

class BannerController extends AppController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct(Banner $banner) {
        $this->banner = $banner;
        $this->defaultimage = "defaultimage.jpg";
    }

    public function index() {
        //
        return view('admin/banner/list', [
            'listbanner' => $this->getAdminListBanner(),
            'viewbanner' => $this->viewBanner(1)
        ]);
    }

    public function add() {
        //
        return view('admin/banner/add');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request) {
        //
        if (!$request->isMethod("POST") || !$request->isMethod("post")) {
            Session::flash('status', 'Error!');
            return redirect("admin/banner/list");
        }
        if (session('locale') == "en") {
            $rules = [
                'title_en' => 'max:255',
                'label_en' => 'max:255',
                'link_en' => 'max:255',
            ];
            $title = $request->input('title_en');
            $label = $request->input('label_en');
            $link = $request->input('link_en');
        } else {
            $rules = [
                'title_vn' => 'max:255',
                'label_vn' => 'max:255',
                'link_vn' => 'max:255',
            ];
            $title = $request->input('title_vn');
            $label = $request->input('label_vn');
            $link = $request->input('link_vn');
        }

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        

        if ($request->hasFile('image')) {
            $image = time() . "-" . $request->file('image')->getClientOriginalName();
            $destinationPath = public_path() . '/images/upload/bannerslides';
            $request->file('image')->move($destinationPath, $image);
        } else {
            $image = $this->defaultimage;
        }

        $created_at = date("Y-m-d H:i:s");
        $id = $this->banner->insertBannerSlide(session('locale'), $title, $label, $request->input('id_cate'), $link, $image, $created_at);
        Session::flash('status', 'Add banner success!');
        return redirect('admin/banner/view/' . $id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
        //  lay danh sach id
        // kiem tra id co ton tai trong table post

        if (!in_array($id, $this->banner->getListId())) {
            return redirect("admin/banner/list");
        }

        return view('admin/banner/view', [
            'viewbanner' => $this->banner->getViewSlide($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edits(Request $request, $id) {
        //
//        echo $id; exit;
//        echo session('locale');exit;
//        if (!$request->isMethod("POST") || !$request->isMethod("post")) {
//            Session::flash('status', 'Error!');
//            return redirect("admin/banner/list");
//        }
//        if (session('locale') == "en") {
//            $rules = [
//                'title_en' => 'required|max:255',
//                'label_en' => 'required|max:255',
//                'link_en' => 'required|max:255',
//            ];
//            $title = $request->input('title_en');
//            $label = $request->input('label_en');
//            $link = $request->input('link_en');
//        } else {
//            $rules = [
//                'title_vn' => 'required|max:255',
//                'label_vn' => 'required|max:255',
//                'link_vn' => 'required|max:255',
//            ];
//            $title = $request->input('title_vn');
//            $label = $request->input('label_vn');
//            $link = $request->input('link_vn');
//        }
////        $validator = Validator::make($request->all(), $rules);
////        if ($validator->fails()) {
////            return redirect()->back()->withErrors($validator->errors());
////        }
//        if ($request->hasFile('image')) {
//            $image = time() . "-" . $request->file('image')->getClientOriginalName();
//            $destinationPath = public_path() . '\images\upload\bannerslides';
//            $request->file('image')->move($destinationPath, $image);
//        } else {
//            $image = $request->input('url');
//        }
//
//        $updated_at = date("Y-m-d H:i:s");
//
//
//        $this->banner->updateBanner(session('locale'), $id, $title, $label, $request->input('id_cate'), $link, $image, $updated_at);
//
//        Session::flash('status', 'Cập nhật banner thành công.');
//        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id) {
        //
        if (!$request->isMethod("POST") || !$request->isMethod("post")) {
            Session::flash('status', 'Error!');
            return redirect("admin/banner/list");
        }
        if (session('locale') == "en") {
            $rules = [
                'title_en' => 'max:255',
                'label_en' => 'max:255',
                'link_en' => 'max:255',
            ];
            $title = $request->input('title_en');
            $label = $request->input('label_en');
            $link = $request->input('link_en');
        } else {
            $rules = [
                'title_vn' => 'max:255',
                'label_vn' => 'max:255',
                'link_vn' => 'max:255',
            ];
            $title = $request->input('title_vn');
            $label = $request->input('label_vn');
            $link = $request->input('link_vn');
        }
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        if ($request->hasFile('image')) {
            $image = time() . "-" . $request->file('image')->getClientOriginalName();
            $destinationPath = public_path() . '/images/upload/bannerslides';
            $request->file('image')->move($destinationPath, $image);
        } else {
            $image = $request->input('url');
        }

        $updated_at = date("Y-m-d H:i:s");


        $this->banner->updateBanner(session('locale'), $id, $title, $label, $request->input('id_cate'), $link, $image, $updated_at);

        Session::flash('status', 'Cập nhật banner thành công.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
        //  lay danh sach id

        if (!in_array($id, $this->banner->getListId())) {
            return redirect("admin/banner/list");
        }
        $this->banner->delBanner($id);
        Session::flash('status', 'Xóa bài viết thành công');
        return redirect('admin/banner/list');
    }

    public function getAdminListBanner() {
        $bannerslide = new Banner();
        return $bannerslide->getAdminListBanner();
    }

    public function viewBanner($id) {
        $bannerslide = new Banner();
        return $bannerslide->getViewSlide($id);
    }

}
