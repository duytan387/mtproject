<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Menu;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class MenuController extends AppController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct(Menu $menu) {
        $this->menu = $menu;
    }

    public function index() {
        //
        return view('admin/menu/list', [
            'listcate' => $this->menu->getCate()
        ]);
    }

    public function showCate($id_catemenu) {
        return view('admin/menu/catemenu', [
            'getcatemenu' => $this->menu->getCateMenu($id_catemenu),
            'listmenu' => $this->menu->getListCate($id_catemenu)
        ]);
    }

    public function showMenu($id) {
        
        if (!in_array($id, $this->menu->listIDLinhVuc())) {
            return redirect("admin/menu/catemenu/".$id);
        }
        $checkcate = $this->menu->getMenu($id);
        return view('admin/menu/detailmenu', [
            'detailmenu' => $this->menu->getMenu($id),
            'cate' => $this->menu->getOneCate($checkcate->id_catemenu),
            'listid_catemenu' => $this->menu->getCate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
        return view('admin/menu/add',[
            'listid_catemenu' => $this->menu->getCate()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }
    
    public function doAddMenu(Request $request)
    {
        if ($request->method() == "POST" || $request->method() == "post") {
            $rules = [
                'title_vn' => 'required|max:255',
                'link_vn' => 'required|max:255',
                'id_catemenu' => 'required'
            ];

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors());
            }
            $id = $this->menu->setMenuLinhVuc($request->input('title_vn'), $request->input('link_vn'), $request->input('id_catemenu'));
            Session::flash('status', 'Success');
            return redirect('admin/menu/detailmenu/'.$id);
        }
        Session::flash('status', 'Error! Please try again');
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function setMenu(Request $request, $id) {
        //
//        echo "dsadsa";exit;
        if ($request->method() == "POST") {
            $rules = [
                'title_vn' => 'required|max:255',
                'link_vn' => 'required|max:255',
                'id_catemenu' => 'required',
            ];
//            $request->input('title_vn');

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors());
            }
            $this->menu->updateLinhVuc($id, $request->input('title_vn'), $request->input('link_vn'), $request->input('id_catemenu'));
            Session::flash('status', 'Success');
            return redirect('admin/menu/detailmenu/'.$id);
        }
        Session::flash('status', 'Đã có lỗi trong quá trình xử lý. Vui lòng thử lại!');
        return redirect('admin/menu/detailmenu/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
//        echo "dsadasda";exit;
        $this->menu->doDelLinhVuc($id);
        Session::flash('status', 'Thành công');
        return redirect('admin/menu/catemenu/1');
    }
	
	public function destroyCateMenu($id) {
        //
//        echo "dsadasda";exit;
        $this->menu->delCateMenu($id);
        Session::flash('status', 'Thành công');
        return redirect('admin/menu/list');
    }

}
