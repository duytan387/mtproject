<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Customer;
use App\Orders;
use Session;
use Illuminate\Support\Facades\Validator;

class CustomerController extends AppController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct(Customer $customer) {
        $this->customer = $customer;
    }

    public function index() {
        //        
        return view('admin/customer/list', [
            'customerlist' => $this->customer->getAdminListCustomer(),
        ]);
    }

    public function getListScreenshot() {
//        $customer = new Screenshot();
//        return $customer->getAdminListScreenshot();
    }

    public function add() {
//        $listcate = new Post();
        return view('admin/customer/add');
    }

    public function doAdd(Request $request) {
        if (!$request->isMethod("POST") || !$request->isMethod("post")) {
            Session::flash('status', 'Error!');
            return redirect("admin/customer/add");
        }
        $rules = [
            'name' => 'required',
        ];

        $name = $request->input('name');
        $position = $request->input('position');
        $comment = $request->input('comment');


        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $created_at = date("Y-m-d H:i:s");

        if ($request->hasFile('image')) {
            $image = time() . "-" . $request->file('image')->getClientOriginalName();
            $destinationPath = public_path() . '/images/upload/customer';
            $request->file('image')->move($destinationPath, $image);
        } else {
            $image = "defaultimage.jpg";
        }

        $id = $this->customer->addCustomer($name, $position, $comment, $image, $created_at);
        Session::flash('status', 'Thêm thành công');
        return redirect('admin/customer/view/' . $id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //  lay danh sach id
        $listid = $this->customer->getListId();
        // kiem tra id co ton tai trong table
        if (!in_array($id, $listid)) {
            return redirect("admin/customer/list");
        }

        return view('admin/customer/view', [
            'viewcustomer' => $this->customer->viewCustomer($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Request $request, $id) {
        $rules = [
            'name' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }
        $name = $request->input('name');
        $position = $request->input('position');
        $comment = $request->input('comment');
        if ($request->hasFile('image')) {
            $image = time() . "-" . $request->file('image')->getClientOriginalName();
            $destinationPath = public_path() . '/images/upload/customer';
            $request->file('image')->move($destinationPath, $image);
        } else {
            $image = $request->input('currentfile');
        }

        $updated_at = date("Y-m-d H:i:s");

//        exit;
        // neu loi xuat ra "The title may not be greater than 255 characters."
        $this->customer->updateCustomer($id, $name, $position, $comment, $image, $updated_at);
        Session::flash('status', 'Cập nhật bài viết thành công.');
        return redirect('admin/customer/view/' . $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
        $listid = $this->customer->getListId();
        // kiem tra id co ton tai trong table
        // if (!in_array($id, $listid)) {
            // return redirect()->back();
        // }
		
		$viewItem = Customer::find($id);
		//kiem tra id product co ton tai trong order
		$check = Orders::where('customer_id',$id)->get();
		
		if(count($check)==0)
			$result = $viewItem->delete(); 
        if (isset($result)) {
            Session::flash('status', 'Success!');
        } else {
            Session::flash('status', 'Failed! This item maybe using');
        }
        // $this->customer->delCustomer($id);
        // Session::flash('status', 'Xóa bài viết thành công');
        return redirect()->back();
    }

}
