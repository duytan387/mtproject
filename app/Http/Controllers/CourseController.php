<?php

namespace App\Http\Controllers;

use App\Consultant;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;

class CourseController extends Controller {

    public function __construct(Consultant $consultant) {
        $this->consultant = $consultant;
    }

    public function create(Request $request) {
        if (!$request->isMethod('post') || !$request->isMethod('POST')) {
            return response()->json(['result' => '<p>Lỗi! Vui lòng thử lại</p>']);
        }
        $rules = [
            'name' => 'required|max:255',
            'phone' => 'required|numeric|phone_count',
            'course' => 'required|max:255',
        ];
        $messages = [
            'name.required' => 'Vui lòng nhập Họ Tên',
            'name.max' => 'Họ Tên quá dài',
            'phone.required' => 'Vui lòng nhập số Điện thoại',
            'phone.phone_count' => 'Số điện thoại không đúng',
            'course.required' => 'Vui lòng chọn khóa học',
        ];
        // them validate phone_count
        Validator::extend('phone_count', function($attribute, $value, $parameters) {
            return strlen($value) >= 10 && strlen($value) < 11;
        });

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
//            return redirect()->back()->withErrors($validator);
            return response()->json(['result' => $validator->messages(), 'hoten' => $request->input('name'), 'phone' => $request->input('phone'), 'course' => $request->input('course')]);
        }
//        $email = $request->input('email');
//        if (empty($request->input('name')) || !is_numeric($request->input('phone')) || empty($request->input('course'))) {
//            return response()->json(['result' => '<p>Vui lòng điền đầy đủ thông tin</p>', 'status' => '']);
//        }
        else {
            $name = $request->input('name');
            $phone = $request->input('phone');
            $course = $request->input('course');
            $currentdate = date("Y-m-d h:i:s");
            $this->consultant->addItem($name, $phone, $course, $currentdate);
            // Neu co status khac rong thi dang ky thanh cong
            return response()->json(['result' => '<p>Cảm ơn bạn đã liên hệ! Chúng tôi sẽ phản hồi trong thời gian sớm nhất.</p>', 'hoten' => '', 'phone' => '', 'course' => '']);
        }
    }

}
