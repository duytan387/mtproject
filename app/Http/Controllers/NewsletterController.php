<?php

namespace App\Http\Controllers;

use App\NotificationRegister;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Newsletter;

class NewsletterController extends AppController {
    public function __construct(Newsletter $newsletter) {
        parent::__construct();
        $this->newsletter = $newsletter;
    }

    public function setNewsletter(Request $request) {

        if(!$request->isMethod('post') || !$request->isMethod('POST')){
            return redirect('/');
        }
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
        ]);

        if ($validator->fails()) {
            Session::flash('newsletter', 'Vui lòng điền email');
            return redirect()->back()->withErrors($validator->errors());
        }
        $email = $request->input('email');
        $currentDate = date("Y-m-d h:i:s");
        $this->newsletter->createForm($email, $currentDate);
        Session::flash('newsletter', 'Cảm ơn bạn đã đăng ký');
        return redirect('/');

    }



    /**

     * Store a newly created resource in storage.

     *

     * @return Response

     */

    public function store() {

        //

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return Response

     */

    public function show($id) {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return Response

     */

    public function edit($id) {

        //

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  int  $id

     * @return Response

     */

    public function update($id) {

        //

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return Response

     */

    public function destroy($id) {

        //

    }



}

