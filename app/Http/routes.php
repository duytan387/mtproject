<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/', [
    'as' => 'home',
    'uses' => 'HomeController@index'
]);
Route::get('san-pham/dat-hang',[
	'as'=>'dathang',
	'uses'=>'CartController@getCheckout'
]);
Route::get('san-pham/dat-hang-thanh-cong',[
    'as' => 'dathangthanhcong',
    'uses' => 'CartController@orderSuccess'
]);
Route::get('/gio-hang',[
    'as' => 'giohang',
    'uses' => 'CartController@index'
]);
Route::get('/banh-trung-thu-kinh-do', [
    'as' => 'sanpham',
    'uses' => 'ProductController@index'
]);
Route::post('/san-pham/thanh-toan',[
    'as' => 'thanhtoan',
    'uses' => 'CartController@setPayment'
]);

Route::get('/{slug}', [
    'as' => 'page',
    'uses' => 'PageController@index'
]);
Route::get('/du-an/{slug}', [
    'as' => 'projectdetail',
    'uses' => 'PageController@detail'
]);
Route::get('/tin-tuc/{slug}', [
    'as' => 'newsdetail',
    'uses' => 'PageController@detail'
]);
Route::get('/tuyen-dung/{slug}', [
    'as' => 'careerdetail',
    'uses' => 'PageController@detail'
]);
Route::get('/dich-vu/{slug}', [
    'as' => 'servicedetail',
    'uses' => 'PageController@detail'
]);
Route::post('/dich-vu', [
    'as' => 'service',
    'uses' => 'CourseController@create'
]);
Route::post('/newsletter', 'NewsletterController@setNewsletter');
Route::post('/lien-he', 'ContactController@create');

Route::get('/banner', 'HomeController@getSource');


Route::get('san-pham/{cate_slug}', [
    'as' => 'productcate',
    'uses' => 'ProductController@showCate'
]);

Route::get('san-pham/{cate_slug}/{slug}', [
    'as' => 'productdetail',
    'uses' => 'ProductController@show'
]);



//Route::get('/banh-trung-thu-kinh-do/{slug}.html', [
//    'as' => 'productcategory',
//    'uses' => 'ProductController@showCate'
//]);

Route::get('add-to-cart/{id}', [
    'as' => 'themgiohang',
    'uses' => 'ProductController@addToCart'
]);
Route::get('del-cart/{id}',[
	'as'=>'xoagiohang',
	'uses'=>'ProductController@getDelItemCart'
]);
Route::get('update-cart/{id}',[
	'as'=>'capnhatgiohang',
	'uses'=>'ProductController@updateItemCart'
]);


//Route::get('home', 'HomeController@index');
//Route::post('/login', 'LoginController@postLogin')->name('login');
Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

// Start Language
Route::get('language/{locale}', function ($locale) {
    Session::put('locale', $locale);
    return redirect()->back();
});
// End Language
// Start Login
Route::get('/admin/login', [
    'as' => 'login',
    'uses' => 'Admin\LoginController@index'
]);
Route::get('/admin', [
    'as' => 'logins',
    'uses' => 'Admin\LoginController@index'
]);
Route:post('admin/login', [
    'as' => 'dologin',
    'uses' => 'Admin\LoginController@doLogin'
]);

Route::get('admin/logout', [
    'as' => 'logout',
    'uses' => 'Admin\LoginController@logout'
]);

Route::get('admin/remember', [
    'as' => 'remember',
    'uses' => 'Admin\LoginController@remember'
]);
Route::post('admin/remember', [
    'as' => 'doremember',
    'uses' => 'LoginController@doRemember'
]);
// End Login
// Start dashboard
Route::get('admin/dashboard', [
    'as' => 'dashboard',
    'middleware' => 'auth',
    'uses' => 'Admin\DashboardController@index'
]);
// End dashboard

Route::group(['prefix' => 'admin'], function() {
    // Start consultant
    Route::get('/consultant/list', [
        'as' => 'listcourse',
        'middleware' => 'auth',
        'uses' => 'Admin\ConsultantController@index'
    ]);
    Route::get('/consultant/delete/{id}', [
        'as' => 'delcourse',
        'middleware' => 'auth',
        'uses' => 'Admin\ConsultantController@destroy'
    ]);
    Route::get('/consultant/view/{id}', [
        'as' => 'viewcourse',
        'middleware' => 'auth',
        'uses' => 'Admin\ConsultantController@show'
    ]);
    // End consultant
    // Start contact
    Route::get('/contact/list', ['as' => 'listcontact', 'uses' => 'Admin\ContactController@index']);
    Route::get('/contact/delete/{id}', ['as' => 'delcontact', 'uses' => 'Admin\ContactController@destroy']);
    Route::get('/contact/view/{id}', ['as' => 'viewcontact', 'uses' => 'Admin\ContactController@show']);
    Route::post('/contact/update/{id}', ['as' => 'updatecontact', 'uses' => 'Admin\ContactController@update']);
    // End contact
});

Route::group(['middleware' => 'editor'], function() {
    
});




// Start Post

Route::get('admin/post/list', [
    'as' => 'listpost',
    'middleware' => 'auth',
    'uses' => 'Admin\PostController@index'
]);
Route::get('admin/post/list/{id_cate}', [
    'as' => 'listcatepost',
    'middleware' => 'auth',
    'uses' => 'Admin\PostController@listCatePost'
]);

Route::post('admin/post/list?id_cate={id_cate}&keyword={keyword}', [
    'as' => 'searchpostcate',
    'middleware' => 'auth',
    'uses' => 'Admin\PostController@searchPostCate'
]);

Route::get('admin/post/view/{id}', [
    'as' => 'viewpost',
    'middleware' => 'auth',
    'uses' => 'Admin\PostController@show'
]);

Route::post('admin/post/edit/{id}', [
    'as' => 'editpost',
    'middleware' => 'auth',
    'uses' => 'Admin\PostController@edit'
]);
Route::get('admin/post/add', [
    'as' => 'addpost',
    'middleware' => 'auth',
    'uses' => 'Admin\PostController@add'
]);
Route::post('admin/post/doAdd', [
    'as' => 'doaddpost',
    'middleware' => 'auth',
    'uses' => 'Admin\PostController@doAdd'
]);
Route::get('admin/post/delete/{id}', [
    'as' => 'deletepost',
    'middleware' => 'auth',
    'uses' => 'Admin\PostController@destroy'
]);
Route::post('admin/post/list', [
    'as' => 'searchpostcate',
    'middleware' => 'auth',
    'uses' => 'Admin\PostController@searchPostCate'
]);
Route::get('admin/post/listcategorypost', [
    'as' => 'listcategorypost',
    'middleware' => 'auth',
    'uses' => 'Admin\PostController@categoryPost'
]);
Route::get('admin/post/viewcategorypost/{id}', [
    'as' => 'viewcategorypost',
    'middleware' => 'auth',
    'uses' => 'Admin\PostController@viewCategoryPost'
]);
Route::post('admin/post/editcategorypost/{id}', [
    'as' => 'editcategorypost',
    'middleware' => 'auth',
    'uses' => 'Admin\PostController@editCategoryPost'
]);
Route::get('admin/post/destroycatepost/{id}', [
    'as' => 'destroycatepost',
    'middleware' => 'auth',
    'uses' => 'Admin\PostController@destroyCatePost'
]);
Route::get('admin/post/addcategorypost', [
    'as' => 'addcategorypost',
    'middleware' => 'auth',
    'uses' => 'Admin\PostController@addCategoryPost'
]);
Route::post('admin/post/doaddcatepost', [
    'as' => 'doaddcatepost',
    'middleware' => 'auth',
    'uses' => 'Admin\PostController@doAddCatePost'
]);

Route::get('admin/post/listcollectionpost', [
    'as' => 'listcollectionpost',
    'middleware' => 'auth',
    'uses' => 'Admin\CollectionController@index'
]);
Route::get('admin/post/viewcollectionpost/{id}', [
    'as' => 'viewcollectionpost',
    'middleware' => 'auth',
    'uses' => 'Admin\CollectionController@viewCollectionPost'
]);
Route::post('admin/post/editcollectionpost/{id}', [
    'as' => 'editcollectionpost',
    'middleware' => 'auth',
    'uses' => 'Admin\CollectionController@editCollectionPost'
]);
Route::get('admin/post/addcollectionpost', [
    'as' => 'addcollectionpost',
    'middleware' => 'auth',
    'uses' => 'Admin\CollectionController@addCollectionPost'
]);
Route::post('admin/post/doaddcollectionpost', [
    'as' => 'doaddcollectionpost',
    'middleware' => 'auth',
    'uses' => 'Admin\CollectionController@doAddCollectionPost'
]);
Route::get('admin/post/destroycollectionpost/{id}', [
    'as' => 'destroycollectionpost',
    'middleware' => 'auth',
    'uses' => 'Admin\CollectionController@destroyCollectionPost'
]);

Route::post('admin/post/image/edit/{id}', [
    'as' => 'delimage',
    'middleware' => 'auth',
    'uses' => 'Admin\PostController@delImage'
]);

// End Post
//Start Page

Route::get('admin/page/list', [
    'as' => 'listpage',
    'middleware' => 'auth',
    'uses' => 'Admin\PageController@index'
]);

Route::get('admin/page/view/{id}', [
    'as' => 'viewpage',
    'middleware' => 'auth',
    'uses' => 'Admin\PageController@show'
]);

Route::get('admin/page/add', [
    'as' => 'addpage',
    'middleware' => 'auth',
    'uses' => 'Admin\PageController@add'
]);

Route::post('admin/page/doAdd', [
    'as' => 'doaddpage',
    'middleware' => 'auth',
    'uses' => 'Admin\PageController@doAdd'
]);

Route::get('admin/page/delete/{id}', [
    'as' => 'deletepage',
    'middleware' => 'auth',
    'uses' => 'Admin\PageController@destroy'
]);

Route::post('admin/page/edit/{id}', [
    'as' => 'editpage',
    'middleware' => 'auth',
    'uses' => 'Admin\PageController@edit'
]);


// End Page
// Start banner
Route::get('admin/banner/list', [
    'as' => 'listbanner',
    'middleware' => 'auth',
    'uses' => 'Admin\BannerController@index'
]);
Route::get('admin/banner/destroy/{id}', [
    'as' => 'delbanner',
    'uses' => 'Admin\BannerController@destroy'
]);
Route::get('admin/banner/view/{id}', [
    'as' => 'viewbanner',
    'middleware' => 'auth',
    'uses' => 'Admin\BannerController@show'
]);
Route::post('admin/banner/edit/{id}', [
    'as' => 'editbanner',
    'middleware' => 'auth',
    'uses' => 'Admin\BannerController@edit'
]);

Route::post('admin/banner/update/{id}', [
    'as' => 'updatebanner',
    'middleware' => 'auth',
    'uses' => 'Admin\BannerController@update'
]);

Route::get('admin/banner/add', [
    'as' => 'addbanner',
    'middleware' => 'auth',
    'uses' => 'Admin\BannerController@add'
]);
Route::post('admin/banner/create', [
    'as' => 'createbanner',
    'middleware' => 'auth',
    'uses' => 'Admin\BannerController@create'
]);

// End banner
// Start section
Route::get('admin/section/list', [
    'as' => 'listsection',
    'middleware' => 'auth',
    'uses' => 'Admin\SectionController@index'
]);
Route::get('admin/section/view/{id}', [
    'as' => 'editsection',
    'middleware' => 'auth',
    'uses' => 'Admin\SectionController@show'
]);
Route::post('admin/banner/edit/{id}', [
    'as' => 'editsection',
    'middleware' => 'auth',
    'uses' => 'Admin\SectionController@edit'
]);
// End section
// Start Newsletter
Route::get('admin/newsletter/list', [
    'as' => 'listnewsletter',
    'middleware' => 'auth',
    'uses' => 'Admin\NewsletterController@index'
]);
Route::get('admin/newsletter/delete/{id}', [
    'as' => 'delnewsletter',
    'middleware' => 'auth',
    'uses' => 'Admin\NewsletterController@destroy'
]);
Route::get('admin/newsletter/view/{id}', [
    'as' => 'viewnewsletter',
    'middleware' => 'auth',
    'uses' => 'Admin\NewsletterController@show'
]);

Route::post('admin/newsletter/update/{id}', [
    'as' => 'updatenewsletter',
    'middleware' => 'auth',
    'uses' => 'Admin\NewsletterController@update'
]);
// End Newsletter
// Start Screenshot
Route::get('admin/screenshot/list', [
    'as' => 'listscreenshot',
    'middleware' => 'auth',
    'uses' => 'Admin\ScreenshotController@index'
]);
Route::get('admin/screenshot/delete/{id}', [
    'as' => 'delscreenshot',
    'middleware' => 'auth',
    'uses' => 'Admin\ScreenshotController@destroy'
]);
Route::get('admin/screenshot/view/{id}', [
    'as' => 'viewscreenshot',
    'middleware' => 'auth',
    'uses' => 'Admin\ScreenshotController@show'
]);
Route::post('admin/screenshot/edit/{id}', [
    'as' => 'editscreenshot',
    'middleware' => 'auth',
    'uses' => 'Admin\ScreenshotController@edit'
]);
Route::get('admin/screenshot/add', [
    'as' => 'addscreenshot',
    'middleware' => 'auth',
    'uses' => 'Admin\ScreenshotController@add'
]);
Route::post('admin/screenshot/doAdd/{id}', [
    'as' => 'doaddscreenshot',
    'middleware' => 'auth',
    'uses' => 'Admin\ScreenshotController@doAdd'
]);
// End Screenshot
// Start Customer
Route::get('admin/customer/list', [
    'as' => 'listcustomer',
    'middleware' => 'auth',
    'uses' => 'Admin\CustomerController@index'
]);
Route::get('admin/customer/delete/{id}', [
    'as' => 'delcustomer',
    'middleware' => 'auth',
    'uses' => 'Admin\CustomerController@destroy'
]);
Route::get('admin/customer/view/{id}', [
    'as' => 'viewcustomer',
    'middleware' => 'auth',
    'uses' => 'Admin\CustomerController@show'
]);
Route::post('admin/customer/edit/{id}', [
    'as' => 'editcustomer',
    'middleware' => 'auth',
    'uses' => 'Admin\CustomerController@edit'
]);
Route::get('admin/customer/add', [
    'as' => 'addcustomer',
    'middleware' => 'auth',
    'uses' => 'Admin\CustomerController@add'
]);
Route::post('admin/customer/doAdd', [
    'as' => 'doaddcustomer',
    'middleware' => 'auth',
    'uses' => 'Admin\CustomerController@doAdd'
]);
// End Customer
// Start Video
Route::get('admin/video/view/{id}', [
    'as' => 'viewvideo',
    'middleware' => 'auth',
    'uses' => 'Admin\VideoController@show'
]);
Route::post('admin/video/edit/{id}', [
    'as' => 'editvideo',
    'middleware' => 'auth',
    'uses' => 'Admin\VideoController@edit'
]);
// End Video
// Start setting
Route::get('admin/setting/mail', [
    'as' => 'mail',
    'middleware' => 'auth',
    'uses' => 'Admin\SettingController@configMail'
]);
Route::get('admin/setting/advertising', [
    'as' => 'advertising',
    'middleware' => 'auth',
    'uses' => 'Admin\SettingController@setAds'
]);
Route::post('admin/setting/doupdate', [
    'as' => 'doupdate',
    'middleware' => 'auth',
    'uses' => 'Admin\SettingController@setAdvertising'
]);
Route::post('admin/setting/setMail', [
    'as' => 'setmail',
    'middleware' => 'auth',
    'uses' => 'Admin\SettingController@setMail'
]);
Route::get('admin/setting/frontend', [
    'as' => 'frontend',
    'middleware' => 'auth',
    'uses' => 'Admin\SettingController@frontend'
]);
Route::post('admin/setting/social', [
    'as' => '',
    'middleware' => 'auth',
    'uses' => 'Admin\SettingController@setSocial'
]);
Route::post('admin/setting/header', [
    'as' => '',
    'middleware' => 'auth',
    'uses' => 'Admin\SettingController@setHeader'
]);
Route::post('admin/setting/footer', [
    'as' => '',
    'middleware' => 'auth',
    'uses' => 'Admin\SettingController@setFooter'
]);
// End setting
// Start User
Route::get('admin/user/profile', [
    'as' => 'userprofile',
    'middleware' => 'auth',
    'uses' => 'Admin\UserController@setProfile'
]);
Route::get('admin/user/list', [
    'as' => 'userlist',
    'middleware' => 'auth',
    'uses' => 'Admin\UserController@index'
]);
Route::get('admin/user/view/{id}', [
    'as' => 'userview',
    'middleware' => 'auth',
    'uses' => 'Admin\UserController@view'
]);
Route::post('admin/user/edit/{id}', [
    'as' => 'useredit',
    'middleware' => 'auth',
    'uses' => 'Admin\UserController@edit'
]);
Route::get('admin/user/add', [
    'as' => 'useradd',
    'middleware' => 'auth',
    'uses' => 'Admin\UserController@add'
]);
Route::post('admin/user/create', [
    'as' => 'usercreate',
    'middleware' => 'auth',
    'uses' => 'Admin\UserController@create'
]);
Route::get('admin/user/delete/{id}', [
    'as' => 'userdelete',
    'middleware' => 'auth',
    'uses' => 'Admin\UserController@destroy'
]);
// End User
// Start Menu
Route::get('admin/menu/list', [
    'as' => 'listmenu',
    'middleware' => 'auth',
    'uses' => 'Admin\MenuController@index'
]);
Route::get('admin/menu/catemenu/{id_catemenu}', [
    'as' => 'catemenu',
    'middleware' => 'auth',
    'uses' => 'Admin\MenuController@showCate'
]);
Route::get('admin/menu/detailmenu/{id}', [
    'as' => 'detailmenu',
    'middleware' => 'auth',
    'uses' => 'Admin\MenuController@showMenu'
]);
Route::post('admin/menu/updatelinhvuc/{id}', [
    'as' => 'detailmenu',
    'middleware' => 'auth',
    'uses' => 'Admin\MenuController@setMenu'
]);
Route::get('admin/menu/delete/{id}', [
    'as' => 'deletemenu',
    'middleware' => 'auth',
    'uses' => 'Admin\MenuController@destroy'
]);
Route::get('admin/menu/deletecatemenu/{id}', [
    'as' => 'deletecatemenu',
    'middleware' => 'auth',
    'uses' => 'Admin\MenuController@destroyCateMenu'
]);
Route::get('admin/menu/add', [
    'as' => 'addmenu',
    'middleware' => 'auth',
    'uses' => 'Admin\MenuController@create'
]);
Route::post('admin/menu/doAdd', [
    'as' => 'doaddmenu',
    'middleware' => 'auth',
    'uses' => 'Admin\MenuController@doAddMenu'
]);
// End Menu
// Start File Manager
Route::get('admin/filemanage/filemanager', [
    'as' => 'filemanager',
    'middleware' => 'auth',
    'uses' => 'Admin\FileController@index'
]);
// End File Manager
//Start Gallery

Route::get('admin/gallery/list', [
    'as' => 'listgallery',
    'middleware' => 'auth',
    'uses' => 'Admin\GalleryController@index'
]);

Route::get('admin/gallery/view/{id}', [
    'as' => 'viewgallery',
    'middleware' => 'auth',
    'uses' => 'Admin\GalleryController@show'
]);

Route::get('admin/gallery/add', [
    'as' => 'addgallery',
    'middleware' => 'auth',
    'uses' => 'Admin\GalleryController@add'
]);

Route::post('admin/gallery/doAdd', [
    'as' => 'doaddgallery',
    'middleware' => 'auth',
    'uses' => 'Admin\GalleryController@doAdd'
]);

Route::get('admin/gallery/delete/{id}', [
    'as' => 'deletegallery',
    'middleware' => 'auth',
    'uses' => 'Admin\GalleryController@destroy'
]);

Route::post('admin/gallery/edit/{id}', [
    'as' => 'editgallery',
    'middleware' => 'auth',
    'uses' => 'Admin\GalleryController@edit'
]);


// End Gallery

/* Start Product */
Route::get('admin/product/list', [
    'as' => 'listproduct',
    'middleware' => 'auth',
    'uses' => 'Admin\ProductController@index'
]);
Route::get('admin/product/view/{id}', [
    'as' => 'viewproduct',
    'middleware' => 'auth',
    'uses' => 'Admin\ProductController@show'
]);
Route::post('admin/product/edit/{id}', [
    'as' => 'editproduct',
    'middleware' => 'auth',
    'uses' => 'Admin\ProductController@edit'
]);
Route::get('admin/product/add', [
    'as' => 'addproduct',
    'middleware' => 'auth',
    'uses' => 'Admin\ProductController@create'
]);
Route::post('admin/product/add', [
    'as' => 'doaddproduct',
    'middleware' => 'auth',
    'uses' => 'Admin\ProductController@doAdd'
]);
Route::get('admin/product/delete/{id}', [
    'as' => 'delproduct',
    'middleware' => 'auth',
    'uses' => 'Admin\ProductController@destroy'
]);
Route::get('admin/product/category', [
    'as' => 'cateproduct',
    'middleware' => 'auth',
    'uses' => 'Admin\ProductController@category'
]);
Route::get('admin/product/category/{id}', [
    'as' => 'viewcateproduct',
    'middleware' => 'auth',
    'uses' => 'Admin\ProductController@viewCategory'
]);
Route::post('admin/product/category/{id}', [
    'as' => 'editcateproduct',
    'middleware' => 'auth',
    'uses' => 'Admin\ProductController@editCategory'
]);
Route::get('admin/product/categoryproduct/add', [
    'as' => 'addcateproduct',
    'middleware' => 'auth',
    'uses' => 'Admin\ProductController@addCategory'
]);
Route::post('admin/product/categoryproduct/add', [
    'as' => 'doaddcateproduct',
    'middleware' => 'auth',
    'uses' => 'Admin\ProductController@doAddCategory'
]);
Route::get('admin/product/category/delete/{id}', [
    'as' => 'cateproduct',
    'middleware' => 'auth',
    'uses' => 'Admin\ProductController@destroyCategory'
]);
Route::post('admin/product/search', [
    'as' => 'searchproduct',
    'middleware' => 'auth',
    'uses' => 'Admin\ProductController@search'
]);
Route::get('admin/product/trademark', [
    'as' => 'trademarkproduct',
    'middleware' => 'auth',
    'uses' => 'Admin\ProductController@trademark'
]);
Route::get('admin/product/trademark/add', [
    'as' => 'addtrademark',
    'middleware' => 'auth',
    'uses' => 'Admin\ProductController@addTrademark'
]);
Route::post('admin/product/trademark/add', [
    'as' => 'doaddtrademark',
    'middleware' => 'auth',
    'uses' => 'Admin\ProductController@doAddTrademark'
]);
Route::get('admin/product/trademark/{id}', [
    'as' => 'viewtrademark',
    'middleware' => 'auth',
    'uses' => 'Admin\ProductController@viewTrademark'
]);
Route::post('admin/product/trademark/{id}', [
    'as' => 'edittrademark',
    'middleware' => 'auth',
    'uses' => 'Admin\ProductController@editTrademark'
]);
Route::get('admin/product/trademark/delete/{id}', [
    'as' => 'trademark',
    'middleware' => 'auth',
    'uses' => 'Admin\ProductController@destroyTrademark'
]);
Route::get('admin/product/optionautocomplete', [
	'as' => 'optionautocomplete',
	'middleware' => 'auth',
	'uses' => 'Admin\ProductController@optionautocomplete'
]);
Route::post('admin/product/optionautocomplete', [
	'as' => 'optionautocomplete',
	'middleware' => 'auth',
	'uses' => 'Admin\ProductController@optionautocomplete'
]);
// End Product

/* Start Order */
Route::group(['prefix' => '/admin/order'], function() {
    Route::get('list', [
        'as' => 'orderlist',
        'middleware' => 'auth',
        'uses' => 'Admin\OrderController@index'
    ]);
    Route::get('view/{id}', [
        'as' => 'orderview',
        'middleware' => 'auth',
        'uses' => 'Admin\OrderController@orderDetail'
    ]);
    Route::post('view/{id}', [
        'as' => 'orderupdate',
        'middleware' => 'auth',
        'uses' => 'Admin\OrderController@orderUpdate'
    ]);
	Route::get('add', [
		'as' => 'orderadd',
		'middleware' => 'auth',
		'uses' => 'Admin\OrderController@addOrder'
	]);
	Route::post('add', [
		'as' => 'doaddorder',
		'middleware' => 'auth',
		'uses' => 'Admin\OrderController@doAddOrder'
	]);
	Route::get('ajaxgetoptionproduct/{id}', [
		'as' => 'ajaxgetoptionproduct',
		'middleware' => 'auth',
		'uses' => 'Admin\OrderController@ajaxgetoptionproduct'
	]);
	Route::post('ajaxgetoptionproduct/{id}', [
		'as' => 'ajaxgetoptionproduct',
		'middleware' => 'auth',
		'uses' => 'Admin\OrderController@ajaxgetoptionproduct'
	]);
});
/* End Order*/
/*attribute option product*/
Route::group(['prefix' => '/admin/product'], function() {
	Route::get('/option', [
        'as' => 'optionlist',
        'middleware' => 'auth',
        'uses' => 'Admin\OptionController@index'
    ]);
	Route::get('/option/add', [
		'as' => 'addoption',
		'middleware' => 'auth',
		'uses' => 'Admin\OptionController@addOption'
	]);
	Route::post('/option/add', [
		'as' => 'doaddoption',
		'middleware' => 'auth',
		'uses' => 'Admin\OptionController@doAddOption'
	]);
	Route::get('/option/{id}', [
		'as' => 'viewoption',
		'middleware' => 'auth',
		'uses' => 'Admin\OptionController@viewOption'
	]);
	Route::post('/option/{id}', [
		'as' => 'editoption',
		'middleware' => 'auth',
		'uses' => 'Admin\OptionController@editOption'
	]);
	Route::get('/option/delete/{id}', [
		'as' => 'option',
		'middleware' => 'auth',
		'uses' => 'Admin\OptionController@destroyOption'
	]);
	

});
// Start Sitemap XML
//Route::get('/aaa', [
//    'as' => 'sitemap',
////    'middleware' => 'auth',
//    'uses' => 'Admin\SitemapController@index'
//]);

// End Sitemap XML