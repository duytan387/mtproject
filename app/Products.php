<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = "products";
    protected $fillable = ['name', 'slug', 'id_type', 'id_trademark', 'description', 'content', 'unit_price', 'promotion_price', 'image', 'unit', 'feature', 'neew', 'date_price', 'created_at', 'updated_at'];

    public function product_type(){
    	return $this->belongsTo('App\ProductType','id_type','id');
    }
	public function trademark(){
    	return $this->belongsTo('App\Trademark','id_trademark','id');
    }

    public function bill_detail(){
    	return $this->hasMany('App\BillDetail','id_product','id');
    }
    
    public function updateItem($input, $id) {
        return $this->where('id', $id)
                    ->update($input);
    }
}
