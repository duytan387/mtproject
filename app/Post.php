<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Post extends Model {

    //
    protected $table = 'post';

    /**
     * Define guarded columns
     *
     * @var array
     */
    protected $guarded = array('id');
    
    protected $casts = [
        'gallery' => 'array',
    ];

    //
    public function getCateSwap($id_swap) {
        return DB::table("post")->where('id_cate', $id_swap)->get();
    }

    public function getListProcess($id_process) {
        return DB::table("post")->where('id_cate', $id_process)->get();
    }

    public function getListSpecial($id_special) {
        return DB::table("post")->where('id_cate', $id_special)->get();
    }

    public function adminListPost($id_cate) {
        if($id_cate == 0){
            return DB::table("post")
                            ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
                            ->select('post.*', 'categorypost.title_vn as namecate_vn', 'categorypost.title_en as namecate_en', 'categorypost.slug_vn as cateslug_vn', 'categorypost.slug_en as cateslug_en')
                            ->orderby('id', 'desc')
                            ->paginate(10);
        }else{
            return DB::table("post")
                            ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
                            ->select('post.*', 'categorypost.title_vn as namecate_vn', 'categorypost.title_en as namecate_en', 'categorypost.slug_vn as cateslug_vn', 'categorypost.slug_en as cateslug_en')
                            ->whereIn('post.id_cate',$id_cate)
                            ->orderby('id', 'desc')
                            ->paginate(10);
        }
    }
    
    // get list post in sub category and pagination
    public function listCatePost($cate_slug, $paginate) {
        return DB::table('post')
                        ->join('categorypost as c1', 'post.id_cate', '=', 'c1.id')
                        ->join('categorypost as c2','c2.id','=','c1.parent_id')
                        ->where('c2.slug_vn', $cate_slug)
                        ->select('post.*')
                        ->orderby('created_at', 'desc')
                        ->paginate($paginate);
    }
    
    public function listPostCustom($cate_slug, $paginate) {
        return DB::table('post')
                        ->join('categorypost as c1', 'post.id_cate', '=', 'c1.id')
                        ->join('categorypost as c2','c2.id','=','c1.parent_id')
                        ->where('c2.slug_vn', $cate_slug)
                        ->select('post.title_vn', 'post.slug_vn', 'post.image')
                        ->where('post.feature',1)
                        ->orderby('post.created_at', 'desc')
                        ->paginate($paginate);
    }
    
    // get list post in category and pagination
    public function listOneCatePost($cate_slug, $paginate) {
        return DB::table('post')
                        ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
                        ->where('categorypost.slug_vn', $cate_slug)
                        ->select('post.*')
                        ->orderby('post.created_at', 'desc')
//                        ->paginate($paginate);
                ->limit($paginate)
        ->get();
    }

    public function adminListCatePost($id_cate) {
        return DB::table("post")
                        ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
                        ->select('post.*', 'categorypost.title_vn as namecate_vn', 'categorypost.title_en as namecate_en')
                        ->where('post.id_cate', $id_cate)
                        ->orderby('post.created_at', 'desc')
                        ->paginate(10);
    }

    public function getServices($cateslug) {
        return DB::table("post")
                        ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
                        ->select('post.*')
                        ->where('categorypost.slug_vn', $cateslug)
                        ->orderby('post.created_at', 'desc')
                        ->get();
    }

    public function adminEditPost($id) {
//        return DB::table("post")->where("id",$id)->update([
//            'title' => $title
//        ]);
    }

    public function adminViewPost($id) {
        return DB::table("post")->whereId($id)->first();
    }

    public static function getCurrentCate($id) {
        return DB::table("post")
                        ->select('id_cate')
                        ->whereId($id)
                        ->first();
    }

    public function getListCate() {
        // lay danh sach catepost, ngoai tru cate delete
        return DB::table("categorypost")->select("id", "title_vn", "title_en")->get();
    }

    public function getListCollection() {
        // lay danh sach catepost, ngoai tru cate delete
        return DB::table("collection")->select("id", "title_vn", "title_en")->get();
    }

    public function getListId() {
        return DB::table('post')->lists('id');
    }

    public function getListSlugVN() {
        return DB::table('post')->lists('slug_vn');
    }

    public function getListSlugEN() {
        return DB::table('post')->lists('slug_en');
    }

    public function updatePost($id, $lang, $title, $slug, $description, $content, $image, $galleries, $category, $collection, $feature, $order, $seotitle, $seokeyword, $seodescription, $checkSlug, $updated_at) {
        if ($lang == "vn") {
            return DB::table('post')
                            ->where('id', $id)
                            ->update([
                                'title_vn' => $title,
                                'slug_vn' => $slug,
                                'description_vn' => $description,
                                'content_vn' => $content,
                                'image' => $image,
                                'galleries' => $galleries,
                                'id_cate' => $category,
                                'id_collection' => $collection,
                                'feature' => $feature,
                                'orderby' => $order,
                                'seotitle' => $seotitle,
                                'seokeyword' => $seokeyword,
                                'seodescription' => $seodescription,
                                'updated_at' => $updated_at
            ]);
        } else {
            return DB::table('post')
                            ->where('id', $id)
                            ->update([
                                'title_en' => $title,
                                'slug_en' => $slug,
                                'description_en' => $description,
                                'content_en' => $content,
                                'image' => $image,
                                'id_cate' => $category,
                                'id_collection' => $collection,
                                'feature' => $feature,
                                'updated_at' => $updated_at
            ]);
        }
    }

    public function countPost() {
        return DB::table('post')->count();
    }

    public function addPost($title_vn, $title_en, $slug_vn, $slug_en, $description_vn, $description_en, $content_vn, $content_en, $image, $category, $collection, $feature, $order, $seotitle, $seokeyword, $seodescription, $created_at) {
        return DB::table('post')->insertGetId([
                    'title_vn' => $title_vn,
                    'title_en' => $title_en,
                    'slug_vn' => $slug_vn,
                    'slug_en' => $slug_en,
                    'description_vn' => $description_vn,
                    'description_en' => $description_en,
                    'content_vn' => $content_vn,
                    'content_en' => $content_en,
                    'image' => $image,
                    'id_cate' => $category,
                    'id_collection' => 0,
                    'feature' => $feature,
                    'orderby' => $order,
                    'seotitle' => $seotitle,
                    'seokeyword' => $seokeyword,
                    'seodescription' => $seodescription,
                    'created_at' => $created_at
        ]);
    }

    public function delPost($id) {
        return DB::table('post')->where('id', $id)->delete();
    }

    public function delCatePost($id) {
        return DB::table('categorypost')->where('id', $id)->delete();
    }
    
    //get list id and check id is empty
    public function getListCateSearch($id){
        $query = DB::table('categorypost as c1')
                ->join('categorypost as c2', 'c1.parent_id','=','c2.id')
                ->select('c1.*')
                ->where('c1.parent_id',$id)
                ->lists('c1.id');
        (empty($query))?$query[0] = $id:$query;
        return $query;
    }

    public function searchPost($lang, $keyword, $id_cate) {
//        echo $id_cate;exit;
        if ($lang == "en") {
            if($id_cate[0] == 0){
            return DB::table('post')
                            ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
                            ->select('post.*', 'categorypost.title_en as namecate_en', 'categorypost.title_en as namecate_en')
                            ->where('post.title_en', 'like', '%' . $keyword . '%')
//                            ->whereIn('post.id_cate', $id_cate)
                            ->orderby('created_at', 'desc')
                            ->paginate(15);
            }else{
                return DB::table('post')
                            ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
                            ->select('post.*', 'categorypost.title_en as namecate_en', 'categorypost.title_en as namecate_en')
                            ->where('post.title_en', 'like', '%' . $keyword . '%')
                            ->whereIn('post.id_cate', $id_cate)
                            ->orderby('created_at', 'desc')
                            ->paginate(15);
            }
        } else {
            if($id_cate[0] == 0){
                return DB::table('post')
                                ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
                                ->select('post.*', 'categorypost.title_vn as namecate_vn', 'categorypost.title_vn as namecate_vn')
                                ->where('post.title_vn', 'like', '%' . $keyword . '%')
//                                ->whereIn('post.id_cate', $id_cate)
                                ->orderby('created_at', 'desc')
                                ->paginate(15);
            }else{
                return DB::table('post')
                                ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
                                ->select('post.*', 'categorypost.title_vn as namecate_vn', 'categorypost.title_vn as namecate_vn')
                                ->where('post.title_vn', 'like', '%' . $keyword . '%')
                                ->whereIn('post.id_cate', $id_cate)
                                ->orderby('created_at', 'desc')
                                ->paginate(15);
            }
        }
        
        // chua co phan trang cho search
    }

    /* public function searchPost($lang, $id_cate, $keyword) {
      //        exit('dsadas');
      if ($lang == "en") {
      if (empty($id_cate)) {
      return DB::table('post')
      ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
      ->select('post.*', 'categorypost.title_en as namecate_en', 'categorypost.title_en as namecate_en')
      ->where('post.title_en', 'like', '%' . $keyword . '%')
      ->orderby('created_at', 'desc')
      ->paginate(15);
      } else {
      if ($id_cate != 0) {
      return DB::table('post')
      ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
      //                                ->where('post.title_en', 'like', '%' . $keyword . '%')
      ->where('categorypost.parent_id', $id_cate)
      //                            ->where('categorypost.id', 'post.id_cate')
      ->select('post.*', 'categorypost.title_en as namecate_en', 'categorypost.title_en as namecate_en')
      ->orderby('created_at', 'desc')
      //                            ->toSql();
      ->paginate(15);
      //                    exit;
      } else {
      return DB::table('post')
      ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
      //                                ->where('post.title_en', 'like', '%' . $keyword . '%')
      ->where('categorypost.parent_id', $id_cate)
      //                            ->where('categorypost.id', 'post.id_cate')
      ->select('post.*', 'categorypost.title_en as namecate_en', 'categorypost.title_en as namecate_en')
      ->orderby('created_at', 'desc')
      //                            ->toSql();
      ->paginate(15);
      }
      }
      } else {
      if (empty($id_cate)) {
      return DB::table('post')
      ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
      ->select('post.*', 'categorypost.title_vn as namecate_vn', 'categorypost.title_vn as namecate_vn')
      ->where('post.title_vn', 'like', '%' . $keyword . '%')
      ->orderby('created_at', 'desc')
      ->paginate(15);
      } else {
      return DB::table('post')
      ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
      ->select('post.*', 'categorypost.title_vn as namecate_vn', 'categorypost.title_vn as namecate_vn')
      ->where('post.title_vn', 'like', '%' . $keyword . '%')
      ->where('post.id_cate', $id_cate)
      ->orderby('created_at', 'desc')
      ->paginate(15);
      }
      }
      //        if ($lang == "en") {
      //            return DB::table('post')
      //                            ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
      //                            ->select('post.*', 'categorypost.title_en as namecate_en', 'categorypost.title_en as namecate_en')
      //                            ->where('post.id_cate', $id_cate)
      //                            ->where('post.title_en', 'like', '%' . $keyword . '%')
      //                            ->orderby('id', 'desc')
      //                            ->paginate(15);
      //        } else {
      //            return DB::table('post')
      //                            ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
      //                            ->select('post.*', 'categorypost.title_vn as namecate_vn', 'categorypost.title_vn as namecate_vn')
      //                            ->where('post.id_cate', $id_cate)
      //                            ->where('post.title_vn', 'like', '%' . $keyword . '%')
      //                            ->orderby('id', 'desc')
      //                            ->paginate(15);
      //        }
      } */

    public function getListCategoryParent($parent_id) {
        return DB::table('categorypost')
                        ->where('parent_id', $parent_id)
                        ->orderBy('title_vn', 'DESC')
                        ->get();
    }

    public function getListCategoryChild($parent_id) {
        return DB::table('categorypost')
                        ->where('parent_id', $parent_id)
                        ->orderBy('title_vn', 'DESC')
                        ->get();
    }

    public function getListSubCategory($parent_id = 0) {
        return DB::table('categorypost')
                        ->where('parent_id', $parent_id)
                        ->get();
    }

    public function viewCategoryPost($id) {
        return DB::table('categorypost')->whereId($id)->first();
    }

    public function getListIdCategory() {
        return DB::table('categorypost')->lists('id');
    }

    public function editCategoryPost($id, $lang, $title, $slug, $description, $parent_id, $image, $updated_at) {
        if ($lang == "en") {
            return DB::table('categorypost')
                            ->where('id', $id)
                            ->update([
                                'title_en' => $title,
                                'slug_en' => $slug,
                                'description_en' => $description,
                                'parent_id' => $parent_id,
                                'image' => $image,
                                'updated_at' => $updated_at
            ]);
        } else {
            return DB::table('categorypost')
                            ->where('id', $id)
                            ->update([
                                'title_vn' => $title,
                                'slug_vn' => $slug,
                                'description_vn' => $description,
                                'parent_id' => $parent_id,
                                'image' => $image,
                                'updated_at' => $updated_at
            ]);
        }
    }

    public function addCatePost($lang, $title, $slug, $description, $parent_id, $image, $created_at) {
        if ($lang == "en") {
            return DB::table('categorypost')->insertGetId([
                        'title_en' => $title,
                        'title_vn' => $title . " vn",
                        'slug_en' => $slug,
                        'slug_vn' => $slug . "-vn",
                        'description_en' => $description,
                        'parent_id' => $parent_id,
                        'image' => $image,
                        'created_at' => $created_at
            ]);
        } else {
            return DB::table('categorypost')->insertGetId([
                        'title_vn' => $title,
                        'title_en' => $title . " en",
                        'slug_vn' => $slug,
                        'slug_en' => $slug . "-en",
                        'description_vn' => $description,
                        'parent_id' => $parent_id,
                        'image' => $image,
                        'created_at' => $created_at
            ]);
        }
    }

    public function listItem($limit) {
        return DB::table('post')
                        ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
//                        ->where('categorypost.id', 7)
                        ->where('categorypost.slug_vn', 'san-pham')
                        ->select('post.*')
                        ->limit($limit)
                        ->orderBy('post.created_at', 'desc')
                        ->get();
    }

    public function postFirst($category) {
        return DB::table('post')
                        ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
                        ->where('categorypost.slug_vn', $category)
                        ->where('feature', 1)
                        ->select('post.*')
                        ->orderby('orderby', 'asc')
                        ->first();
    }
    
    public function postFeature($category, $limit) {
        return DB::table('post')
                        ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
                        ->where('categorypost.slug_vn', $category)
                        ->where('feature', 0)
                        ->select('post.*')
                        ->orderby('orderby', 'asc')
                        ->limit($limit)
                        ->get();
    }

    public function getPost($slug) {
        return DB::table("post")->where('slug_vn', $slug)->first();
    }

    public function listProject() {
        return DB::table('post')
                        ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
                        ->where('categorypost.slug_vn', 'du-an')
                        ->select('post.*')
                        ->orderBy('post.created_at', 'desc')
                        ->limit(8)
                        ->get();
    }

    public function getProjectCate($id) {
        return DB::table('categorypost')->where('parent_id', $id)->orderBy('orderby', 'asc')->get();
    }

    public function listPost($cate_slug) {
        return DB::table('post')
                        ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
                        ->where('categorypost.slug_vn', $cate_slug)
                        ->where('post.id_cate','categorypost.parent_id')
                        ->select('post.*', 'categorypost.title_vn as category_vn', 'categorypost.title_en as category_en', 'categorypost.slug_vn as categoryslug_vn', 'categorypost.slug_en as categoryslug_en')
                        ->orderby('created_at', 'desc')
//                ->toSql();
                        ->get();
//                        ->paginate($limit);
    }

    public function listPostProjectCate($slug, $limit) {
//        echo $slug;exit;
        return DB::table('post')
                        ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
                        ->where('categorypost.slug_vn', $slug)
                        ->select('post.*')
                        ->orderby('post.created_at', 'desc')
                        ->paginate($limit);
    }

    public function getPostSlug($slug) {

        $postData = DB::table('post')
                ->where('slug_' . trans('home.lang'), $slug)
                ->first();
        return $postData;
    }

    public function getListProduct($slug) {
        return DB::table("post")
                        ->select("post.*", "categorypost.slug_vn as cateslug_vn", "categorypost.slug_en as cateslug_en")
                        ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
                        ->where('categorypost.slug_' . trans('home.lang'), $slug)
                        ->get();
    }

    public function getPostProjectCollection($id) {
        return DB::table('post')
                        ->where('id', $id)
                        ->first();
    }
    
    public function getPostFeature($slug) {
        return DB::table("post")
                        ->select("post.*")
                        ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
                        ->orderby('created_at', 'desc')
                        ->where('categorypost.slug_vn',$slug)
                        ->first();
    }

    public function listPost01($id_cate, $limit) {
        return DB::table("post")
                        ->where('id_cate', $id_cate)
                        ->orderby('created_at', 'desc')
                        ->paginate($limit);
//                ->get();
    }
    
    public function listCategoryPost($slug) {
        return DB::table("post")
                        ->select("post.*")
                        ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
                        ->where('categorypost.slug_vn', $slug)
                        ->orderby('created_at', 'desc')
                        ->limit(3)
                ->get();
    }

    public function listPost02($id_cate, $slug, $limit) {
        return DB::table("post")
                        ->where('id_cate', $id_cate)
                        ->orderby('created_at', 'desc')
                        ->having('slug_vn', '<>', $slug)
                        ->limit($limit)
                        ->get();
    }

    public function relatedProduct($slug1, $slug2, $limit) {
        return DB::table("post")
                        ->select("post.*", "categorypost.slug_vn as cateslug_vn", "categorypost.slug_en as cateslug_en")
                        ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
                        ->where('post.slug_' . trans('home.lang'), '<>', $slug2)
                        ->where('categorypost.slug_' . trans('home.lang'), $slug1)
                        ->limit($limit)
                        ->get();
    }

    public function listPostNewsFeature() {
        return DB::table("post")
                        ->where('id_cate', 3)
                        ->where('feature', 1)
                        ->orderby('created_at', 'desc')
                        ->paginate(5);
    }

    // Show menu con project
    public function listCateProject() {
        return DB::table("categorypost")
                        ->where('parent_id', 7)
                        ->orderby('orderby', 'desc')
                        ->get();
    }

    public function getCollection($slug) {
        return DB::table("post")
                        ->select('id_collection')
                        ->where('slug_vn', '=', $slug)
                        ->first();
    }

    public function listCollectionProject($slug, $id_collection) {
        return DB::table("post")
                        ->where('slug_vn', '!=', $slug)
                        ->where('id_collection', $id_collection)
                        ->where('id_collection', '>', 1)
                        ->get();
    }

    public function updateSlugPost($id, $slug_vn, $slug_en) {
        if (session('locale') == "vn") {
            return DB::table('post')->where('id', $id)->update([
                        'slug_vn' => $slug_vn
            ]);
        } else {
            return DB::table('post')->where('id', $id)->update([
                        'slug_en' => $slug_en
            ]);
        }
    }

    public function getSlug($id, $lang) {
        if ($lang == "vn") {
            return DB::table('post')->where('id', $id)->select('slug_vn')->first();
        } else {
            return DB::table('post')->where('id', $id)->select('slug_en')->first();
        }
    }
    
    public function getCate($slug) {
        return DB::table('post')
                ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
                ->select('post.id_cate', 'categorypost.title_vn as catetitle_vn')
                ->where('post.slug_vn', $slug)
                ->first();
    }
    
    public function getPostRelated($post_slug, $id_cate, $limit) {
        return DB::table("post")
                        ->where('slug_vn', '!=', $post_slug)
                        ->where('id_cate', $id_cate)
                        ->limit($limit)
                        ->get();
    }
    
    public function getPostMetaTag($slug_vn){
        return DB::table("post")
                        ->where('slug_vn', $slug_vn)
                        ->first();
    }
    
    public function sitemap(){
        return DB::table("post")
                        ->join('categorypost', 'post.id_cate', '=', 'categorypost.id')
                        ->select('post.*', 'categorypost.title_vn as namecate_vn', 'categorypost.title_en as namecate_en', 'categorypost.slug_vn as cateslug_vn', 'categorypost.slug_en as cateslug_en')
                        ->orderby('created_at', 'desc')
                        ->get();
    }
    
    public function setView($id, $count_views){
        return DB::table('post')
                ->where('id', $id)
                            ->update([
                                'count_views' => $count_views
            ]);
    }

}
