<?php

namespace App\Providers;
use App\Cart;
use App\ProductType;
use Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        //
        
//        view()->composer('pages.cart',function($view){
//            $loai_sp = ProductType::all();
//            
//            $view->with('loai_sp',$loai_sp);
//        });

//        view()->composer('pages.cart',function($view){
//            if(Session('cart')){
//                $oldCart = Session::get('cart');
//                $cart = new Cart($oldCart);
//                $view->with(['cart'=>Session::get('cart'), 'product_cart'=>$cart->items,'totalPrice'=>$cart->totalPrice,'totalQty'=>$cart->totalQty]);
//            }
//            
//        });
         view()->composer('pages.cart','App\Http\ViewComposers\ProfileComposer');
    }

    /**
     * Register any application services.
     *
     * This service provider is a great spot to register your various container
     * bindings with the application. As you can see, we are registering our
     * "Registrar" implementation here. You can add your own bindings too!
     *
     * @return void
     */
    public function register() {
        $this->app->bind(
                'Illuminate\Contracts\Auth\Registrar', 'App\Services\Registrar'
        );
    }

}
