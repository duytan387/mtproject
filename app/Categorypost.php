<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Categorypost extends Model {

    //


    protected $table = 'categorypost';
//
//    public function parent()
//    {
//        return $this->belongsTo('App\Categorypost', 'parent_id');
//    }
//
//    public function children() {
//        return $this->hasMany('App\Categorypost', 'parent_id');
//    }
//
//    public function getParentsAttribute() {
//        $parents = collect([]);
//        $parent = $this->parent;
//        while (!is_null($parent)) {
//            $parents->push($parent);
//            $parent = $parent->parent;
//        }
//        return $parents;
//    }

    public static function getListCategoryParent($parent_id = 0) {
        return DB::table('categorypost')->where('parent_id',$parent_id)->get();
    }
    
    public function getListIdCategory() {
        return DB::table('categorypost')->lists('id');
    }
}
