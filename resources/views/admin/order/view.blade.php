<?php use App\Http\Controllers\Admin\OrderController;?>
@extends('admin.dashboard')

@section('title', 'Order')

@section('assets')

@endsection

@section('content')

<div class="page-content">

    <div class="container-fluid">



        <!-- Breadcromb Row Start -->

        <div class="row">

            <div class="col-md-12">

                <div class="breadcromb-area">

                    <div class="row">

                        <div class="col-md-6 col-sm-6">

                            <div class="seipkon-breadcromb-left">

                                <h1>Khách Hàng: {!!$customer['name']!!}</h1>

                            </div>

                        </div>

                        <div class="col-md-6 col-sm-6">

                            <div class="seipkon-breadcromb-right">

                                <ul>

                                    <li><a href="{!!url('admin/dashboard')!!}">home</a></li>

                                    <li><a href="{!!url('admin/order/list')!!}">Order</a></li>

                                    <li>Order Detail</li>

                                </ul>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <!-- End Breadcromb Row -->



        @if(Session::has('status'))

                <!--<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>-->

        <div class="alert alert-success fade in alert-dismissible" style="margin-top:18px;">

            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>

            {!!Session::get('status')!!}

        </div>

        @endif



        <!-- Pages Table Row Start -->

        <div class="row">

            <div class="col-md-12">

                <div class="page-box">

                    <form action="{!!action('Admin\OrderController@orderUpdate', $orders['id'])!!}" method="post" enctype="multipart/form-data">

                        <input type="hidden" name="_token" value="{!!csrf_token()!!}">

                        <div class="row">

                            <div class="col-md-12">

                                <div class="create-page-left">

                                    <div class="form-group">

                                        <div class="create-page-left">

                                            <div class="row">

                                                <div class="col-md-6 col-sm-6">

                                                    <div class="single-profile-bio">

                                                        <h3>Thông Tin Khách Hàng</h3>

                                                        

														<div class="form-group">

                                                            <label>Tên khách hàng:</label>
															<select placeholder="Chọn hoặc nhập tên để tìm khach hang" class="form-control" id="name" name="customer"  >
																<option value="">Chọn khách hàng</option>
																@foreach ($customers as $item)  
																
																<option value="{{$item->id}}" @if($customer['id']==$item->id)selected  @endif >{{$item->name}} </option>                                    @endforeach								
															</select>
                                                            <br/>

                                                            <p id="input_feedback1"></p>

                                                        </div>

														<div class="form-group">

                                                            <label>Địa Chỉ:</label>

                                                            <textarea id="seotitle" class="form-control" name="address" placeholder="SEO name" >{{ $customer['address'] }}</textarea>

                                                            <p id="input_feedback1"></p>

                                                        </div>

														<div class="form-group">

                                                            <label>Điện Thoại:</label>

                                                            <input id="phone" type="text" name="phone" placeholder="SEO Keyword" value="{{ $customer['phone'] }}" ><br/>

                                                            <p id="input_feedback1"></p>

                                                        </div>

														<div class="form-group">

                                                            <label>Email:</label>

                                                            <input id="email" type="text" name="email" placeholder="SEO Keyword" value="{{ $customer['email'] }}" ><br/>

                                                            <p id="input_feedback1"></p>

                                                        </div>

                                                       

                                                    </div>

                                                </div>

                                                <div class="col-md-6 col-sm-6">

                                                    <div class="single-profile-bio">

                                                        <h3>Thông Tin Đơn Hàng</h3>

														<div class="form-group">

                                                            <label>Ngày Đặt Hàng:</label>

                                                            <input id="order_date" type="text" name="order_date" placeholder="SEO Keyword" value="{{ $orders['order_date'] }}" class='datepicker'><br/>

                                                            <p id="input_feedback1"></p>

                                                        </div>

														<div class="form-group">

                                                            <label>Ghi Chú:</label>

                                                            <textarea id="notes" class="form-control" name="notes" placeholder="SEO name" >{{ $orders['notes'] }}</textarea>

                                                            <p id="input_feedback1"></p>

                                                        </div>

														<div class="form-group">

															<label class="control-label" for="select">Phương Thức Thanh Toán:</label>

															<select class="form-control" id="select" name="payment_method">

																<option value="Chuyển Khoản" <?php echo ($orders['payment_method'] == 'Chuyển Khoản') ? "selected" : "" ?>>Chuyển Khoản</option>

																<option value="Tiền Mặt" <?php echo ($orders['payment_method'] == 'Tiền Mặt') ? "selected" : "" ?>>Tiền Mặt</option>

															</select>

														</div>
														<div class="form-group">
															<label class="control-label" for="select">Tình Trạng Đơn Hàng:</label>
															<select class="form-control" id="status" name="status" name="category">
																<option value="">Chưa Phân Loại</option>
																<option value="chưa giải quyết" <?php echo ($orders['status'] === "chưa giải quyết")?"selected":""?>>Chưa Giải Quyết</option>
																<option value="đang xử lý" <?php echo ($orders['status'] === "đang xử lý")?"selected":""?>>Đang Xử Lý</option>
																<option value="đã được xử lý" <?php echo ($orders['status'] === "đã được xử lý")?"selected":""?>>Đã Được Xử Lý</option>
																<option value="thất bại" <?php echo ($orders['status'] === "thất bại")?"selected":""?>>Từ Chối</option>
																<option value="hoàn thành" <?php echo ($orders['status'] === "hoàn thành")?"selected":""?>>Hoàn Thành</option>
															</select>
														</div>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>



                                    <div class="table-responsive">

                                        <div class="single-profile-bio">

                                            <h3>Thông Tin Sản Phẩm</h3>

											<input type="hidden" id="getnum" name="getnum" value="{{$countpro}}" class="form-control"/>

											<div class="title-table">

												<div class="form-group col-md-4">		

													<label>Tên sản phẩm </span></label>

												</div>
												<div class="form-group col-md-2">		
													<label>Tuỳ chọn</label>
												</div>
												<div class="form-group col-md-1">		

													<label>Số lượng </span></label>

												</div>

												<div class="form-group col-md-2">		

													<label>Đơn giá </span></label>

												</div>

												<div class="form-group col-md-2">		

													<label>Thành tiền </span></label>

												</div>
												<div class="form-group col-md-1">		
													<label><i class="fa fa-cog"></i></label>
												</div>

											</div>

											@if($orderDetail)

                                            @foreach($orderDetail as $count => $detail)

											 <?php if($count == 0) echo '<div class="blockpropa">'; else echo '<div class="blockaddpro">'; ?>
												<input type="hidden" name="product[{{$count}}][orderdetail_id]" value="{{$detail['id']}}" />
												<div class="blockpro block{{$count+1}}">

													<div class="form-group col-md-4">               

														<select placeholder="Chọn hoặc nhập tên để tìm sản phẩm"  class="form-control " data-id="{{$count+1}}" id="product{{$count+1}}" name="product[{{$count}}][product_id]" data-plugin-selectTwo >									

														@foreach ($products as $product)  

														<?php

															if($product->promotion_price == 0) $price = $product->unit_price;

															if($product->promotion_price > 100) $price =  $product->promotion_price;

															if($product->promotion_price < 100 && $product->promotion_price > 0) $price = ($product->unit_price * $product->promotion_price)/100; 

														?>														

														<option <?php if($detail['product_id'] == $product->id) echo 'selected'  ?> value="{{$product->id}}" data-price="{{$price}}">{{$product->name}} </option>                                    @endforeach								

														</select>

													</div>	
													<div class="form-group col-md-2 option{{$count+1}}">
														<?php 
															$items = OrderController::getOrderOption($detail['product_id'],$detail['order_id']);
															//echo '<pre>';print_r($item);exit;
															
														?>
															@foreach($items as $key=>$item)
															@if($item['type']=='select')
																<select name="product[{{$count}}][option][{{$key}}][product_option_value_id]" id="input-option30" class="opva {{$count}}optionproduct{{$key}} optionproduct form-control" data-option="{{$key}}" data-pt="{{$count}}">        
																	<option value="">Choose {{$item['name']}}</option>
																	<?php 
																		$op = OrderController::getProductOptionValue($item['product_option_id']); 
																		foreach($op as $itm){
																		 ?>
																		<option <?php if($itm['name'] == $item['value']) echo 'selected'; ?> value="{{$itm['product_option_value_id']}}">{{$itm['name']}}</option>
																		<?php } ?>
																</select>
																<input type="hidden" name="product[{{$count}}][option][{{$key}}][type]" value="select" />
																<input type="hidden" name="product[{{$count}}][option][{{$key}}][name]" value="{{$item['name']}}" />
																<input type="hidden" class="{{$count}}optionvalue{{$key}}" name="product[{{$count}}][option][{{$key}}][value]" value="{{$item['value']}}" />
																<input type="hidden" name="product[{{$count}}][option][{{$key}}][product_option_id]" value="{{$item['product_option_id']}}" />
																<input type="hidden" name="product[{{$count}}][option][{{$key}}][order_option_id]" value="{{$item['id']}}" />
															 @endif
														@endforeach
														
													</div>

													<div class="form-group col-md-1">

														<input id="sl{{$count+1}}" type="number" data-id="{{$count+1}}" min="1" name="product[{{$count}}][sl]" value="{{$detail['quantity']}}" class="form-control inputsl" placeholder="Số lượng" required/>

													</div>

													<div class="form-group col-md-2">

														<input id="dg{{$count+1}}" type="number" min="1" name="product[{{$count}}][dg]" value="{{$detail['unit_price']}}" class="form-control inputdg" readonly/>

													</div>

													<div class="form-group col-md-2">	

														<input id="tt{{$count+1}}" type="number" min="1" name="product[{{$count}}][tt]" value="{{$detail['quantity'] * $detail['unit_price']}}" class="form-control thanhtien inputtt" readonly/>

													</div>
													<div class="form-group col-md-1"><span class="block-trash subpronew" id="del{{$count+1}}"><i class="fa fa-trash"></i></span></div>

												</div>

												</div>

												@endforeach

												@endif

												<?php if($countpro == 1) echo '

												<div class="blockaddpro">

												</div>'; ?>

												<div class="form-group " style="width:100%;display: inline-block;">

													<div type="text" id="addpro" class="btn btn-primary" value="Thêm sản phẩm">Thêm sản phẩm</div>

												</div>

												<div class="col-md-8 col-md-offset-4 order-table">

													 <table class="table text-right">

														<tbody>

														   <tr id="subtotal">

															  <td><span class="bold">Tổng :</span>

															  </td>
																
															  <td class="subtotal"><b>{{$orders['subtotal']}} </b>đ<input type="hidden" id="inputsubtotal" name="subtotal" value="{{$orders['subtotal']}}"></td>

														   </tr>

														   <tr id="discount_area">

															  <td>

																 <div class="row">

																	<div class="col-md-7">

																	   <span class="bold">

																		Chiếu khấu                         </span>

																	</div>

																	<div class="col-md-5">

																		<div class="input-group" id="discount-total">

																			<input type="number" value="{{$orders['discount_percent']}}" class="form-control pull-left input-discount" min="0" id="discount" name="discount" aria-invalid="false">

																		</div>

																	</div>

																 </div>

															  </td>
																
															  <td class="discount-total"><b>0.000 </b>đ</td>

														   </tr>

														   

														   <tr>

															  <td><span class="bold">Thành tiền :</span>

															  </td>

															  <td class="total"><b>{{$orders['total_price']}} </b>đ<input type="hidden" id="inputtotal" name="total" value="{{$orders['total_price']}}"></td>

														   </tr>

														</tbody>

													 </table>

												  </div>

                                           

                                        </div>

                                    </div>







                                </div>



                            </div>




                        </div>

                        <div class="form-layout-submit">

                            <p>

                                <button type="submit" class="btn btn-success" ><i class="fa fa-check"></i>Update</button>

                                <a class="btn btn-default" href="{!!url('admin/order/list')!!}" role="button"><i class="fa fa-chevron-left"></i> Back</a>

                            </p>

                        </div>

                    </form>

                </div>



            </div>

        </div>

        <!-- End Pages Table Row -->



    </div>

</div>



<!-- Footer Area Start -->

<footer class="seipkon-footer-area">

    @include('admin/layouts/footer')

</footer>

<!-- End Footer Area -->

@endsection

@section('assetjs')

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>



  <script>

  $( "#order_date" )

        .datepicker({

          changeMonth: true,

          changeYear: true,

		  dateFormat: "dd-mm-yy",

          numberOfMonths: 1

        });

	$( "#duedate" ).datepicker({

        changeMonth: true,

        changeYear: true,

		dateFormat: "dd-mm-yy",

        numberOfMonths: 1

      });

	 $.datepicker.regional["vi-VN"] =

	{

		closeText: "Đóng",

		prevText: "Trước",

		nextText: "Sau",

		currentText: "Hôm nay",

		monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],

		monthNamesShort: ["Một", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy", "Tám", "Chín", "Mười", "Mười một", "Mười hai"],

		dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],

		dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],

		dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],

		weekHeader: "Tuần",

		dateFormat: "dd/mm/yy",

		firstDay: 1,

		isRTL: false,

		showMonthAfterYear: false,

		yearSuffix: ""

	};



	$.datepicker.setDefaults($.datepicker.regional["vi-VN"]);

	$("#addpro").click(function(){
	  
	  var getnumer = $('#getnum').val();
	  getnum = parseInt(getnumer) + 1;
	  html = '<div class="blockpro block'+getnum+'"><div class="form-group col-md-4"><select placeholder="Chọn hoặc nhập tên để tìm sản phẩm" data-plugin-selectTwo class="form-control" data-id="'+getnum+'" id="product'+getnum+'" name="product['+getnumer+'][product_id]" class="form-control"><option value="" data-price="">Chọn sản phẩm</option> <?php foreach($product1 as $product){ ?> <option data-price="<?php 
		if($product->promotion_price == '') $price = $product->unit_price;
		if($product->promotion_price > 100 ) $price =  $product->promotion_price;
		if($product->promotion_price < 100 && $product->promotion_price > 0) $price = ($product->unit_price * $product->promotion_price)/100; 
		echo $price; ?>" value="<?php echo $product->id; ?>" ><?php echo $product->name; ?>  </option><?php } ?> </select></div><div class="form-group col-md-2 option'+getnum+'"></div><div class="form-group col-md-1"><input id="sl'+getnum+'" data-id="'+getnum+'" type="number" min="1" name="product['+getnumer+'][sl]" value="1" class="form-control inputsl" required/></div><div class="form-group col-md-2"><input id="dg'+getnum+'" type="number" min="1" name="product['+getnumer+'][dg]" value="" class="form-control inputdg" readonly/></div><div class="form-group col-md-2">	<input id="tt'+getnum+'" type="number" min="1" name="product['+getnumer+'][tt]" value="" class="form-control thanhtien inputtt" readonly/></div><div class="form-group col-md-1"><span class="block-trash subpronew" id="del'+getnum+'"><i class="fa fa-trash"></i></span></div></div>';
	 
	  $('.blockaddpro').append(html);
	  $('#getnum').val(getnum);
	  $('#product'+getnum).select2();

	});

	$("#subpro").click(function(){

		$('.blockaddpro .blockpro:last-child').remove();

		var getnum = $('#getnum').val();

		if(getnum > 1) getnum = getnum -1;

		$('#getnum').val(getnum);

		subtotal();

	});
	$(document).on("click",'.single-profile-bio .subpronew', function(){
	console.log('cnsdjc');
		$(this).parent().parent().remove();
		subtotal();
	});
	function formatprice(num) {

		// var p = num.toFixed(0).split(".");

		// return  p[0].split("").reverse().reduce(function(acc, num, i, orig) {

			// return  num=="-" ? acc : num + (i && !(i % 3) ? "." : "") + acc;

		// }, "");

		

		return num;

	}

	function subtotal() {

		var total = 0;

		var thanhtien = document.getElementsByClassName('thanhtien');

		for (var i = 0; i < thanhtien.length; ++i) {

		if (!isNaN(parseInt(thanhtien[i].value)) )

		total += parseInt(thanhtien[i].value);  

		}

		$('#inputsubtotal').val(total);

		$('.subtotal b').html(total);

		

		var discount = $('#discount').val();

		if(discount >0 && discount<100){

			var end = total - (total*discount)/100;

			var discounttotal = (total*discount)/100;

			$('.discount-total b').html(discounttotal);

		}

		else{ var end = total - discount;

			$('.discount-total b').html(formatprice(discount));

		}

		

		$('#inputtotal').val(end);

		$('.total b').html(formatprice(end));

	}

	$(document).on("change",'#discount', function(){

	   

	   var sub_total = $('#inputsubtotal').val();

	   var discount = $('#discount').val();

		if(discount >0 && discount<100){

			var discounttotal = (sub_total*discount)/100;

			$('.discount-total b').html(formatprice(discounttotal));

		}

		else $('.discount-total b').html(formatprice(discount));

		 subtotal();

	});

	$(document).on("change",'.inputsl', function(){

		var id = $(this).attr('data-id');

		var sl = $(this).val();

		var dg = $('#dg'+id+'').val();

		$('#tt'+id+'').val(formatprice(dg*sl));

		subtotal();

	});
	$(document).on("change",'.optionproduct', function(){
		//lấy value option product
		//var id = $(this).attr('data-id');
		var option = $(this).attr('data-option');
		var pt = $(this).attr('data-pt');
		
		var value = $("."+pt+"optionproduct"+option+" option:selected").text();
		console.log('value:'+value);
		$('.'+pt+'optionvalue'+option+'').val(value);
		
	});
	$(document).on("change",'select[name^=product]', function(){

	     var id = $(this).attr('data-id');console.log(id);
		 id1 = id - 1;
	     var idproduct = $(this).val();
		 var data = $(this).find(':selected').attr('data-price');console.log(data);

		 $('#dg'+id+'').val(formatprice(data));

		 var sl = $('#sl'+id+'').val();

	     $('#tt'+id+'').val(formatprice(parseInt(data)*parseInt(sl)));
		 /*cal ajax option */
		
		 $.ajax({
            type: 'get', 
            url: '{!!url("admin/order/ajaxgetoptionproduct")!!}/' + idproduct, 
            data: idproduct, 
        }).done(function(res) { 
            console.log('mss:'+res);
			var getnumer = $('#getnum').val();	
			html ='';
            for (var i in res) {
				console.log('mssbb:'+res[i].option);
		
					if(res[i].option.length > 0){
						html = '';
						for (var k in res[i].option) {
							option = res[i].option[k];
							if (option['type'] == 'select') {
								html += '    <select name="product['+id1+'][option]['+k+'][product_option_value_id]" id="input-option' + option['product_option_id'] + '" class="opva '+id1+'optionproduct'+k+' optionproduct form-control" data-option="'+k+'" data-pt="'+id1+'">';
								html += '      <option value="">Choose '+option['name']+'</option>';

								for (j = 0; j < option['product_option_value'].length; j++) {
									option_value = option['product_option_value'][j];

									html += '<option value="' + option_value['product_option_value_id'] + '">' + option_value['option_value_name'];
									// ko tinh giá
									// if (option_value['price']) {
										// html += ' (' + option_value['price_prefix'] + option_value['price'] + ')';
									// }

									html += '</option>';
								}
								html += '   <input type="hidden" name="product['+id1+'][option]['+k+'][type]" value="select" />';
								html += '   <input type="hidden" name="product['+id1+'][option]['+k+'][name]" value="'+option['name']+'" />';
								html += '   <input type="hidden" class="'+id1+'optionvalue'+k+'" name="product['+id1+'][option]['+k+'][value]" value="" />';
								html += '   <input type="hidden" name="product['+id1+'][option]['+k+'][product_option_id]" value="' + option['product_option_id'] + '" />';
								html += '    </select>';
							}
							if (option['type'] == 'radio') {
								
								html += '    <select name="product['+id1+'][option]['+k+'][product_option_value_id]" id="input-option' + option['product_option_id'] + '" data-option="'+k+'" class="'+id1+'optionproduct'+k+' optionproduct form-control radiooption'+id1+'" data-pt="'+id1+'">';
								html += '      <option value="">Choose '+option['name']+'</option>';

								for (j = 0; j < option['product_option_value'].length; j++) {
									option_value = option['product_option_value'][j];

									html += '<option value="' + option_value['product_option_value_id'] + '">' + option_value['name'];

									// if (option_value['price']) {
										// html += ' (' + option_value['price_prefix'] + option_value['price'] + ')';
									// }

									html += '</option>';
								}
								html += '   <input type="hidden" name="product['+id1+'][option]['+k+'][type]" value="radio" />';
								html += '   <input type="hidden" name="product['+id1+'][option]['+k+'][name]" value="'+option['name']+'" />';
								html += '   <input type="hidden" class="'+id1+'optionvalue'+k+'" name="product['+id1+'][option]['+k+'][value]" value="" />';
								html += '   <input type="hidden" name="product['+id1+'][option]['+k+'][product_option_id]" value="' + option['product_option_id'] + '" />';
								html += '    </select>';
							}

							if (option['type'] == 'checkbox') {
								
								for (j = 0; j < option['product_option_value'].length; j++) {
									option_value = option['product_option_value'][j];

									html += '<div class="checkbox">';

									html += '  <label><input type="checkbox" name="product['+id1+'][option]['+k+'][product_option_value_id][]" value="' + option_value['product_option_value_id'] + '" /> ' + option_value['name'];

									// if (option_value['price']) {
										// html += ' (' + option_value['price_prefix'] + option_value['price'] + ')';
									// }
									
								}
								html += '   <input type="hidden" name="product['+id1+'][option]['+k+'][product_option_id]" value="' + option['product_option_id'] + '" />';
								html += '    </div>';
							}

							if (option['type'] == 'image') {
								html += '    <select name="product['+id1+'][option]['+k+'][product_option_value_id]" id="input-option' + option['product_option_id'] + '" class="form-control">';
								html += '      <option value="">Choose '+option['name']+'</option>';

								for (j = 0; j < option['product_option_value'].length; j++) {
									option_value = option['product_option_value'][j];

									html += '<option value="' + option_value['product_option_value_id'] + '">' + option_value['name'];

									if (option_value['price']) {
										html += ' (' + option_value['price_prefix'] + option_value['price'] + ')';
									}

									html += '</option>';
								}
								html += '   <input type="hidden" name="product['+id1+'][option]['+k+'][product_option_id]" value="' + option['product_option_id'] + '" />';
								html += '    </select>';
							}

							if (option['type'] == 'text') {
								html += '  <div class="col-sm-10"><input type="text" name="product['+id1+'][option]['+k+'][product_option_value_id]" value="' + option['value'] + '" id="input-option' + option['product_option_id'] + '" class="form-control" placeholder="' + option['name'] + '" /></div>';
								html += '   <input type="hidden" name="product['+id1+'][option]['+k+'][product_option_id]" value="' + option['product_option_id'] + '" />';
							}

							if (option['type'] == 'textarea') {
								html += '  <div class="col-sm-10"><textarea name="product['+id1+'][option]['+k+'][product_option_value_id]" rows="5" id="input-option' + option['product_option_id'] + '" class="form-control" placeholder="' + option['name'] + '">' + option['value'] + '</textarea></div>';
								html += '   <input type="hidden" name="product['+id1+'][option]['+k+'][product_option_id]" value="' + option['product_option_id'] + '" />';
							}

							if (option['type'] == 'file') {
								html += '    <button type="button" id="button-upload' + option['product_option_id'] + '" data-loading-text="" class="btn btn-default"><i class="fa fa-upload"></i> </button>';
								html += '    <input type="hidden" name="product['+id1+'][option]['+k+'][product_option_value_id]" value="' + option['value'] + '" id="input-option' + option['product_option_id'] + '" />';
							}

							if (option['type'] == 'date') {
								html += '  <div class="col-sm-12"><div class="input-group date"><input type="text" name="product['+id1+'][option]['+k+'][product_option_value_id]" value="' + option['value'] + '" placeholder="' + option['name'] + '" data-date-format="YYYY-MM-DD" id="input-option' + option['product_option_id'] + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
								html += '   <input type="hidden" name="product['+id1+'][option]['+k+'][product_option_id]" value="' + option['product_option_id'] + '" />';
							}

							if (option['type'] == 'datetime') {
								html += '  <div class="col-sm-12"><div class="input-group datetime"><input type="text" name="product['+id1+'][option]['+k+'][product_option_value_id]" value="' + option['value'] + '" placeholder="' + option['name'] + '" data-date-format="YYYY-MM-DD HH:mm" id="input-option' + option['product_option_id'] + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
								html += '   <input type="hidden" name="product['+id1+'][option]['+k+'][product_option_id]" value="' + option['product_option_id'] + '" />';
							}

							if (option['type'] == 'time') {
								html += '  <div class="col-sm-12"><div class="input-group time"><input type="text" name="product['+id1+'][option]['+k+'][product_option_value_id]" value="' + option['value'] + '" placeholder="' + option['name'] + '" data-date-format="HH:mm" id="input-option' + option['product_option_id'] + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
								html += '   <input type="hidden" name="product['+id1+'][option]['+k+'][product_option_id]" value="' + option['product_option_id'] + '" />';
							}
							k++;
						}//end for
					}
				
				
            }$('.option'+id).html(html);
        });
		 subtotal();

	});

	

  

  </script>

  <script type="text/javascript">

       	

		$(document).ready(function() {	
			$('#name').select2({
			});		
			$('#product1').select2({
			});	
			var countpro = {{$countpro}}

			for(i=1;i<countpro+1;i++){

				$('#product'+i).select2({
				});

			}
		

		});
		
    </script>

	<link rel="stylesheet" href="{{asset('admin_assets/plugins/select2/css/select2.min.css')}}">
	<script src="{{asset('admin_assets/plugins/select2/js/select2.full.js')}}"></script>

@endsection