@extends('admin.dashboard')
@section('title', 'Order')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        <!-- Breadcromb Row Start -->
        <div class="row">
            <div class="col-md-12">
                <div class="breadcromb-area">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="seipkon-breadcromb-left">
                                <h3>All Order</h3>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="seipkon-breadcromb-right">
                                <ul>
                                    <li><a href="{!!url('admin/dashboard')!!}">home</a></li>
                                    <li>Order</li>
                                    <li>All Order</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Breadcromb Row -->
		<section class="content-header">
			<div style="text-align: right">
				<a href="{!!url('admin/order/list')!!}" class="btn btn-default  btn-flat"><span class="glyphicon glyphicon-refresh"></span></a>
				<a href="{!!url('admin/order/add')!!}" class="btn btn-primary btn-flat"><span class="glyphicon glyphicon-plus-sign"></span> Add New</a>
				
			</div>
		</section>
		@if(Session::has('status'))
        <div class="alert alert-success fade in alert-dismissible" style="margin-top:18px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            {!!Session::get('status')!!}
        </div>
        @endif
        <!-- Pages Table Row Start -->
        <div class="row">
            <div class="col-md-12">
                <div class="page-box">
                    <div class="table-responsive">
                        <table id="page-list" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="25%">Name</th>
                                    <th width="15%">Tổng Cộng</th>
                                    <th width="15%">Phương Thức Thanh Toán</th>
                                    <th width="10%">Trạng Thái</th>
                                    <th width="10%">Publish Date</th>
                                    <th width="10%">Update Date</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($orders)
                                @foreach($orders as $count => $item)
                                <tr>
                                    <td>{!!$count + 1!!}</td>
                                    <td><a href="{!!url('admin/order/view/'.$item['id'].'?client_id='.$item['customer']['id'])!!}" class="page-table-success">{!!$item['customer']['name']!!}</a></td>
                                    <td>{!! number_format($item['total_price']) !!} <sup>đ</sup></td>
                                    <td>{!! $item['payment_method'] !!}</td>
                                    <td>{!! $item['status'] !!}</td>
                                    <td>{!! $item['created_at']!!}</td>
                                    <td>{!! $item['updated_at']!!}</td>
                                    <td>
                                        <a href="{!!url('admin/order/view/'.$item['id'].'?client_id='.$item['customer']['id'])!!}" class="page-table-info" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer Area Start -->
<footer class="seipkon-footer-area">
    @include('admin/layouts/footer')
</footer>
<!-- End Footer Area -->
@endsection
@section('assetjs')
<script>
    function myDelete(id) {
    var r = confirm("Are you delete!?");
    if (r == true) {
    window.location.assign("{!!url('admin/page/delete')!!}" + "/" + id);
    } else {
    txt = "You pressed Cancel!";
    }
    }
</script>
<script src="{!!url('admin_assets/plugins/sweet-alerts/js/sweetalert.min.js')!!}"></script>
@endsection