<?php
//echo "<pre>";
//print_r($detailmenu);
//echo "</pre>";
//exit;
?>
@extends('admin.dashboard')
@section('title', 'Page Title')
@section('content')
<div class="page-content">
    <div class="container-fluid">

        <!-- Breadcromb Row Start -->
        <div class="row">
            <div class="col-md-12">
                <div class="breadcromb-area">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="seipkon-breadcromb-left">
                                <h3>{!!$detailmenu->title_vn!!}</h3>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="seipkon-breadcromb-right">
                                <ul>
                                    <li><a href="{!!url('admin/dashboard')!!}">home</a></li>
                                    <li><a href="{!!url('admin/menu/catemenu/'.$cate->id)!!}">{!!$cate->title_vn!!}</a></li>
                                    <li>{!!$detailmenu->title_vn!!}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Breadcromb Row -->
        @if(Session::has('status'))
        <!--<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>-->
        <div class="alert alert-success fade in alert-dismissible" style="margin-top:18px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            {!!Session::get('status')!!}
        </div>
        @endif


        <!-- Pages Table Row Start -->
        <div class="row">
            <div class="col-md-12">
                <div class="page-box">
                    <form action="{!!action('Admin\MenuController@setMenu', $detailmenu->id)!!}" method="post">
                        <div class="row">
                            <input type="hidden" name="_token" value="{!!csrf_token()!!}">
                            <div class="col-md-9">
                                <div class="create-page-left">                                    
                                    <div class="form-group">
                                        <img class="marrig5" src="{!!url('admin_assets/img/'.session('locale').'.png')!!}"><label>Title</label>
                                        <input type="text" name="title_{!!session('locale')!!}" placeholder="Number" value="{!!$detailmenu->title_vn!!}" >
                                    </div>
                                    <div class="form-group">
                                        <img class="marrig5" src="{!!url('admin_assets/img/'.session('locale').'.png')!!}"><label>Link</label>
                                        <input type="text" name="link_{!!session('locale')!!}" placeholder="Number" value="{!!$detailmenu->link_vn!!}" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <img class="marrig5" src="{!!url('admin_assets/img/'.session('locale').'.png')!!}">
                                <label class="control-label" for="select">Category</label>
                                <select class="form-control" id="select" name="id_catemenu">
                                    @foreach($listid_catemenu as $id_catemenu)
                                    <option <?php echo ($detailmenu->id_catemenu==$id_catemenu->id)?"selected":""?> value="{!!$id_catemenu->id!!}">{!!$id_catemenu->title_vn!!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-layout-submit">
                            <p>
                                <button type="submit" class="btn btn-success" ><i class="fa fa-check"></i>Update</button>
                                <a class="btn btn-default" href="{!!url('admin/menu/catemenu/'.$cate->id)!!}" role="button"><i class="fa fa-chevron-left"></i> Back</a>
                            </p>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Footer Area Start -->
<footer class="seipkon-footer-area">
    @include('admin/layouts/footer')
</footer>
<!-- End Footer Area -->
@endsection