<?php
//$lang = (session('locale') == 'en')?'en':'vn';
if ($_SERVER['HTTP_HOST'] == "localhost")
    $pathtinymce = "/vam/public/admin_assets/filemanager/";
else {
    $pathtinymce = "http://vam.tuvanthietkeweb.net/public/admin_assets/filemanager/";
}
//echo "<pre>";
//print_r($listProductType);
//echo "</pre>";
//exit;
?>
@extends('admin.dashboard')
@section('title', 'Page Title')
@section('assets')
<script src="{!!url('admin_assets/tinymce/js/tinymce/tinymce.min.js')!!}"></script>
<script>
    tinymce.init({

        editor_selector: "mceEditor",
        selector: '#mytextarea_vn', height: 500,
        plugins: ["advlist autolink link image lists charmap preview hr anchor pagebreak", "code searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking", "table contextmenu directionality emoticons paste textcolor responsivefilemanager fullscreen"],
        toolbar1: "insertfile undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
        toolbar2: "fontsizeselect | forecolor backcolor | responsivefilemanager | link unlink anchor | image media | fullscreen preview code",
        fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
        image_advtab: true,
        relative_urls: false,
        nowrap: false,
        filemanager_sort_by: "date",
        filemanager_descending: 1,
        content_css: "{!!asset('admin_assets/css/editor-style.css')!!}",

        /*external_filemanager_path:"{!!url('admin_assets/filemanager/'.'/')!!}",*/
        external_filemanager_path: "<?php echo $pathtinymce ?>",
        filemanager_title: "Responsive Filemanager",
        external_plugins: {"filemanager": "{{asset('admin_assets/tinymce/js/tinymce/plugins/responsivefilemanager/filemanager/plugin.min.js')}}"}
    });

    tinymce.init({

        editor_selector: "mceEditor",
        selector: '#mytextarea_en', height: 500,
        plugins: ["advlist autolink link image lists charmap preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking", "table contextmenu directionality emoticons paste textcolor responsivefilemanager fullscreen"],
        toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
        toolbar2: "fontsizeselect | forecolor backcolor | responsivefilemanager | link unlink anchor | image media | fullscreen preview code",
        fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
        image_advtab: true,
        relative_urls: false,
        nowrap: false,
        content_css: "{!!asset('admin_assets/css/editor-style.css')!!}",

        /*external_filemanager_path:"{!!url('admin_assets/filemanager/'.'/')!!}",*/
        /*external_filemanager_path: "/~jpwebseo/projects/vtgroup/public/admin_assets/filemanager/",*/
        external_filemanager_path: "<?php echo $pathtinymce ?>",
        filemanager_title: "Responsive Filemanager",
        external_plugins: {"filemanager": "{{asset('admin_assets/tinymce/js/tinymce/plugins/responsivefilemanager/filemanager/plugin.min.js')}}"}
    });
</script>
@endsection
@section('content')
<div class="page-content">
    <div class="container-fluid">

        <!-- Breadcromb Row Start -->
        <div class="row">
            <div class="col-md-12">
                <div class="breadcromb-area">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="seipkon-breadcromb-left">
                                <h3>Edit Page</h3>
                                <a target="blank" href="#">Xem nhanh</a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="seipkon-breadcromb-right">
                                <ul>
                                    <li><a href="{!!url('admin/dashboard')!!}">home</a></li>
                                    <li>page</li>
                                    <li>Edit page</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Breadcromb Row -->

        @if(Session::has('status'))
                <!--<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>-->
        <div class="alert alert-success fade in alert-dismissible" style="margin-top:18px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            {!!Session::get('status')!!}
        </div>
        @endif

        <!-- Pages Table Row Start -->
        <div class="row">
            <div class="col-md-12">
                <div class="page-box">
                    <form action="{!!action('Admin\ProductController@edit', $viewItem->id)!!}" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <input type="hidden" name="old_image" value="{!!$viewItem->image!!}" >
                        <div class="row">
                            <div class="col-md-9">
                                <div class="create-page-left">
                                    <div class="form-group">
                                        <img class="marrig5" src="{!!url('admin_assets/img/'.session('locale').'.png')!!}"><label>Title</label>
                                        <input type="text" name="name_{!!session('locale')!!}" placeholder="Enter Page Title" value='{!!$viewItem->name!!}'>
                                    </div>

                                    <div class="form-group">
                                        <img class="marrig5" src="{!!url('admin_assets/img/'.session('locale').'.png')!!}"><label>Slug</label>
                                        <input type="text" name="slug_{!!session('locale')!!}" placeholder="Enter Product Slug" value="{!!$viewItem->slug!!}" >
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p>
                                                    <label>Price</label>
                                                    <input type="number" placeholder="Enter Price" name="unit_price" value="{!!$viewItem->unit_price!!}">
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <p>
                                                    <label>Discount</label>
                                                    <input type="number" placeholder="Enter 1-100 as Percentage, other number as price" name="promotion_price" value="{!!$viewItem->promotion_price!!}">
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p>
                                                    <label>SKU</label>
                                                    <input type="text" placeholder="Enter SKU" name="sku" value="{!!$viewItem->sku!!}">
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <p>
                                                    <label>Unit</label>
													<select class="form-control" id="select" name="unit">
														<option value="null">Chưa Phân Loại</option>
														<option value="hộp" {!! ($viewItem->unit == 'hộp')?'selected':'' !!}>Hộp</option>
														<option value="cái" {!! ($viewItem->unit == 'cái')?'selected':'' !!}>Cái</option>
													</select>
                                                    <!--input type="text" placeholder="Enter Unit" name="unit" value="{!!$viewItem->unit!!}" disabled=""-->
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <img class="marrig5" src="{!!url('admin_assets/img/'.session('locale').'.png')!!}"><label class="control-label" for="message">Description</label>
                                        <textarea name="description_{!!session('locale')!!}" class="form-control" id="message" placeholder="Description">{!!$viewItem->description!!}</textarea>
                                    </div>

                                    <div class="page-editor-box form-group">
                                        <img class="marrig5" src="{!!url('admin_assets/img/'.session('locale').'.png')!!}"><label>Content</label>
                                        <textarea name="content_{!!session('locale')!!}" id="mytextarea_{!!session('locale')!!}">{!!$viewItem->content!!}</textarea>
                                    </div>

                                </div>

                                <div class="page-box">
                                    <div class="button-page-box">
                                        <div class="button-page-box-heading">
                                            <h4>SEO Supporting</h4>
           <!--                                 <p>
                                               Turn a button into a dropdown toggle with some basic markup changes.
                                            </p>-->
                                        </div>
                                        <div class="button-page-box-content">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="create-page-left">
                                                        <div class="form-group">
                                                            <label>Title</label>
                                                            <input id="seotitle" type="text" name="seotitle" placeholder="SEO Title" value='' ><br/>
                                                            <p id="input_feedback1"></p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Keyword</label>
                                                            <input id="seokeyword" type="text" name="seokeyword" placeholder="SEO Keyword" value='' >
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label" for="seodescription">Description</label>
                                                            <textarea name="seodescription" class="form-control" id="seodescription" placeholder="SEO Description"></textarea>
                                                            <p id="textare_feedback"></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-3">
                                <div class="create-page-right">
                                    <div class="form-group">
                                        <label class="control-label" for="select">Category</label>
                                        <select class="form-control" id="select" name="id_type">
                                            <option value="0">Chưa Phân Loại</option>
                                            @foreach($listProductType as $count => $producttype)
                                            <option value="{!!$producttype['id']!!}" <?php echo ($producttype['id'] == $viewItem->id_type) ? "selected" : "" ?>>{!!$producttype['name']!!}</option>
                                            @endforeach
                                        </select>
                                    </div>
									<div class="form-group">
                                        <label class="control-label" for="select">Trademark</label>
                                        <select class="form-control" id="select" name="id_trademark">
                                            <option value="0">Không thương hiệu</option>
                                            @foreach($listTrademark as $count => $producttype)
                                            <option value="{!!$producttype['id']!!}" <?php echo ($producttype['id'] == $viewItem->id_trademark) ? "selected" : "" ?>>{!!$producttype['name']!!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group form-checkbox">
                                        <input name="neew" type="checkbox" id="chk_1" <?php echo ($viewItem->neew == 1) ? "checked" : "" ?>/>
                                        <label class="inline control-label" for="chk_1">New</label>
                                    </div>
                                    <div class="form-group form-checkbox">
                                        <input name="feature" type="checkbox" id="chk_2" <?php echo ($viewItem->feature == 1) ? "checked" : "" ?>/>
                                        <label class="inline control-label" for="chk_2">Feature</label>
                                    </div>
                                    <div class="page-img-upload">
                                        <img class="marrig5" src="{!!url('admin_assets/img/'.session('locale').'.png')!!}"><label>Feature image</label><br>
                                        <img src="{!!url('images/upload/product/'.$viewItem->image)!!}" alt="{!!$viewItem->name!!}">
                                        <div class="product-upload btn btn-info">
                                            <i class="fa fa-upload"></i>
                                            Upload Image
                                            <input type="file" class="custom-file-input form-control" id="customFile" name="image">
                                            <input type="hidden" name="path" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="form-layout-submit">
                            <p>
                                <button type="submit" class="btn btn-success" ><i class="fa fa-check"></i>Update</button>
                                <a class="btn btn-default" href="{!!url('admin/product/list')!!}" role="button"><i class="fa fa-chevron-left"></i> Back</a>
                            </p>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <!-- End Pages Table Row -->

    </div>
</div>

<!-- Footer Area Start -->
<footer class="seipkon-footer-area">
    @include('admin/layouts/footer')
</footer>
<!-- End Footer Area -->
@endsection
@section('assetjs')
<script>
    $.noConflict();
    jQuery(document).ready(function () {
//    var text_max = 99;
//    $('#textarea_feedback').html(text_max + ' characters remaining');
        var text_length1 = jQuery('#seotitle').val().length;
        jQuery('#input_feedback1').html(text_length1 + " / 60 ký tự");
        jQuery('#seotitle').keyup(function () {
            var text_length1 = jQuery('#seotitle').val().length;
//        var text_remaining = text_max - text_length;
//    console.log(text_length);
            jQuery('#input_feedback1').html(text_length1 + " / 60 ký tự");
            if (text_length1 > 60) {
                jQuery('#input_feedback1').css('color', 'red')
            } else {
                jQuery('#input_feedback1').css('color', '#242a33')
            }
        });

        var text_length2 = jQuery('#seodescription').val().length;
        jQuery('#textare_feedback').html(text_length2 + " / 180 ký tự");
        jQuery('#seodescription').keyup(function () {
            var text_length2 = jQuery('#seodescription').val().length;
//        var text_remaining = text_max - text_length2;
//    console.log(text_length2);
            jQuery('#textare_feedback').html(text_length2 + " / 180 ký tự");
            if (text_length2 > 180) {
                jQuery('#textare_feedback').css('color', 'red')
            } else {
                jQuery('#textare_feedback').css('color', '#242a33')
            }
        });

    });
</script>
@endsection