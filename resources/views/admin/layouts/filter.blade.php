<?php
//echo Request::get('id_cate');
?>
<div class="row">
    <form action="{!!action('Admin\PostController@searchPostCate')!!}" method="POST">
        <input type="hidden" name="_token" value="{!!csrf_token()!!}">
        <div class="col-md-12">
            <table class="table table-condensed margin0">
                <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td width="20%"><div class="single-button-item">
                                <select class="form-control" id="category" name="id_cate" onchange="return window.location.href = 'list?id_cate='+ $(this).val(); ">
                                    <option value="0">Category</option>
                                    <?php
                                    
                                    function categoryTree($parent_id = 0, $submark = '') {
                                        $query = App\Categorypost::getListCategoryParent($parent_id);
                                        if ($query > 0) {
                                            foreach ($query as $catepost) {
                                                ?>
                                                <option <?php if ($catepost->id == Request::input('id_cate')) echo "selected" ?> value="{!!$catepost->id!!}"><?php echo (session('locale') == 'vn') ? $submark . $catepost->title_vn : $submark . $catepost->title_en ?></option>
                                                <?php
                                                categoryTree($catepost->id, $submark . '---- ');
                                            }
                                        }
                                    }

                                    echo categoryTree();
                                    ?>
                                </select>
                            </div>
                        </td>
                        <td width="20%">
                            <div class="single-button-item">
                                <input class="form-control" placeholder="Keyword" id="text-field" type="text" name="keyword" value="<?php echo (Request::isMethod('POST') ? Request::input('keyword') : ""); ?>">
                            </div>
                        </td>
                        <td width="9%">
                            <div class="single-button-item">
                                <button type="submit" class="btn btn-default">Search</button>
                                <a class="btn btn-default" href="{!!url('admin/post/list')!!}">Reset</a>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>
</div>

