@extends('admin.dashboard')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        
        <!-- Breadcromb Row Start -->
        <div class="row">
            <div class="col-md-12">
                <div class="breadcromb-area">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="seipkon-breadcromb-left">
                                <h3>Edit Section</h3>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="seipkon-breadcromb-right">
                                <ul>
                                    <li><a href="{!!url('admin/dashboard')!!}">home</a></li>
                                    <li>section</li>
                                    <li>Edit section</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Breadcromb Row -->
        
        @if(Session::has('status'))
                <!--<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>-->
        <div class="alert alert-success fade in alert-dismissible" style="margin-top:18px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            {!!Session::get('status')!!}
        </div>
        @endif

        <!-- Pages Table Row Start -->
        <div class="row">
            <div class="col-md-12">
                <div class="page-box">
                    <form action="{!!action('Admin\SectionController@edit',$viewsection->id)!!}" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <input type="hidden" name="_token" value="{!!csrf_token()!!}">
                            <div class="col-md-9">
                                <div class="create-page-left">
                                    <div class="form-group">
                                        <label>Position Page</label>
                                        <input type="text" name="name" placeholder="Enter name" value="{!!$viewsection->name!!}" readonly="readonly">
                                    </div>
                                    <div class="form-group">
                                        <img class="marrig5" src="{!!url('admin_assets/img/'.session('locale').'.png')!!}"><label>Title</label>
                                        <input type="text" name="title_{!!Session('locale')!!}" placeholder="Enter Page Title" value="{!!(Session('locale')=='en')?$viewsection->title_en:$viewsection->title_vn!!}" >
                                    </div>
                                    <div class="form-group">
                                        <img class="marrig5" src="{!!url('admin_assets/img/'.session('locale').'.png')!!}"><label class="control-label" for="message">Textarea</label>
                                        <textarea name="description_{!!Session('locale')!!}" class="form-control" id="message" placeholder="Description">{!!(Session('locale')=='en')?$viewsection->description_en:$viewsection->description_vn!!}</textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Link</label>
                                        <input type="text" name="link" placeholder="Enter link" value="{!!$viewsection->link!!}" >
                                    </div>
                                    <div class="form-group">
                                        <label>Video</label>
                                        <input type="text" name="video" placeholder="Enter video" value="{!!$viewsection->video!!}" >
                                    </div>
                                    
                                    
                                </div>
                            </div>

                            <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="add-product-image-upload">
                                            <label>Image</label>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="product-upload-image">
                                                        <img src="{!!url('images/upload/section/'.$viewsection->image)!!}" alt="" width="300px" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="product-upload-action">
                                                        <div class="product-upload btn btn-info">
                                                            <p>
                                                                <i class="fa fa-upload"></i>
                                                                Upload Another Image
                                                            </p>
                                                            <input type="file" name="image" value="">
                                                            <input type="hidden" name="url" value="{!!$viewsection->image!!}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>


                        </div>
                        <div class="form-layout-submit">
                            <p>
                                <button type="submit" class="btn btn-success" ><i class="fa fa-check"></i>Update</button>
                                <a class="btn btn-default" href="{!!url('admin/section/list')!!}" role="button"><i class="fa fa-chevron-left"></i> Back</a>
                            </p>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <!-- End Pages Table Row -->

    </div>
</div>

<!-- Footer Area Start -->
<footer class="seipkon-footer-area">
    @include('admin/layouts/footer')
</footer>
<!-- End Footer Area -->
@endsection