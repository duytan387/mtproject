@extends('admin.dashboard')
@section('title', 'Page Title')
@section('content')
<div class="page-content">
    <div class="container-fluid">
        @if($errors->has())
        @foreach ($errors->all() as $error)
        <div style="color: red">{{ $error }}</div>
        @endforeach
        @endif
        <!-- Breadcromb Row Start -->
        <div class="row">
            <div class="col-md-12">
                <div class="breadcromb-area">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="seipkon-breadcromb-left">
                                <h3>Edit Banner</h3>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="seipkon-breadcromb-right">
                                <ul>
                                    <li><a href="{!!url('admin/dashboard')!!}">home</a></li>
                                    <li>banner</li>
                                    <li>Edit banner</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Breadcromb Row -->
		
		@if(Session::has('status'))
                <!--<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>-->
        <div class="alert alert-success fade in alert-dismissible" style="margin-top:18px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            {!!Session::get('status')!!}
        </div>
        @endif

        <!-- Pages Table Row Start -->
        <div class="row">
            <div class="col-md-12">
                <div class="page-box">
                    <form action="{!!action('Admin\BannerController@update',$viewbanner->id)!!}" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <input type="hidden" name="_token" value="{!!csrf_token()!!}">
                            <div class="col-md-9">
                                <div class="create-page-left">
                                    <div class="form-group">
                                        <img class="marrig5" src="{!!url('admin_assets/img/'.session('locale').'.png')!!}"><label>Title</label>
                                        <input type="text" name="title_{!!session('locale')!!}" placeholder="Title" value="{!!(session('locale')=='en')?$viewbanner->title_en:$viewbanner->title_vn!!}" >
                                    </div>
                                    
                                    <div class="form-group">
                                        <img class="marrig5" src="{!!url('admin_assets/img/'.session('locale').'.png')!!}"><label>Label</label>
                                        <input type="text" name="label_{!!session('locale')!!}" placeholder="Label" value="{!!(session('locale')=='en')?$viewbanner->label_en:$viewbanner->label_vn!!}" >
                                    </div>
                                    
                                    <div class="form-group">
                                        <img class="marrig5" src="{!!url('admin_assets/img/'.session('locale').'.png')!!}"><label>Link</label>
                                        <input type="text" name="link_{!!session('locale')!!}" placeholder="Link" value="{!!(session('locale') == 'en')?$viewbanner->link_en:$viewbanner->link_vn!!}" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label" for="select">Category</label>
                                    <select class="form-control" id="select" name="id_cate">
                                        <option value="0">Chưa Phân Loại</option>
                                        <option <?php echo ($viewbanner->id_cate == 1)?"selected":""?> value="1">Banner Top</option>
                                        <option <?php echo ($viewbanner->id_cate == 2)?"selected":""?> value="2">Banner Sidebar</option>
                                        <option <?php echo ($viewbanner->id_cate == 3)?"selected":""?> value="3">Banner Popup</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="add-product-image-upload">
                                        <label>Image slide</label>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="product-upload-image">
                                                    <img src="{!!url('images/upload/bannerslides/'.$viewbanner->image)!!}" alt="image" width="300px" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="product-upload-action">
                                                    <div class="product-upload btn btn-info">
                                                        <p>
                                                            <i class="fa fa-upload"></i>
                                                            Upload Another Image
                                                        </p>
                                                        <input type="file" name="image" value="">
                                                        <input type="hidden" name="url" value="{!!$viewbanner->image!!}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-layout-submit">
                            <p>
                                <button type="submit" class="btn btn-success" ><i class="fa fa-check"></i>Update</button>
                                <a class="btn btn-default" href="{!!url('admin/banner/list')!!}" role="button"><i class="fa fa-chevron-left"></i> Back</a>
                            </p>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <!-- End Pages Table Row -->

    </div>
</div>

<!-- Footer Area Start -->
<footer class="seipkon-footer-area">
    @include('admin/layouts/footer')
    <?php
//    echo session('locale');
    ?>
</footer>
<!-- End Footer Area -->
@endsection