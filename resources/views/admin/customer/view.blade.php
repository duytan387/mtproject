@extends('admin.dashboard')
@section('title', 'Page Title')
@section('assets')
@endsection
@section('content')
<div class="page-content">
    <div class="container-fluid">

        <!-- Breadcromb Row Start -->
        <div class="row">
            <div class="col-md-12">
                <div class="breadcromb-area">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="seipkon-breadcromb-left">
                                <h3>Edit Customer</h3>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="seipkon-breadcromb-right">
                                <ul>
                                    <li><a href="index-2.html">home</a></li>
                                    <li>customer</li>
                                    <li>Edit Customer</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Breadcromb Row -->

        @if($errors->has())
        @foreach ($errors->all() as $error)
        <div style="color: red">{{ $error }}</div>
        @endforeach
        @endif

        <!-- Pages Table Row Start -->
        <div class="row">
            <div class="col-md-12">
                <div class="page-box">
                    <form action="{!!action('Admin\CustomerController@edit', $viewcustomer->id)!!}" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <input type="hidden" name="_token" value="{!!csrf_token()!!}">
                            <div class="col-md-12">
                                <div class="create-page-left">                                    
                                    <div class="form-group">
                                        <div class="add-product-image-upload">

                                            <div class="row">
                                                <div class="col-md-9">
                                                    <div class="create-page-left">
                                                        <div class="form-group">
                                                            <img class="marrig5" src="{!!url('admin_assets/img/'.session('locale').'.png')!!}"><label>Name</label>
                                                            <input type="text" name="name" placeholder="Name" value="{!!$viewcustomer->name!!}" >
                                                        </div>

                                                        <div class="form-group">
                                                            <img class="marrig5" src="{!!url('admin_assets/img/'.session('locale').'.png')!!}"><label>Position</label>
                                                            <input type="text" name="position" placeholder="Position" value="{!!$viewcustomer->position!!}" >
                                                        </div>

                                                        <div class="form-group">
                                                            <img class="marrig5" src="{!!url('admin_assets/img/'.session('locale').'.png')!!}"><label class="control-label" for="message">Comment</label>
                                                            <textarea name="comment" class="form-control" id="comment" placeholder="Description">{!!$viewcustomer->comment!!}</textarea>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="create-page-right">
                                                        <div class="page-img-upload">
                                                            <img class="marrig5" src="{!!url('admin_assets/img/'.session('locale').'.png')!!}"><label>Feature image</label><br>
                                                            <img src="{!!url('images/upload/customer/'.$viewcustomer->image)!!}" alt="Image">
                                                            <div class="product-upload btn btn-info">
                                                                <i class="fa fa-upload"></i>
                                                                Upload Image
                                                                <input type="file" class="custom-file-input form-control" id="customFile" name="image">
                                                                <input type="hidden" class="custom-file-input form-control" id="currentfile" name="currentfile" value="{!!$viewcustomer->image!!}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                            </div>
                        </div>
                        <div class="form-layout-submit">
                            <p>
                                <button type="submit" class="btn btn-success" ><i class="fa fa-check"></i>Update</button>
                                <a class="btn btn-default" href="{!!url('admin/customer/list')!!}" role="button"><i class="fa fa-chevron-left"></i> Back</a>
                            </p>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <!-- End Pages Table Row -->

    </div>
</div>

<!-- Footer Area Start -->
<footer class="seipkon-footer-area">
    @include('admin/layouts/footer')
</footer>
<!-- End Footer Area -->
@endsection