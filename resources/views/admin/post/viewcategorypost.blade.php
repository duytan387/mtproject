<?php
//echo "<pre>";
//print_r($viewcategorypost);
//echo "</pre>";
//exit;
?>
@extends('admin.dashboard')
@section('content')
<div class="page-content">
    <div class="container-fluid">

        <!-- Breadcromb Row Start -->
        <div class="row">
            <div class="col-md-12">
                <div class="breadcromb-area">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="seipkon-breadcromb-left">
                                <h3>Edit Category</h3>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="seipkon-breadcromb-right">
                                <ul>
                                    <li><a href="{!!url('admin/dashboard')!!}">home</a></li>
                                    <li>Post</li>
                                    <li>Edit Category</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Breadcromb Row -->
        @if(Session::has('status'))
                <!--<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>-->
        <div class="alert alert-success fade in alert-dismissible" style="margin-top:18px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
            {!!Session::get('status')!!}
        </div>
        @endif
        <!-- Pages Table Row Start -->
        <div class="row">
            <div class="col-md-12">
                <div class="page-box">
                    <form action="{!!action('Admin\PostController@editCategoryPost',$viewcategorypost->id)!!}" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <input type="hidden" name="_token" value="{!!csrf_token()!!}">
                            <div class="col-md-9">
                                <div class="create-page-left">
                                    <div class="form-group">
                                        <img class="marrig5" src="{!!url('admin_assets/img/'.session('locale').'.png')!!}"><label>Title</label>
                                        <input type="text" name="title_{!!session('locale')!!}" placeholder="Enter Page Title" value="{!!(session('locale') == 'en')?$viewcategorypost->title_en:$viewcategorypost->title_vn!!}" >
                                    </div>
                                    <div class="form-group">
                                        <img class="marrig5" src="{!!url('admin_assets/img/'.session('locale').'.png')!!}"><label>Slug</label>
                                        <input type="text" name="slug_{!!session('locale')!!}" placeholder="Enter Page Title" value="{!!(session('locale') == 'en')?$viewcategorypost->slug_en:$viewcategorypost->slug_vn!!}" >
                                    </div>
                                    <div class="form-group">
                                        <img class="marrig5" src="{!!url('admin_assets/img/'.session('locale').'.png')!!}"><label class="control-label" for="message">Description</label>
                                        <textarea name="description_{!!session('locale')!!}" class="form-control" id="message" placeholder="Description">{!!(session('locale') == "en")?$viewcategorypost->description_en:$viewcategorypost->description_vn!!}</textarea>
                                    </div>
                                    


                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <img class="marrig5" src="{!!url('admin_assets/img/'.session('locale').'.png')!!}"><label class="control-label" for="select">Category</label>
                                    <select class="form-control" id="select" name="parent_id">
                                        <?php
                                            function categoryTree($parent_id = 0, $submark = '') {
                                                $query = App\Categorypost::getListCategoryParent($parent_id);
                                                if ($query > 0) {
                                                    foreach ($query as $catepost) {
                                                        ?>
                                                        <option value="{!!$catepost->id!!}"><?php echo (session('locale') == 'vn')?$submark.$catepost->title_vn:$submark.$catepost->title_en?></option>
                                                        <?php
                                                        categoryTree($catepost->id, $submark . '---- ');
                                                    }
                                                }
                                            }
                                            echo categoryTree();
                                            ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                        <div class="add-product-image-upload">
                                            <label>Image</label>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="product-upload-image">
                                                        <img src="{!!url('images/upload/post/'.$viewcategorypost->image)!!}" alt="" width="300px" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="product-upload-action">
                                                        <div class="product-upload btn btn-info">
                                                            <p>
                                                                <i class="fa fa-upload"></i>
                                                                Upload Another Image
                                                            </p>
                                                            <input type="file" name="image" value="">
                                                            <input type="hidden" name="url" value="{!!$viewcategorypost->image!!}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>

                            
                        </div>
                        <div class="form-layout-submit">
                            <p>
                                <button type="submit" class="btn btn-success" ><i class="fa fa-check"></i>Update</button>
                                <a class="btn btn-default" href="{!!url('admin/post/listcategorypost')!!}" role="button"><i class="fa fa-chevron-left"></i> Back</a>
                            </p>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <!-- End Pages Table Row -->

    </div>
</div>

<!-- Footer Area Start -->
<footer class="seipkon-footer-area">
    @include('admin/layouts/footer')
</footer>
<!-- End Footer Area -->
@endsection