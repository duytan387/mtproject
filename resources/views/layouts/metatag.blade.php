<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="alternate" hreflang="vi" href="{!!url('/'.Request::segment(1))!!}" />
<link rel="canonical" href="{!!url('/'.Request::segment(1))!!}" />
<meta property="og:locale" content="vi_VN" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{!!strip_tags($getmeta->title_vn)!!}" />
<meta property="og:description" content="{!!$getmeta->description_vn!!}" />
<meta property="og:url" content="{!!url('/'.$getmeta->slug_vn)!!}" />
<meta property="og:site_name" content="{!!strip_tags($getmeta->title_vn)!!}" />
<meta property="og:image" content="{!!url('images/upload/page/'.$getmeta->image)!!}" />
<meta property="og:image:width" content="600" />
<meta property="og:image:height" content="450" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="{!!strip_tags($getmeta->title_vn)!!}" />
<meta name="twitter:image" content="{!!url('images/upload/page/'.$getmeta->image)!!}" />