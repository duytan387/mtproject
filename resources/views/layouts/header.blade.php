<?php
$header = json_decode($header['config']);
$social = json_decode($social['config']);
//echo "<pre>";
//print_r($header);
//echo "</pre>";
//exit;
?>
<header class="main_header_area">
    <div class="top_header_area row m0">
        <div class="container">
            <div class="float-left">
                <a href="tel:0917966067"><i class="fa fa-phone" aria-hidden="true"></i> 091 796 6067 (Ms. Phụng)</a>
				<a href="tel:0967931680"><i class="fa fa-phone" aria-hidden="true"></i> 0967 931 680 (Ms. Nga)</a>
                <a href="mailto:cty.longquockim@gmail.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> cty.longquockim@gmail.com</a>
            </div>
            <div class="float-right">
                <ul class="h_social list_style">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
<!--                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>-->
                </ul>
                <ul class="h_search list_style">
                    <li class="shop_cart">
                        <a href="{!!route('giohang')!!}">
                            <span>
                                @if(Session::has('cart'))
                                    {!!Session('cart')->totalQty!!}
                                @else 
                                    0 
                                @endif 
                            </span>
                            <i class="lnr lnr-cart"></i>
                        </a>
                    </li>
                    <!--<li><a class="popup-with-zoom-anim" href="#test-search"><i class="fa fa-search"></i></a></li>-->
                </ul>
            </div>
        </div>
    </div>
    <div class="main_menu_two">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="{!!url('/')!!}"><img src="{!!url('images/upload/'.$header->logo)!!}" alt="Logo Long Quốc Kim" width="150px"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="my_toggle_menu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav justify-content-end">
<!--                        <li class="dropdown submenu active">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Trang Chủ</a>
                        </li>-->
                        <li><a href="{!!url('banh-trung-thu-kinh-do')!!}">Bánh Trung Thu Kinh Đô</a></li>
                        <li><a href="{!!url('bang-gia-banh-trung-thu-kinh-do')!!}">Bảng Giá</a></li>
						<li><a href="{!!url('chiet-khau-banh-trung-thu-kinh-do-2019')!!}">Chiết Khấu</a></li>
                        <li class="dropdown submenu">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Giới Thiệu</a>
                            <ul class="dropdown-menu">
                                <li><a href="{!!url('ve-chung-toi')!!}">Về Chúng Tôi</a></li>
                                <li><a href="{!!url('thu-ngo')!!}">Thư Ngỏ</a></li>
                            </ul>
                        </li>
                        <li class="dropdown submenu">
                            <a href="{!!url('tin-tuc')!!}">Tin Tức</a>
                        </li>
                        <li><a href="{!!url('lien-he')!!}">Liên Hệ</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>