<header class="header stricky"> 
    <div class="service_page">
        <div class="container">
            <section id="manu_area" class="manu_service">
                <h2 class="hide">menu</h2>
                <div class="navbar" role="navigation"> 
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- logo here -->
                        <a class="navbar-brand" href="./"><img width="69px" src="http://vuongtrongroup.com/images/logo.png" /></a>
                    </div>

                    <!-- Main menu aea -->
                    <div class="navbar-collapse collapse">  
                        <!-- Right nav -->
                        <ul class="nav navbar-nav main-menu scroll-menu">	 
                            <li><a href="{!!url('/')!!}">Trang chủ<span class="border"></span></a> </li>
                            <li><a href="{!!url('/ve-chung-toi')!!}">Về Chúng Tôi<span class="border"></span></a></li>
                            <li class=""><a href="{!!url('/du-an')!!}">Dự án<span class="border"></span></a>
                                <ul class="dropdown-menu submenu">
                                    <li><a href="{!!url('/du-an/can-ho')!!}">Căn Hộ</a></li>
                                    <li><a href="{!!url('/du-an/nha-pho-biet-thu')!!}">Nhà Phố, Biệt Thự</a></li>
                                    <li><a href="{!!url('/du-an/van-phong-thuong-mai')!!}">Văn Phòng, Thương Mại</a></li>
                                    <li><a href="{!!url('/du-an/khach-san-resort')!!}">Khách Sạn, Resort</a></li>
                                </ul>
                            </li>
                            <li><a href="{!!url('/bang-gia-thiet-ke')!!}">Bảng giá<span class="border"></span></a></li>
                            <li><a href="{!!url('/tin-tuc')!!}">Tin Tức <span class="border"></span></a>
                                <ul class="dropdown-menu submenu">
                                    <li><a href="{!!url('/tu-van-thiet-ke-noi-that-mien-phi-dt38724')!!}">Tư Vấn Thiết Kế</a></li>
                                    <li><a href="{!!url('/tin-tuc/tuyen-dung')!!}">Tuyển Dụng</a></li>
                                </ul>
                            </li>
                            <li><a href="{!!url('/lien-he')!!}">Liên Hệ</a></li>
                        </ul> 
                    </div> 
                </div>  
            </section>  
        </div>
    </div>  
</header> 