<div class="main-slider style-two default-banner"><div class="tp-banner-container"><div class="tp-banner"><div id="rev_slider_1014_1_wrapper"class="rev_slider_wrapper fullscreen-container" data-alias="typewriter-effect" data-source="gallery"><div id="rev_slider_1014_1" class="rev_slider fullscreenbanner" style="display:none" data-version="5.4.1"><ul>@foreach($bannerslide as $count => $banner) @if($count == 1)<li data-index="rs-1000" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="{!!url('images/upload/bannerslides/'.$banner->image)!!}" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description=""><img data-lazyload="{!!url('images/upload/bannerslides/'.$banner->image)!!}" src="{!!url('images/upload/bannerslides/'.$banner->image)!!}" alt="" data-bgposition="center bottom" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina><div class="tp-caption tp-shape tp-shapewrapper"id="slide-<?php echo $count + 1 ?>00-layer-1" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="full" data-height="full" data-whitespace="nowrap" data-type="shape" data-basealign="slide" data-responsive_offset="off" data-responsive="off" data-frames='[
                                                                                                                                                                                                        {"from":"opacity:0;","speed":1000,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},
                                                                                                                                                                                                        {"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power4.easeOut"}
                                                                                                                                                                                                        ]'data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"style="z-index:12;background-color:rgba(0,0,0,.1);border-color:transparent;border-width:0"></div><div class="tp-caption tp-resizeme"id="slide-<?php echo $count + 1 ?>00-layer-2" data-x="['left','left','left','left']" data-hoffset="['30','30','30','100']" data-y="['top','top','top','top']" data-voffset="['200','200','200','200']" data-fontsize="['40','40','40','30']" data-lineheight="['70','70','70','60']" data-width="['700','700','700','700']" data-height="['none','none','none','none']" data-whitespace="['normal','normal','normal','normal']" data-type="text" data-responsive_offset="on" data-frames='[
                                                                                                                                                                                                        {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},
                                                                                                                                                                                                        {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                                                                                                                                                                                        ]'data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"style="z-index:13;white-space:normal;font-weight:700;color:rgba(255,255,255,1);border-width:0"><span class="text-uppercase"style="font-family:Roboto"><span class="text-primary">{!!$banner->title_vn!!}</span></span></div><div class="tp-caption tp-resizeme"id="slide-<?php echo $count + 1 ?>00-layer-3" data-x="['left','left','left','left']" data-hoffset="['30','30','30','100']" data-y="['top','top','top','top']" data-voffset="['300','300','300','300']" data-fontsize="['18','18','18','30']" data-lineheight="['30','30','30','40']" data-width="['650','650','650','650']" data-height="['none','none','none','none']" data-whitespace="['normal','normal','normal','normal']" data-type="text" data-responsive_offset="on" data-frames='[
                                                                                                                                                                                                        {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":1000,"ease":"Power4.easeOut"},
                                                                                                                                                                                                        {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                                                                                                                                                                                        ]'data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"style="z-index:13;font-weight:500;color:rgba(255,255,255,.85);border-width:0"><span style="font-family:Roboto">{!!$banner->label_vn!!}</span></div><div class="tp-caption tp-resizeme"id="slide-<?php echo $count + 1 ?>00-layer-4" data-x="['left','left','left','left']" data-hoffset="['30','30','30','100']" data-y="['top','top','top','top']" data-voffset="['430','430','450','500']" data-lineheight="['none','none','none','none']" data-width="['300','300','300','300']" data-height="['none','none','none','none']" data-whitespace="['normal','normal','normal','normal']" data-type="text" data-responsive_offset="on" data-frames='[ 
                                                                                                                                                                                                        {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":1500,"ease":"Power4.easeOut"},
                                                                                                                                                                                                        {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                                                                                                                                                                                        ]'data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"style="z-index:13;text-transform:uppercase;font-weight:700"><a href="{!!$banner->link_vn!!}"class="site-button button-lg skew-icon-btn m-r15">Xem thêm <i class="fa fa-angle-double-right"></i></a></div></li>@else<li data-index="rs-2000" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="{!!url('images/upload/bannerslides/'.$banner->image)!!}" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description=""><img data-lazyload="{!!url('images/upload/bannerslides/'.$banner->image)!!}" src="{!!url('images/upload/bannerslides/'.$banner->image)!!}" alt="" data-bgposition="center bottom" data-bgfit="cover" data-bgrepeat="no-repeat"class="rev-slidebg" data-no-retina><div class="tp-caption tp-shape tp-shapewrapper"id="slide-<?php echo $count + 1 ?>00-layer-1" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="full" data-height="full" data-whitespace="nowrap" data-type="shape" data-basealign="slide" data-responsive_offset="off" data-responsive="off" data-frames='[
                                                                                                                                                                                                        {"from":"opacity:0;","speed":1000,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},
                                                                                                                                                                                                        {"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power4.easeOut"}
                                                                                                                                                                                                        ]'data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"style="z-index:12;background-color:rgba(0,0,0,.5);border-color:transparent;border-width:0"></div><div class="tp-caption tp-resizeme"id="slide-<?php echo $count + 1 ?>00-layer-2" data-x="['left','left','left','left']" data-hoffset="['30','30','30','100']" data-y="['top','top','top','top']" data-voffset="['200','200','200','200']" data-fontsize="['40','40','40','30']" data-lineheight="['70','70','70','60']" data-width="['700','700','700','700']" data-height="['none','none','none','none']" data-whitespace="['normal','normal','normal','normal']" data-type="text" data-responsive_offset="on" data-frames='[
                                                                                                                                                                                                        {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},
                                                                                                                                                                                                        {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                                                                                                                                                                                        ]'data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"style="z-index:13;white-space:normal;font-weight:700;color:rgba(255,255,255,1);border-width:0"><span class="text-uppercase"style="font-family:Roboto"><span class="text-primary">{!!$banner->title_vn!!}</span></span></div><div class="tp-caption tp-resizeme"id="slide-<?php echo $count + 1 ?>00-layer-3" data-x="['left','left','left','left']" data-hoffset="['30','30','30','100']" data-y="['top','top','top','top']" data-voffset="['300','300','300','300']" data-fontsize="['18','18','18','30']" data-lineheight="['30','30','30','40']" data-width="['650','650','650','650']" data-height="['none','none','none','none']" data-whitespace="['normal','normal','normal','normal']" data-type="text" data-responsive_offset="on" data-frames='[
                                                                                                                                                                                                        {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":1000,"ease":"Power4.easeOut"},
                                                                                                                                                                                                        {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                                                                                                                                                                                        ]'data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"style="z-index:13;font-weight:500;color:rgba(255,255,255,.85);border-width:0"><span style="font-family:Roboto">{!!$banner->label_vn!!}</span></div><div class="tp-caption tp-resizeme"id="slide-<?php echo $count + 1 ?>00-layer-4" data-x="['left','left','left','left']" data-hoffset="['30','30','30','100']" data-y="['top','top','top','top']" data-voffset="['430','430','450','500']" data-lineheight="['none','none','none','none']" data-width="['300','300','300','300']" data-height="['none','none','none','none']" data-whitespace="['normal','normal','normal','normal']" data-type="text" data-responsive_offset="on" data-frames='[ 
                                                                                                                                                                                                        {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":1500,"ease":"Power4.easeOut"},
                                                                                                                                                                                                        {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                                                                                                                                                                                        ]'data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"style="z-index:13;text-transform:uppercase;font-weight:700"><a href="{!!$banner->link_vn!!}"class="site-button button-lg skew-icon-btn m-r15">Xem thêm <i class="fa fa-angle-double-right"></i></a></div><div class="tp-caption tp-resizeme"id="slide-<?php echo $count + 1 ?>00-layer-5" data-x="['left','left','left','left']" data-hoffset="['230','230','230','230']" data-y="['top','top','top','top']" data-voffset="['490','490','490','600']" data-fontsize="['none','none','none','none']" data-lineheight="['none','none','none','none']" data-width="['300','300','300','300']" data-height="['none','none','none','none']" data-whitespace="['normal','normal','normal','normal']" data-type="text" data-responsive_offset="on" data-frames='[ 
                                                                                                                                                                                                        {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeOut"},
                                                                                                                                                                                                        {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                                                                                                                                                                                        ]'data-textalign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]"style="z-index:13;text-transform:uppercase;font-weight:700"></div></li>@endif @endforeach()</ul><div class="tp-bannertimer tp-bottom"style="visibility:hidden!important"></div></div></div></div></div></div>
@section('scripts')
@parent
<script>
    var tpj = jQuery;
    var revapi1014;

    tpj(document).ready(function () {
        if (tpj("#rev_slider_1014_1").revolution == undefined) {
            revslider_showDoubleJqueryError("#rev_slider_1014_1");
        } else {
            revapi1014 = tpj("#rev_slider_1014_1").show().revolution({
                sliderType: "standard",
                jsFileLocation: "revolution/js/",
                sliderLayout: "fullwidth",
                dottedOverlay: "none",
                delay: 2000,
                navigation: {
                    keyboardNavigation: "off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    mouseScrollReverse: "default",
                    onHoverStop: "off",
                    touch: {
                        touchenabled: "on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    },
                    arrows: {
                        enable: true,
                        style: 'hesperiden',
                        tmp: '',
                        rtl: false,
                        hide_onleave: false,
                        hide_onmobile: true,
                        hide_under: 0,
                        hide_over: 9999,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                    }
                },
                viewPort: {
                    enable: true,
                    outof: "pause",
                    visible_area: "80%",
                    presize: false
                },
                responsiveLevels: [1240, 1024, 778, 480],
                visibilityLevels: [1240, 1024, 778, 480],
                gridwidth: [1240, 1240, 1240, 800],
                gridheight: [700, 700, 700, 700],
                lazyType: "smart",
                parallax: {
                    type: "mouse",
                    origo: "slidercenter",
                    speed: 2000,
                    levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50, 47, 48, 49, 50, 51, 55],
                    type: "mouse"
                },
                shadow: 0,
                spinner: "off",
                stopLoop: "off",
                stopAfterLoops: -1,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                hideThumbsOnMobile: "on",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false
                }
            });
        }
    }); /*ready*/
</script>
@stop