<?php
$header = json_decode($header['config']);
$social = json_decode($social['config']);
$footer = json_decode($footer['config']);
//echo "<pre>";
//print_r($social);
//echo "</pre>";
//exit;
?>

<footer class="footer_area">
    <div class="footer_widgets">
        <div class="container">
            <div class="row footer_wd_inner">
                <div class="col-lg-3 col-6">
                    <aside class="f_widget f_about_widget">
                        <img src="{!!url('images/upload/'.$header->logo)!!}" alt="Logo Long Quốc Kim" width="150px">
                        <p>Đại lý phân phối chính thức các dòng sản phẩm Công ty Cổ Phần Mondelez Kinh Đô Việt Nam.</p>
                        <ul class="nav">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
<!--                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>-->
                        </ul>
                    </aside>
                </div>
                <div class="col-lg-3 col-6">
                    <aside class="f_widget f_link_widget">
                        <div class="f_title">
                            <h3>Bánh Trung Thu Kinh Đô</h3>
                        </div>
                        <ul class="list_style">
                            <li><a href="{!!url('banh-trung-thu-kinh-do')!!}">Bánh Trung Thu Kinh Đô</a></li>
                            <li><a href="{!!url('bang-gia-banh-trung-thu-kinh-do')!!}">Bảng Giá Bánh Trung Thu</a></li>
                            <li><a href="{!!url('lien-he')!!}">Liên Hệ</a></li>
                            <li><a href="{!!url('ve-chung-toi')!!}">Về Chúng Tôi</a></li>
                        </ul>
                    </aside>
                </div>
                <div class="col-lg-3 col-6">
                    <aside class="f_widget f_link_widget">
                        <div class="f_title">
                            <h3>Thời Gian Làm Việc</h3>
                        </div>
                        <ul class="list_style">
                            <li><a href="#">Thứ 2 đến Thứ 7: 8 AM - 6 PM</a></li>
                        </ul>
                    </aside>
                </div>
                <div class="col-lg-3 col-6">
                    <aside class="f_widget f_contact_widget">
                        <div class="f_title">
                            <h3>Thông Tin Liên Hệ</h3>
                        </div>
                        <p><strong>Hotline:</strong> 091 796 6067 (Ms. Phụng)</p>
						<p><strong>Hoặc</strong> 0967 931 680 (Ms. Nga)</p>
                        <p><strong>Địa Chỉ:</strong> Số 730/2F Hương Lộ 2, Phường Bình Trị Đông A, Quận Bình Tân, TP.HCM</p>
                        <h5>Email: cty.longquockim@gmail.com</h5>
                    </aside>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_copyright">
        <div class="container">
            <div class="copyright_inner">
                <div class="float-left">
                    <h5>© Copyright Long Quốc Kim 2019 All right reserved.</h5>
                </div>
                <div class="float-right">
                    <a target="_blank" href="http://jteck.net">Phát triển website bởi JTECK</a>
                </div>
            </div>
        </div>
    </div>
</footer>

<!--================Search Box Area =================-->
<!--<div class="search_area zoom-anim-dialog mfp-hide" id="test-search">
    <div class="search_box_inner">
        <h3>Search</h3>
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="icon icon-Search"></i></button>
            </span>
        </div>
    </div>
</div>-->
<!--================End Search Box Area =================-->