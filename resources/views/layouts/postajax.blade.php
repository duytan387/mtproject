<?php
//echo "<pre>";
//print_r($listproject);
//echo "</pre>";
//exit;
?>
@foreach($listproject as $project)
<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
    <div class="block-item slideInUp animated wow addblock-item">
        <div class="item-thumbnail">
            <img src="{!!url('images/upload/post/'.$project->image)!!}" alt="product">
        </div>
        <div class="block-info">
            <h3 class="item-title">
                <a href="{!!url('du-an/'.$project->slug_vn.'/'.$project->id)!!}">{!!$project->title_vn!!}</a>
            </h3>
        </div>
    </div>
</div>
@endforeach