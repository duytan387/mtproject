<?php
//echo "<pre>";
//print_r($listProduct);
//echo "</pre>";
//exit;
?>
@extends('master')
@section('title','Đặt Hàng Thành Công')

@section('css')
@stop


@section('header')
@parent
@include('layouts.header')
@stop

@section('content')

<!--================End Main Header Area =================-->
<section class="banner_area">
    <div class="container">
        <div class="banner_text">
            <!--            <h3>Menu</h3>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li><a href="menu.html">Menu</a></li>
                        </ul>-->
        </div>
    </div>
</section>
<!--================End Main Header Area =================-->

<!--================Recipe Menu list Area =================-->
<section class="price_list_area p_100">
    <div class="container">
        <div class="our_bakery_text">
            <h1>Đặt Hàng Thành Công</h1>
            <div class="notify--title">Cảm ơn Quý khách hàng đã tin tưởng</div>
            
            <div class="row">
                <div class="form-group col-md-12">
                    <div class="redirect--text">
                    <a class="pest_btn" href="{!!url('/')!!}">Về Trang Chủ</a>
                    <!--<button type="submit" value="submit" class="btn pest_btn form-control">Submit now</button>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="row our_bakery_image">
            <div class="col-md-4 col-6">
                <img class="img-fluid" src="{!!url('img/our-bakery/bakery-1.jpg')!!}" alt="">
            </div>
            <div class="col-md-4 col-6">
                <img class="img-fluid" src="{!!url('img/our-bakery/bakery-2.jpg')!!}" alt="">
            </div>
            <div class="col-md-4 col-6">
                <img class="img-fluid" src="{!!url('img/our-bakery/bakery-3.jpg')!!}" alt="">
            </div>
        </div>
    </div>
</section>
<!--================End Main Header Area =================-->


@stop
@section('scripts')

@stop