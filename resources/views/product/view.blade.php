<?php
//echo "<pre>";
//print_r($relatedItem);
//echo "</pre>";
//exit;
?>
@extends('master')
@section('title',$viewItem['name'])




@section('header')
@parent
@include('layouts.header')
@stop

@section('content')
<!--================End Main Header Area =================-->
<section class="banner_area">
    <div class="container">
        <div class="banner_text">
            <!--<h3>Product Details</h3>-->
            <!--            <ul>
                            <li><a href="index.html">Home</a></li>
                            <li><a href="product-details.html">Product Details</a></li>
                        </ul>-->
        </div>
    </div>
</section>
<!--================End Main Header Area =================-->
<!--================Product Details Area =================-->
<section class="product_details_area p_100">
    <div class="container">
        <div class="row product_d_price">

            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-5">
                        <div class="product_img"><img class="img-fluid" src="{!!url('images/upload/product/'.$viewItem['image'])!!}" alt=""></div>
                    </div>
                    <div class="col-md-7">
                        <div class="product_details_text">
                            <h1>{!!$viewItem['name']!!}</h1>
                            <h5>Mã sản phẩm: {!!$viewItem['sku']!!}</h5>
                            <p>{!!$viewItem['description']!!}</p>
                            <h5>
							
							<?php
								if($viewItem['unit_price'] > 0){
								?>
									Giá bán: <span>{!!number_format($viewItem['unit_price'])!!} <sup>đ</sup></span>
								<?php
								}else{
									echo "Giá: liên hệ";
								}
								?>
							</h5>
                            <div class="quantity_box">
                            </div>
							<?php
								if($viewItem['unit_price'] > 0){
									?>
									<a class="pink_more" href="{!!route('themgiohang', $viewItem['id'])!!}">Đặt Hàng</a>
									<?php
								}else{
								?>
								<a class="pink_more" href="{!!url('/lien-he')!!}">Liên Hệ</a>
								<?php
								}
								?>
                            
                        </div>
                    </div>

                    <div class="product_tab_area paddingtop40 jteck-fullwidth">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Mô Tả Sản Phẩm</a>
                                <!--                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Specification</a>
                                                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Review (0)</a>-->
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                {!!$viewItem['content']!!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="product_left_sidebar">
                    <aside class="left_sidebar p_catgories_widget jteck-sidebar">
                        <div class="p_w_title">
                            <h3>Hotline</h3>
                        </div>
                        <ul class="list_style">
                            <li><a href="tel:0917966067"><i class="fa fa-phone" aria-hidden="true"></i> 091 796 6067 (Ms. Phụng)</a></li>
                            <li><a href="tel:0967931680"><i class="fa fa-phone" aria-hidden="true"></i> 0967 931 680 (Ms. Nga)</a></li>
                        </ul>
                    </aside>
                    
                    <aside class="left_sidebar p_catgories_widget jteck-sidebar">
                        <div class="p_w_title">
                            <h3>Bánh Trung Thu</h3>
                        </div>
                        <ul class="list_style">
                            @if($listCate)
                            @foreach($listCate as $cate)
                            <li><a href="{!!url('san-pham/'.$cate->slug)!!}">{!!$cate->name!!}</a></li>
                            @endforeach
                            @endif
                        </ul>
                    </aside>
                    @include('layouts.sidebar')
                </div>
            </div>

        </div>

    </div>
</section>
<!--================End Product Details Area =================-->

<!--================Similar Product Area =================-->
<section class="similar_product_area p_100">
    <div class="container">
        <div class="main_title">
            <h2>Bánh Trung Thu Khác</h2>
        </div>
        <div class="row similar_product_inner">
            @if($relatedItem)
            @foreach($relatedItem as $related)
            <div class="col-lg-3 col-md-4 col-6">
                <div class="cake_feature_item">
                    <div class="cake_img">
                        <img src="{!!url('images/upload/product/'.$related['image'])!!}" alt="{!!$related['name']!!}">
                    </div>
                    <div class="cake_text">
                        <h4>{!!number_format($related['unit_price'])!!} <sup>đ</sup></h4>
                        <h3>{!!$related['name']!!}</h3>
                        <a class="pest_btn" href="{!!url('banh-trung-thu-kinh-do/'.$related['slug'])!!}">Chi Tiết</a>
                        <a class="pest_btn" href="{!!route('themgiohang', $related['id'])!!}">Đặt Hàng</a>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>
</section>
<!--================End Similar Product Area =================-->


@stop
