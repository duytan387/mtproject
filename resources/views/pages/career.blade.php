@extends('master')
@section('title',$getpage->title_vn)
@section('meta')
@include('layouts.metatag')
@stop

@section('content')

<!-- SLIDER START -->
{{--@section('bannertop')
@include('layouts.banner')
@show--}}
<!-- SLIDER END -->
<!-- BREADCRUMB ROW -->                            
<div class="bg-gray-light p-tb20">
    <div class="container">
        <ul class="wt-breadcrumb breadcrumb-style-1">
            <li><a href="{!!url('/')!!}">Trang Chủ</a></li>
            <li>Tuyển Dụng</li>
        </ul>
    </div>
</div>
<!-- BREADCRUMB ROW END -->                   

<!-- SECTION CONTENT -->
<div class="section-full p-t80 p-b50 bg-gray">
    <div class="container">
        <div class="row">

            <!-- BLOG POST START -->
            @if($listcareer)
            @foreach($listcareer as $career)
            <!-- COLUMNS 1 -->
            <div class="blog-post blog-md date-style-1 clearfix">

                <div class="wt-post-media wt-img-effect zoom-slow">
                    <a href="{!!url('tuyen-dung/'.$career->slug_vn)!!}"><img class="lazy" src="{!!url('images/lazy-image.jpg')!!}" data-src="{!!url('images/upload/post/'.$career->image)!!}" alt="{!!$career->title_vn!!}"></a>
                </div>
                <div class="wt-post-info">

                    <div class="wt-post-title ">
                        <h3 class="post-title"><a href="{!!url('tuyen-dung/'.$career->slug_vn)!!}">{!!$career->title_vn!!}</a></h3>
                    </div>
                    <div class="wt-post-meta ">
                        <ul>
                            <li class="post-date"> <i class="fa fa-calendar"></i>{!!date("d/m/Y",strtotime($career->created_at))!!}</li>
                            
                            
                        </ul>
                    </div>
                    <div class="wt-post-text">
                        {!!$career->description_vn!!}
                    </div>
                    <div class="wt-post-readmore">
                        <a href="{!!url('tuyen-dung/'.$career->slug_vn)!!}" title="Xem Thêm" rel="bookmark" class="site-button">Xem Thêm</a>
                    </div>
                </div>
            </div>
            @endforeach
            @endif

            @section('pagination')
                @include('pagination.default', ['paginator' => $listcareer->setPath($cateslug)])
            @show

        </div>
    </div>
</div>
<!-- SECTION CONTENT END -->
@stop

@section('scripts')
@parent
<script type="text/javascript" src="{!!url('plugins/jquery.lazy.min.js')!!}"></script>
<script type="text/javascript">
    jQuery(function () {
        jQuery('.lazy').Lazy();
    });
</script>
@stop