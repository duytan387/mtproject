<!doctype html>
<html lang="en">
    <head>

        <meta charset="utf-8">

        <title>Laravel AJAX Pagination with JQuery</title>
        <meta name="_token" content="{!!csrf_token()!!}" />

    </head>
    <body>

        <h1>Posts</h1>

        <div class="posts">
            <div id="moreposts">
                @include('layouts.project')

            </div>

            <button type="submit" data-bind="{!!$next_page!!}" class="clickMe">Show More</button>
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script>

            jQuery(document).ready(function () {
                jQuery.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                var pageNumber = 2;
                jQuery(document).on('click', '.clickMe', function () {

                    jQuery(".clickMe").html("Loading....");
                    jQuery.ajax({

                        dataType: 'json',
                        type: 'GET',
                        url: "layouts/project",
                        data: {
                            page: pageNumber
                        },
                        success: function (data)
                        {
//                alert(data.length);
                            console.log(data);
                            pageNumber += 1;
                            if (data != "")
                            {
                                jQuery('#moreposts').append(data);
                                jQuery(".clickMe").html("Show more");

                            } else
                            {
                                jQuery('.clickMe').html("No Data");
                            }

                        }
                    })
                });
            });

        </script>

    </body>
</html>