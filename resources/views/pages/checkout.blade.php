<?php
//echo "<pre>";
//print_r($product_cart);
//echo "</pre>";
//exit;
?>
@extends('master')
@section('title','Thanh Toán')
@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}" />
@stop
@section('css')
<link href="{!!url('vendors/jquery-ui/jquery-ui.min.css')!!}" rel="stylesheet">
<link href="{!!url('vendors/nice-select/css/nice-select.css')!!}" rel="stylesheet">
@stop


@section('header')
@parent
@include('layouts.header')
@stop

@section('content')
<!--================End Main Header Area =================-->
<section class="banner_area">
    <div class="container">
        <div class="banner_text">
            <!--            <h3>Cart</h3>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li><a href="cart.html">Cart</a></li>
                        </ul>-->
        </div>
    </div>
</section>
<!--================End Main Header Area =================-->


<!--================Billing Details Area =================-->    
<section class="billing_details_area p_100">
    <div class="container">
        <div class="main_title">
            <h1>Thanh Toán</h1>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <form class="billing_form" action="#" method="post" id="contactForm" novalidate="novalidate">
                    <input type="hidden" name="_token" value="{!!csrf_token()!!}">
                    <input type="hidden" id="status" name="status" value="chưa giải quyết">
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="main_title">
                                <h3>Thông Tin Thanh Toán</h3>
                            </div>
                            <div class="billing_form_area row">
                                <div class="form-group col-md-12">
                                    <label for="name">Họ Tên*</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Họ Tên">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="phone">Phone</label>
                                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="address">Địa Chỉ *</label>
                                    <input type="text" class="form-control" id="address" name="address" placeholder="Street Address">
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="district">Quận *</label>
                                    <select class="product_select" id="district" name="district">
                                        <option data-code="null" value="null"  data-display="Chọn Quận/Huyện" selected="">Chọn Quận/Huyện</option>
                                        <option data-code="HC466" value="Quận 1">Quận 1</option>
                                        <option data-code="HC467" value="Quận 2">Quận 2</option>
                                        <option data-code="HC468" value="Quận 3">Quận 3</option>
                                        <option data-code="HC469" value="Quận 4">Quận 4</option>
                                        <option data-code="HC470" value="Quận 5">Quận 5</option>
                                        <option data-code="HC471" value="Quận 6">Quận 6</option>
                                        <option data-code="HC472" value="Quận 7">Quận 7</option>
                                        <option data-code="HC473" value="Quận 8">Quận 8</option>
                                        <option data-code="HC474" value="Quận 9">Quận 9</option>
                                        <option data-code="HC475" value="Quận 10">Quận 10</option>
                                        <option data-code="HC476" value="Quận 11">Quận 11</option>
                                        <option data-code="HC477" value="Quận 12">Quận 12</option>
                                        <option data-code="HC478" value="Quận Gò Vấp">Quận Gò Vấp</option>
                                        <option data-code="HC479" value="Quận Tân Bình">Quận Tân Bình</option>
                                        <option data-code="HC480" value="Quận Bình Thạnh">Quận Bình Thạnh</option>
                                        <option data-code="HC481" value="Quận Phú Nhuận">Quận Phú Nhuận</option>
                                        <option data-code="HC482" value="Quận Thủ Đức">Quận Thủ Đức</option>
                                        <option data-code="HC483" value="Huyện Củ Chi">Huyện Củ Chi</option>
                                        <option data-code="HC484" value="Huyện Hóc Môn">Huyện Hóc Môn</option>
                                        <option data-code="HC485" value="Huyện Bình Chánh">Huyện Bình Chánh</option>
                                        <option data-code="HC486" value="Huyện Nhà Bè">Huyện Nhà Bè</option>
                                        <option data-code="HC487" value="Huyện Cần Giờ">Huyện Cần Giờ</option>
                                        <option data-code="HC679" value="Quận Bình Tân">Quận Bình Tân</option>
                                        <option data-code="HC680" value="Quận Tân Phú">Quận Tân Phú</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="message">Ghi Chú</label>
                                    <textarea class="form-control" name="message" id="message" rows="1" placeholder="Note about your order. e.g. special note for delivery"></textarea>
                                </div>

                                <div class="error"></div>
                                <div class="alert"></div>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="order_box_price">
                                <div class="main_title">
                                    <h3>Đơn Hàng</h3>
                                </div>
                                <div class="payment_list">
                                    <div class="price_single_cost">
                                        <h5>Sản Phẩm <span>Tổng Cộng</span></h5>
                                        @if(Session::has('cart'))
                                        @foreach($product_cart as $product)
                                        <h5>
                                            {!!$product['item']['name']!!} x {!!$product['qty']!!} 
                                            <span>
                                                <?php
                                                echo number_format($product['item']['unit_price'] * $product['qty']);
                                                ?>
                                                <sup>đ</sup>
                                            </span>
                                        </h5>
                                        @endforeach
                                        @endif
                                        <h3 class="font--red">
                                            Tổng Cộng: <span>@if(Session::has('cart')){{number_format($totalPrice)}} @else 0 @endif <sup>đ</sup></span>
                                        </h3>
                                        <!--<h3>Total <span>$65.00</span></h3>-->
                                    </div>
                                    <div id="accordion" class="accordion_area">
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <h5 class="mb-0">
                                                    <input id="cod" type="radio" name="payment_method" value="Chuyển Khoản" checked="" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                    <label for="cod">Chuyển Khoản</label>
                                                </h5>
                                            </div>
                                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                <div class="card-body">
                                                    <p>- Tên TK: ...</p>
                                                    <p>- Số TK:  ...</p>
                                                    <p>- Ngân Hàng ...</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="headingTwo">
                                                <h5 class="mb-0">
                                                    <input type="radio" id="cash" name="payment_method" value="Tiền Mặt" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                    <label for="cash">Tiền Mặt</label>
                                                </h5>
                                            </div>
                                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                                <div class="card-body">
                                                    Nhận tiền khi giao hàng
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div class=" row">
                                <div class="form-group col-md-12">
                                    <button type="submit" value="submit" class="btn pest_btn">Thanh Toán</button>
                                </div>
                            </div>
                        </div>


                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!--================End Billing Details Area =================-->   

@stop
@section('scripts')
<script>
    var j = jQuery;
    j(document).ready(function () {
        j('#contactForm').submit(function (e) {
            j('.error').text('');
            j('.alert').text('');
            e.preventDefault();
            j.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': j('meta[name="csrf-token"]').attr('content')
                }
            });
            j.ajax({
                url: "{!!action('CartController@setPayment')!!}",
                method: 'post',
                data: {
                    name: j('#name').val(),
                    email: j('#email').val(),
                    phone: j('#phone').val(),
                    address: j('#address').val(),
                    district: j('#district').val(),
                    message: j('#message').val(),
                    status: j('#status').val(),
                    payment_method: j('input:checked').val()
                },
                success: function (data) {
                    j("#contactForm").trigger('reset');
                    if (j.type(data.result) === "object") {
                        j.each(data.result, function (i, v) {
                            j('.error').append('<p>*' + v + '</p>');
//                                alert(v);
//                                console.log(v);
                        });
                        if (data.hoten != '') {
                            j('#name').val(data.name);
                        }
                        if (data.phone != '') {
                            j('#phone').val(data.phone);
                        }
                        if (data.quan != '') {
                            j('#district').val(data.district);
                        }
                        if (data.message != '') {
                            j('#message').val(data.message);
                        }
                        if (data.email != '') {
                            j('#email').val(data.email);
                        }
                        if (data.subject != '') {
                            j('#address').val(data.address);
                        }
                        j('.alert').html('');
                    } else {
                        j('.alert').html(data.result);
                        j('.error').html('');
                        j(location).attr("href", "{!!url('/san-pham/dat-hang-thanh-cong')!!}");
                    }
                },
                fail: function (data) {
                    j('.alert').html(data.fail)
                }
            });
        });
    });
</script>
@stop