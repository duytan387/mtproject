<?php
//echo "<pre>";
//print_r($listnews);
//echo "</pre>";
//exit;
?>
@extends('master')
@section('title',$metapage->title_vn)
@section('meta')
<meta property="og:locale" content="vi_VN" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{$metapage->seotitle}}" />
<meta property="og:description" content="{{$metapage->seodescription}}" />        
<meta property="og:url" content="{!!url('/'.$metapage->slug_vn)!!}" />        
<meta property="og:site_name" content="Vuông Tròn Group" />        
<meta property="og:image" content="{!!url('images/upload/page/'.$metapage->image);!!}" />
<meta property="og:image:width" content="600" />
<meta property="og:image:height" content="450" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="{{$metapage->seotitle}}" />
<meta name="twitter:image" content="{!!url('images/upload/page/'.$metapage->image)!!}" />
<meta property="fb:app_id" content="344164529490489" />
<meta name="generator" content="motive">  
@stop
@section('layouts.banner')
@stop
@section('content')
<section class="ntp-section">
    <div class="container">
        <div class="row ntp-block-news">
            <div class="text-center m-bottom-40">
                <h3 class="section-title">{!!$metapage->title_vn!!}</h3>
                <p class="section-description">{!!$metapage->description_vn!!}</p>
            </div>
            <div class="col-lg-12 text-center">
                {!!$metapage->content_vn!!}
            </div>
        </div>
    </div>  
</section>
@stop