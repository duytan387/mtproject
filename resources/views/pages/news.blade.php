<?php
//echo "<pre>";
//print_r($listProduct);
//echo "</pre>";
//exit;
?>
@extends('master')
@section('title',$getpage->title_vn)

@section('css')
@stop


@section('header')
@parent
@include('layouts.header')
@stop

@section('content')
<!--================End Main Header Area =================-->
<section class="banner_area">
    <div class="container">
        <div class="banner_text">
<!--            <h3>Blog</h3>
            <ul>
                <li><a href="index.html">Home</a></li>
                <li><a href="blog.html">Blog</a></li>
            </ul>-->
        </div>
    </div>
</section>
<!--================End Main Header Area =================-->

<!--================Blog Main Area =================-->
<section class="main_blog_area p_100">
    <div class="container">
        <div class="blog_area_inner">
            <div class="main_blog_column row">
                @if($listnews)
                @foreach($listnews as $news)
                <div class="col-md-4">
                    <div class="blog_item">
                        <div class="blog_img">
                            <img class="img-fluid" src="{!!url('images/upload/post/'.$news->image)!!}" alt="{!!$news->title_vn!!}">
                        </div>
                        <div class="blog_text">
                            <a href="{!!url('tin-tuc/'.$news->slug_vn)!!}"><h4>{!!$news->title_vn!!}</h4></a>
                            <p>{!!$news->description_vn!!}</p>
                            <a class="pink_more" href="{!!url('tin-tuc/'.$news->slug_vn)!!}">Chi Tiết</a>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
<!--            <nav aria-label="Page navigation example" class="blog_pagination">
                <ul class="pagination">
                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                </ul>
            </nav>-->
        </div>
    </div>
</section>
<!--================End Blog Main Area =================-->

@stop
@section('scripts')

@stop