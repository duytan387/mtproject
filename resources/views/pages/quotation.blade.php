<?php
//echo "<pre>";
//print_r($listProduct);
//echo "</pre>";
//exit;
?>
@extends('master')
@section('title',$getpage->title_vn)

@section('css')
@stop


@section('header')
@parent
@include('layouts.header')
@stop

@section('content')

<!--================End Main Header Area =================-->
<section class="banner_area">
    <div class="container">
        <div class="banner_text">
            <!--            <h3>Menu</h3>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li><a href="menu.html">Menu</a></li>
                        </ul>-->
        </div>
    </div>
</section>
<!--================End Main Header Area =================-->

<!--================Recipe Menu list Area =================-->
<section class="price_list_area p_100">
    <div class="container">
        <div class="row price_list_inner">
            <div class="col-md-9">
                <div class="single_pest_title">
                    <h1>{!!$getpage->title_vn!!}</h1>
                    {!!$getpage->content_vn!!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="product_left_sidebar">
                    <aside class="left_sidebar p_catgories_widget jteck-sidebar">
                        <div class="p_w_title">
                            <h3>Hotline</h3>
                        </div>
                        <ul class="list_style">
                            <li><a href="tel:0917966067"><i class="fa fa-phone" aria-hidden="true"></i> 091 796 6067 (Ms. Phụng)</a></li>
                            <li><a href="tel:0967931680"><i class="fa fa-phone" aria-hidden="true"></i> 0967 931 680 (Ms. Nga)</a></li>
                        </ul>
                    </aside>

                    <aside class="left_sidebar p_catgories_widget jteck-sidebar">
                        <div class="p_w_title">
                            <h3>Bánh Trung Thu</h3>
                        </div>
                        <ul class="list_style">
                            @if($listCate)
                            @foreach($listCate as $cate)
                            <li><a href="{!!url('san-pham/'.$cate->slug)!!}">{!!$cate->name!!}</a></li>
                            @endforeach
                            @endif
                        </ul>
                    </aside>
					@include('layouts.sidebar')
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Main Header Area =================-->


@stop
@section('scripts')

@stop