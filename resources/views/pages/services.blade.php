<?php
//echo "<pre>";
//print_r($listservice);
//echo "</pre>";
//exit;
?>
@extends('master')
@section('title',strip_tags($getpage->title_vn))
@section('meta')
@parent
<meta name="csrf-token" content="{!!csrf_token()!!}" />
@include('layouts.metatag')
@stop

<!-- Header -->
@section('header')
@parent
@include('layouts.header')
@stop
<!-- //Header -->

@section('banner')
@parent
<div class="banner-top">
    <div class="container">
        <div class="image margin-30">
            <img class="lazy" src="{!!url('images/lazyimage.jpg')!!}" data-src="images/dich-vu-top.jpg" alt="Dich Vu Image">
        </div>
        <div class="content">
            <h1 class="title">{!!$getpage->title_vn!!}</h1>
            {!!$getpage->content_vn!!}
        </div>
    </div>
</div>
@stop
<!-- End Slider area -->
@section('content')
<main class="content">
    <div class="list-gallery">
        <h2 class="title">Sản phẩm học viên</h2>
        <div class="gallery-item">
            <?php
            foreach ($getgallery as $count => $gallery) {
                if ($count == 0) {
                    echo '<div class="item">';
                    echo '<a class="fancybox-thumb" data-fancybox="images" data-type="image" href="' . url('images/upload/gallery/' . $gallery->image) . '"><img class="lazy" src="'.url('images/lazyimage.jpg').'" data-src="' . url('images/upload/gallery/' . $gallery->image) . '" alt="' . $count . '"></a>';
                    echo '</div>';
                    echo '<div class="four-item item">';
                } elseif ($count == 1) {
                    echo '<a class="fancybox-thumb" data-fancybox="images" data-type="image" href="' . url('images/upload/gallery/' . $gallery->image) . '"><img class="lazy" src="'.url('images/lazyimage.jpg').'" data-src="' . url('images/upload/gallery/' . $gallery->image) . '" alt="' . $count . '"></a>';
                } elseif ($count % 2 == 0 && $count < 3) {
                    echo '<a class="fancybox-thumb" data-fancybox="images" data-type="image" href="' . url('images/upload/gallery/' . $gallery->image) . '"><img class="lazy" src="'.url('images/lazyimage.jpg').'" data-src="' . url('images/upload/gallery/' . $gallery->image) . '" alt="' . $count . '"></a>';
                } elseif ($count % 3 == 0 && $count < 4) {
                    echo '<a class="fancybox-thumb" data-fancybox="images" data-type="image" href="' . url('images/upload/gallery/' . $gallery->image) . '"><img class="lazy" src="'.url('images/lazyimage.jpg').'" data-src="' . url('images/upload/gallery/' . $gallery->image) . '" alt="' . $count . '"></a>';
                } elseif ($count % 4 == 0 && $count < 5) {
                    echo '<a class="fancybox-thumb" data-fancybox="images" data-type="image" href="' . url('images/upload/gallery/' . $gallery->image) . '"><img class="lazy" src="'.url('images/lazyimage.jpg').'" data-src="' . url('images/upload/gallery/' . $gallery->image) . '" alt="' . $count . '"></a>';
                    echo '</div>';
                } elseif ($count % 5 == 0) {
                    echo '<div class="item">';
                    echo '<a class="fancybox-thumb" data-fancybox="images" data-type="image" href="' . url('images/upload/gallery/' . $gallery->image) . '"><img class="lazy" src="'.url('images/lazyimage.jpg').'" data-src="' . url('images/upload/gallery/' . $gallery->image) . '" alt="' . $count . '"></a>';
                    echo '</div>';
                    echo '<div class="four-item item">';
                } elseif ($count % 6 == 0) {
                    echo '<a class="fancybox-thumb" data-fancybox="images" data-type="image" href="' . url('images/upload/gallery/' . $gallery->image) . '"><img class="lazy" src="'.url('images/lazyimage.jpg').'" data-src="' . url('images/upload/gallery/' . $gallery->image) . '" alt="' . $count . '"></a>';
                } elseif ($count % 7 == 0) {
                    echo '<a class="fancybox-thumb" data-fancybox="images" data-type="image" href="' . url('images/upload/gallery/' . $gallery->image) . '"><img class="lazy" src="'.url('images/lazyimage.jpg').'" data-src="' . url('images/upload/gallery/' . $gallery->image) . '" alt="' . $count . '"></a>';
                } elseif ($count % 8 == 0) {
                    echo '<a class="fancybox-thumb" data-fancybox="images" data-type="image" href="' . url('images/upload/gallery/' . $gallery->image) . '"><img class="lazy" src="'.url('images/lazyimage.jpg').'" data-src="' . url('images/upload/gallery/' . $gallery->image) . '" alt="' . $count . '"></a>';
                } elseif ($count % 9 == 0) {
                    echo '<a class="fancybox-thumb" data-fancybox="images" data-type="image" href="' . url('images/upload/gallery/' . $gallery->image) . '"><img class="lazy" src="'.url('images/lazyimage.jpg').'" data-src="' . url('images/upload/gallery/' . $gallery->image) . '" alt="' . $count . '"></a>';
                    echo '</div>';
                }
            }
            ?>
        </div>
    </div>
    <div class="feedback-hv">
        <div class="box-container">
            <h2 class="title">Feedback học viên</h2>
            <div class="list-feedback owl-carousel owl-theme">
                @foreach($listcustomer as $customer)
                <div class="item">
                    <div class="image"><img class="lazy" src="{!!url('images/lazyimage.jpg')!!}" data-src="{!!url('images/upload/customer/'.$customer->image)!!}" alt="{!!$customer->name!!}"></div>
                    <div class="content">
                        <div class="box-content">
                            <h3 class="name">{!!$customer->name!!}</h3>
                            <h4 class="position">{!!$customer->position!!}</h4>
                            <p>{!!$customer->comment!!}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="recently-course">
        <div class="box-container">
            <h2 class="title">Các khoá học gần đây</h2>
            <div class="box-course">
                <div class="image">
                    <div class="list-image owl-carousel owl-theme">
                        <div class="item">
                            <img class="lazy" src="{!!url('images/lazyimage.jpg')!!}" data-src="{!!url('images/upload/post/'.$getpostfirst->image)!!}" alt="Course Image">
                        </div>
                        <!--div class="item">
                            <img class="lazy" src="{!!url('images/lazyimage.jpg')!!}" data-src="images/khoa-hoc.jpg" alt="Course Image">
                        </div>
                        <div class="item">
                            <img class="lazy" src="{!!url('images/lazyimage.jpg')!!}" data-src="images/khoa-hoc.jpg" alt="Course Image">
                        </div-->
                    </div>
                    <!-- <img class="bg-icon" src="images/bg-icon-khoa-hoc.png" alt="Course Image"> -->
                </div>
                <div class="content">
                    <h3 class="title">{!!$getpostfirst->title_vn!!}</h3>
                    <h4 class="date">Ngày khai giảng : {!!($getpostfirst->updated_at !== "0000-00-00 00:00:00")?date("d-m-Y",strtotime($getpostfirst->updated_at)):date("d-m-Y",strtotime($getpostfirst->created_at))!!}</h4>
                    {!!$getpostfirst->description_vn!!}
                    <div class="register">
                        <a class="btn-register" href="#register-form">Đăng Ký</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="other-services">
        <div class="box-container">
            <h2 class="title">Dịch vụ khác</h2>
            <div class="list-service">
                @foreach($getpostfeature as $count => $postfeature)
                <div class="item">
                    <div class="image">
                        <img class="lazy" src="{!!url('images/lazyimage.jpg')!!}" data-src="{!!url('images/upload/post/'.$postfeature->image)!!}" alt="{!!$postfeature->title_vn!!}">
                    </div>
                    <div class="content">
                        <h3 class="title">{!!$postfeature->title_vn!!}</h3>
                        <p>{!!$postfeature->description_vn!!}</p>
                    </div>
                    <div class="group-btn">
                        <a class="btn-detail" href="{!!url('dich-vu/'.$postfeature->slug_vn)!!}">Xem chi tiết</a>
                        <a id="btn-register<?php echo $count; ?>" class="btn-register" onclick="" href="#register-form" data-course="{!!strip_tags($postfeature->title_vn)!!}">Đăng Ký</a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</main>

<!-- Footer -->
@section('footer')
@parent
@stop
<!-- End Footer -->
<div class="form-register" id="register-form" style="display: none;">
    <h2 class="title">Đăng ký khóa học</h2>
    <form id="courseform" class="form-content" method="POST" action="">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="form-group">
            <label>Họ tên</label>
            <input type="text" class="form-control" id="name" name="name" value="">
        </div>
        <div class="form-group">
            <label>Số điện thoại</label>
            <input type="text" class="form-control" id="phone" name="phone" value="">
        </div>
        <div class="form-group select">
            <label>Đăng ký khóa</label>
            <select name="course" class="form-control" id="course">
                @foreach($listservice as $service)
                <option value="{!!strip_tags($service->title_vn)!!}">{!!$service->title_vn!!}</option>
                @endforeach
            </select>
        </div>
        <div class="alert"></div>
        <div class="error"></div>
        <div class="form-group submit">
            <button id="btn_submit" type="submit" class="btn btn-primary">Đăng Ký</button>
        </div>
    </form>
</div>
<!-- Popup -->
@section('popup')
@include('vendor.popup')
@stop
<!-- End Popup -->
@stop

@section('scripts')
<script type="text/javascript" src="{!!url('plugins/jquery.lazy.min.js')!!}"></script>
<script type="text/javascript">
jQuery(function ($) {
    // lazy load
    $(function() {
        $('.lazy').Lazy();
    });
    
    // ajax
    $(document).ready(function () {
        $("#courseform").submit(function (e) {
			$('.error').text('');
			$('.alert').text('');
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "{!!action('CourseController@create')!!}",
                method: 'post',
                data: {
                    name: $('#name').val(),
                    phone: $('#phone').val(),
                    course: $('#course').val()
                },
                success: function (data) {
                    $("#courseform").trigger('reset');
//                    $('.alert').html(data.result);
                    if($.type(data.result) === "object"){
                        $.each(data.result, function(i, v){
                            $('.error').append('<p>*' + v + '</p>');
                        });
                        if(data.hoten != ''){
                            $('#name').val(data.hoten);
                        }
                        if(data.phone != ''){
                            $('#phone').val(data.phone);
                        }
                        if(data.course != ''){
                            $('#course').val(data.course);
                        }
                        $('.alert').html('');
                    }else{
                        $('.alert').html(data.result);
                        $('.error').html('');
                    }
                    
//                    console.log($.type(data.result));
//                    console.log(data.result);
                },
                fail: function (data) {
                    $('.alert').html(data.fail);
                }
            });
        });
    });
});
</script>
@stop