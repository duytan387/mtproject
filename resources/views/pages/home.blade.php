<?php
//echo "<pre>";
//print_r($firstnews);
//echo "</pre>";
//exit;
?>
@extends('master')
@section('title',$gethome->title_vn)

@section('meta')
@parent
@include('layouts.metatag')
@stop


@section('header')
@parent
@include('layouts.header')
@stop


@section('banner')
@parent
@include('layouts.banner')
@stop

@section('content')
<section class="welcome_bakery_area cake_feature_main p_100">
    <div class="container">
        <div class="main_title">
            <h1>Bánh Trung Thu Kinh Đô 2019</h1>
            <h5> Seldolor sit amet consect etur</h5>
        </div>
        <div class="cake_feature_row row">
            @if($listProduct)
            @foreach($listProduct as $product)
            <div class="col-lg-3 col-md-4 col-6">
                <div class="cake_feature_item">
                    <div class="cake_img">
                        <img src="{!!url('images/upload/product/'.$product['image'])!!}" alt="{!!$product['name']!!}"></a>
                    </div>
                    <div class="cake_text">
                        <h4>{!!number_format($product['unit_price'])!!} <sup>đ</sup></h4>
                        <h3>{!!$product['name']!!}</h3>
                        <a class="pest_btn" href="{!!url('san-pham/'.$product['product_type']['slug'].'/'.$product['slug'])!!}">Chi Tiết</a>
                        <a class="pest_btn" href="{!!route('themgiohang', $product['id'])!!}">Đặt Hàng</a>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
            
        </div>
    </div>
</section>

<!--<section class="welcome_bakery_area cake_feature_main p_100">
    <div class="container">
        <div class="main_title">
            <h2>Bánh Trung Thu Kinh Đô Cao Cấp</h2>
            <h5> Seldolor sit amet consect etur</h5>
        </div>
        <div class="cake_feature_row row">
            <div class="col-lg-3 col-md-4 col-6">
                <div class="cake_feature_item">
                    <div class="cake_img">
                        <img src="img/cake-feature/c-feature-1.jpg" alt="">
                    </div>
                    <div class="cake_text">
                        <h4>$29</h4>
                        <h3>Strawberry Cupcakes</h3>
                        <a class="pest_btn" href="#">Chi Tiết</a>
                        <a class="pest_btn" href="#">Đặt Hàng</a>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>-->

<!--================Discover Menu Area =================-->
<!--<section class="discover_menu_area menu_d_two">
    <div class="discover_menu_inner">
        <div class="container">
            <div class="single_pest_title">
                <h2>Bảng Giá Bánh Trung Thu 2019</h2>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="discover_item_inner">
                        <div class="discover_item">
                            <h4>Double Chocolate Pie</h4>
                            <p>Chocolate puding, vanilla, fruite rasberry jam milk <span>$8.99</span></p>
                        </div>
                        <div class="discover_item">
                            <h4>Double Chocolate Pie</h4>
                            <p>Chocolate puding, vanilla, fruite rasberry jam milk <span>$8.99</span></p>
                        </div>
                        <div class="discover_item">
                            <h4>Double Chocolate Pie</h4>
                            <p>Chocolate puding, vanilla, fruite rasberry jam milk <span>$8.99</span></p>
                        </div>
                        <div class="discover_item">
                            <h4>Double Chocolate Pie</h4>
                            <p>Chocolate puding, vanilla, fruite rasberry jam milk <span>$8.99</span></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="discover_item_inner">
                        <div class="discover_item">
                            <h4>Double Chocolate Pie</h4>
                            <p>Chocolate puding, vanilla, fruite rasberry jam milk <span>$8.99</span></p>
                        </div>
                        <div class="discover_item">
                            <h4>Double Chocolate Pie</h4>
                            <p>Chocolate puding, vanilla, fruite rasberry jam milk <span>$8.99</span></p>
                        </div>
                        <div class="discover_item">
                            <h4>Double Chocolate Pie</h4>
                            <p>Chocolate puding, vanilla, fruite rasberry jam milk <span>$8.99</span></p>
                        </div>
                        <div class="discover_item">
                            <h4>Double Chocolate Pie</h4>
                            <p>Chocolate puding, vanilla, fruite rasberry jam milk <span>$8.99</span></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <a class="pest_btn" href="#">View Full Menu</a>
                </div>
            </div>
        </div>
    </div>
</section>-->
<!--================End Discover Menu Area =================-->

<!--================Latest News Area =================-->
<section class="latest_news_area gray_bg p_100">
    <div class="container">
        <div class="main_title">
            <h2>Tin Tức</h2>
            <h5>an turn into your instructor your helper, your </h5>
        </div>
        <div class="row latest_news_inner">
            @if($listnews)
            @foreach($listnews as $news)
            <div class="col-lg-4 col-md-6">
                <div class="l_news_item">
                    <div class="l_news_img">
                        <img class="img-fluid" src="{!!url('images/upload/post/'.$news->image)!!}" alt="">
                    </div>
                    <div class="l_news_text">
                        <a href="{!!url('tin-tuc/'.$news->slug_vn)!!}"><h5>{!!$news->created_at!!}</h5></a>
                        <a href="{!!url('tin-tuc/'.$news->slug_vn)!!}"><h4>{!!$news->title_vn!!}</h4></a>
                        <p>{!!$news->description_vn!!}</p>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>
</section>
<!--================End Latest News Area =================-->

<!--================Newsletter Area =================-->
<!--<section class="newsletter_area gray_bg">
    <div class="container">
        <div class="newsletter_inner">
            <div class="row">
                <div class="col-lg-6">
                    <div class="news_left_text">
                        <h4>Join our Newsletter list to get all the latest offers, discounts and other benefits</h4>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="newsletter_form">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Enter your email address">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button">Subscribe Now</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>-->
<!--================End Newsletter Area =================-->




@stop

@section('scripts')
@parent
<script type="text/javascript" src="{!!url('plugins/jquery.lazy.min.js')!!}"></script>
<script type="text/javascript">
    jQuery(function ($) {
        // lazy load
        $(function () {
            $('.lazy').Lazy();
        });
    });
</script>
@stop