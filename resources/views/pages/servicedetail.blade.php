<?php
//echo "<pre>";
//print_r($listnews);
//echo "</pre>";
//exit;
?>
@extends('master')
@section('title',strip_tags($getpost->title_vn))

@section('meta')
@parent
@include('layouts.metatag')
@stop

<!-- Header -->
@section('header')
@parent
@include('layouts.header')
@stop
<!-- //Header -->

@section('banner')
<div class="news banner-top">
      <div class="image">
         <img src="{!!url('images/banner-tintuc.jpg')!!}" alt="Tin Tức">
      </div>
<div class="content">
         <div class="container">
            <h2 class="title">{!!$cate_title!!}</h2>
         </div>
      </div>
   </div>
@stop
<!-- End Slider area -->
@section('content')
<main class="content">
      <div class="content-article">
         <div class="box-container">
            <div class="image detail-post">
                <img src="{!!url('/images/upload/post/'.$getpost->image)!!}" alt="{!!$getpost->title_vn!!}">
            </div>
            <div class="content">
               <h1 class="title">{!!$getpost->title_vn!!}</h1>
               <p class="date">{!!date("d/m/Y",strtotime($getpost->created_at))!!}</p>
               {!!$getpost->content_vn!!}
            </div>
         </div>
      </div>
      <div class="relate-article list-news">
         <div class="box-container">
            <h2 class="title">{!!$cate_title!!} Khác</h2>
            <div class="box-item">
                @foreach($getpostrelated as $count => $postrelated)
               <div class='news-item<?php echo ($count % 2 == 0)?" reverse":""?>'>
                  <div class="news-content">
                     <div class="image">
                         <a href="{!!url('tin-tuc/'.$postrelated->slug_vn)!!}"><img src="{!!url('/images/upload/post/'.$postrelated->image)!!}" alt="{!!$postrelated->title_vn!!}"></a>
                     </div>
                     <div class="content">
                        <h2 class="news-title">{!!$postrelated->title_vn!!}</h2>
                        <p class="date">{!!date("d/m/Y",strtotime($postrelated->created_at))!!}</p>
                        <p>{!!$postrelated->description_vn!!}</p>
                     </div>
                  </div>
               </div>
                @endforeach
            </div>
         </div>
      </div>
   </main>


<!-- Popup -->
@section('popup')
@include('vendor.popup')
@stop
<!-- End Popup -->
@stop

@section('scripts')
@parent
@include('layouts.scripts')

@stop