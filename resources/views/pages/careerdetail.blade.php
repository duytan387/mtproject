@extends('master')
@section('title',$getpost->title_vn)
@section('meta')
@include('layouts.metatag')
@stop

@section('content')

<!-- SLIDER START -->
{{--@section('bannertop')
@include('layouts.banner')
@show--}}
<!-- SLIDER END -->
<!-- BREADCRUMB ROW -->                            
<div class="bg-gray-light p-tb20">
    <div class="container">
        <ul class="wt-breadcrumb breadcrumb-style-1">
            <li><a href="{!!url('/')!!}">Trang Chủ</a></li>
            <li><a href="{!!url('/tuyen-dung')!!}">Tuyển Dụng</a></li>
        </ul>
    </div>
</div>
<!-- BREADCRUMB ROW END -->                   

<!-- SECTION CONTENT START -->
<div class="section-full p-t80 p-b50">
    <div class="container">

        <!-- BLOG START -->
        <div class="blog-post date-style-1 blog-post-single">
            <div class="post-description-area p-t30">
                <div class="wt-post-title ">
                    <h1 class="post-title">{!!$getpost->title_vn!!}</h1>
                </div>
                <div class="wt-post-meta ">
                    <ul>
                        <li class="post-date"> <i class="fa fa-calendar"></i>{!!date("d/m/Y",strtotime($getpost->created_at))!!}</li>
                    </ul>
                </div>
                <div class="wt-post-text">
                    {!!$getpost->content_vn!!}
                </div>
<!--                <div class="widget bg-white  widget_tag_cloud">
                    <h4 class="tagcloud">Tags</h4>
                    <div class="tagcloud">
                        <a href="javascript:void(0);">First tag</a>
                        <a href="javascript:void(0);">Second tag</a>
                        <a href="javascript:void(0);">Three tag</a>
                        <a href="javascript:void(0);">Four tag</a>
                        <a href="javascript:void(0);">Five tag</a>
                    </div>
                </div>-->
                <div class="wt-box">
                    <div class="wt-divider bg-gray-dark"><i class="icon-dot c-square"></i></div>
                    <div class="row  p-lr15">
                        <h4 class="tagcloud pull-left m-t5 m-b0">Chia sẻ qua:</h4>
                        <div class="widget_social_inks pull-right">
                            <ul class="social-icons social-md social-square social-dark m-b0">
                                <li><a href="{!!url('https://www.facebook.com/sharer.php?u='.action('PageController@detail',$getpost->slug_vn))!!}" class="fa fa-facebook"></a></li>
<!--                                <li><a href="javascript:void(0);" class="fa fa-twitter"></a></li>
                                <li><a href="javascript:void(0);" class="fa fa-rss"></a></li>
                                <li><a href="javascript:void(0);" class="fa fa-youtube"></a></li>
                                <li><a href="javascript:void(0);" class="fa fa-instagram"></a></li>-->
                            </ul>
                        </div>
                    </div>
                    <div class="wt-divider bg-gray-dark"><i class="icon-dot c-square"></i></div>
                </div>
            </div>
        </div>

        <div class="section-content p-t50">
            <!-- TITLE START -->
            <div class="section-head">
                <h2 class="text-uppercase">Bài Viết Liên Quan</h2>
                @if($getpostrelated)
                @foreach($getpostrelated as $related)
                <div class="post masonry-item col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="blog-post blog-grid date-style-3 date-skew">
                        <div class="wt-post-media wt-img-effect zoom-slow">
                            <a href="{!!url('tin-tuc/'.$related->slug_vn)!!}"><img src="{!!url('images/upload/post/'.$related->image)!!}" alt="{!!$related->title_vn!!}"></a>
                        </div>
                        <div class="wt-post-info p-a30 p-b15 bg-white">
                            <div class="wt-post-title ">
                                <h3 class="post-title"><a href="{!!url('tin-tuc/'.$related->slug_vn)!!}">{!!$related->title_vn!!}</a></h3>
                            </div>
                            <div class="wt-post-meta ">
                                <ul>
                                    <li class="post-date"> <i class="fa fa-calendar"></i><span> {!!date("d/m/Y",strtotime($related->created_at))!!}</span> </li>
                                </ul>
                            </div>
                            <div class="wt-post-text">
                                {!!$related->description_vn!!}
                            </div>
                            <div class="wt-post-readmore">
                                <a href="{!!url('tin-tuc/'.$related->slug_vn)!!}" title="Xem Thêm" rel="bookmark" class="site-button">Xem Thêm</a>
                            </div>

                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
            <!-- TITLE END -->

        </div>

        <!-- BLOG END -->

    </div>
</div>
<!-- SECTION CONTENT END -->
@stop

@section('assetjs')
<script>
    var tpj = jQuery;
    var revapi1014;
	
    tpj(document).ready(function() {
        if (tpj("#rev_slider_1014_1").revolution == undefined) {
            revslider_showDoubleJqueryError("#rev_slider_1014_1");
        } else {
            revapi1014 = tpj("#rev_slider_1014_1").show().revolution({
                sliderType: "standard",
                jsFileLocation: "revolution/js/",
                sliderLayout: "fullwidth",
                dottedOverlay: "none",
                delay: 9000,
                navigation: {
                    keyboardNavigation: "off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    mouseScrollReverse: "default",
                    onHoverStop: "off",
                    touch: {
                        touchenabled: "on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    },
                    arrows: {
                        style: "hermes",
                        enable: true,
                        hide_onmobile: false,
                        hide_onleave: false,
                        tmp: '<div class="tp-arr-allwrapper">	<div class="tp-arr-imgholder"></div>	<div class="tp-arr-titleholder"></div>	</div>',
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 0,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 0,
                            v_offset: 0
                        }
                    }
                },
                viewPort: {
                    enable: true,
                    outof: "pause",
                    visible_area: "80%",
                    presize: false
                },
                responsiveLevels: [1240, 1024, 778, 480],
                visibilityLevels: [1240, 1024, 778, 480],
                gridwidth: [1240, 1240, 1240, 800],
                gridheight: [700, 700, 700, 700],
                lazyType: "none",
                parallax: {
                    type: "mouse",
                    origo: "slidercenter",
                    speed: 2000,
                    levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50, 47, 48, 49, 50, 51, 55],
                    type: "mouse",
                },
                shadow: 0,
                spinner: "off",
                stopLoop: "off",
                stopAfterLoops: -1,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false,
                }
            });
        }
    }); /*ready*/
</script>
@stop