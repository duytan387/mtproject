<?php
//echo "<pre>";
//print_r($listcustomer);
//echo "</pre>";
//exit;
?>
@extends('master')
@section('title',strip_tags($getpage->title_vn))

@section('meta')
@parent
@include('layouts.metatag')
@stop

<!-- Header -->
@section('header')
@parent
@include('layouts.header')
@stop
<!-- //Header -->

@section('banner')
<div class="galary-kh banner-top">
      <div class="image">
         <img class="lazy" src="{!!url('images/lazyimage.jpg')!!}" data-src="{!!url('images/upload/page/'.$getpage->image)!!}" alt="{!!$getpage->title_vn!!}">
      </div>
<div class="content">
         <div class="container">
            <h2 class="title">{!!$getpage->title_vn!!}</h2>
         </div>
      </div>
   </div>
@stop
<!-- End Slider area -->
@section('content')
<main class="content">
      <div class="galary-kh list-galary">
         <div class="box-container">
            <div class="group-galary">
                @foreach($getgallery as $gallery)
               <div class="item">
                  <div class="box-galary">
                     <div class="image">
                        <a href="{!!url('images/upload/gallery/'.$gallery->image)!!}" data-fancybox="images" data-type="image"><img src="{!!url('images/upload/gallery/'.$gallery->image)!!}" alt="{!!strip_tags($gallery->title)!!}"></a>
                     </div>
                     <div class="content">
                        <h2 class="title">{!!$gallery->title!!}</h2>
                        <p>{!!$gallery->description!!}</p>
                     </div>
                  </div>
               </div>
                @endforeach
                
            </div>
<!--            <div class="load-more">
               <a class="btn-more" href="javascript:void(0)">Xem thêm</a>
            </div>-->
         </div>
      </div>
   </main>

<!-- Footer -->
@section('footer')
@parent
@stop
<!-- End Footer -->
<div class="form-register" id="register-form" style="display: none;">
    <h2 class="title">Đăng ký khóa học</h2>
    <form class="form-content" method="POST" action="">
        <div class="form-group">
            <label>Họ tên</label>
            <input type="text" class="form-control" id="name" name="your-name" value="">
        </div>
        <div class="form-group">
            <label>Số điện thoại</label>
            <input type="text" class="form-control" id="phone" name="your-phone" value="">
        </div>
        <div class="form-group select">
            <label>Đăng ký khóa</label>
            <select class="form-control" id="select-type">
                <option>Pro - MaJik 01</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
            </select>
        </div>
        <div class="form-group submit">
            <button type="submit" class="btn btn-primary">Đăng Ký</button>
        </div>
    </form>
</div>
<!-- Popup -->
@section('popup')
@include('vendor.popup')
@stop
<!-- End Popup -->
@stop

@section('scripts')
@parent
<script type="text/javascript" src="{!!url('plugins/jquery.lazy.min.js')!!}"></script>
<script type="text/javascript">
    jQuery(function ($) {
        // lazy load
        $(function () {
            $('.lazy').Lazy();
        });
    });
</script>

@stop