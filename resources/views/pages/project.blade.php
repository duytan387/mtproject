@extends('master')
@section('title',$getpage->title_vn)
@section('meta')
@include('layouts.metatag')
@stop

@section('content')

<!-- BREADCRUMB ROW -->                            
<div class="bg-gray-light p-tb20">
    <div class="container">
        <ul class="wt-breadcrumb breadcrumb-style-1">
            <li><a href="{!!url('/')!!}">Trang Chủ</a></li>
            <li>Dự Án</li>
        </ul>
    </div>
</div>
<!-- BREADCRUMB ROW END -->                   

<!-- SECTION CONTENT -->
<div class="section-full p-t80 p-b50 bg-gray">
    <div class="container">
        <div class="row">

            <!-- BLOG GRID START -->
            <div class="portfolio-wrap wt-blog-grid-3">
                @if($listproject)
                @foreach($listproject as $project)
                <div class="post masonry-item col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="blog-post blog-grid date-style-3 date-skew">
                        <div class="wt-post-media wt-img-effect zoom-slow">
                            <a href="{!!url('du-an/'.$project->slug_vn)!!}"><img class="lazy" src="{!!url('images/lazy-image.jpg')!!}" data-src="{!!url('images/upload/post/'.$project->image)!!}" alt="{!!$project->title_vn!!}"></a>
                        </div>
                        <div class="wt-post-info p-a30 p-b15 bg-white">
                            <div class="wt-post-title ">
                                <h3 class="post-title"><a href="{!!url('du-an/'.$project->slug_vn)!!}">{!!$project->title_vn!!}</a></h3>
                            </div>
                            <div class="wt-post-meta ">
                                <ul>
                                    <li class="post-date"> <i class="fa fa-calendar"></i><span>{!!date("d/m/Y",strtotime($project->created_at))!!}</span> </li>
                                </ul>
                            </div>
                            <div class="wt-post-text">
                                {!!$project->description_vn!!}
                            </div>
                            <div class="wt-post-readmore">
                                <a href="{!!url('du-an/'.$project->slug_vn)!!}" title="Xem Thêm" rel="bookmark" class="site-button">Xem Thêm</a>
                            </div>

                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
            <!-- BLOG GRID END -->

            <!-- PAGINATION START -->
            @section('pagination')
                @include('pagination.default', ['paginator' => $listproject->setPath($cateslug)])
            @show
            <!-- PAGINATION END -->

        </div>
    </div>
</div>
<!-- SECTION CONTENT END -->
@stop

@section('scripts')
@parent
<script type="text/javascript" src="{!!url('plugins/jquery.lazy.min.js')!!}"></script>
<script type="text/javascript">
    jQuery(function () {
        jQuery('.lazy').lazy();
    });
</script>
@stop