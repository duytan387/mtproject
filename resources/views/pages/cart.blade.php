<?php
//echo "<pre>";
//print_r($count);
//echo "</pre>";
//exit;
?>
@extends('master')
@section('title','Giỏ Hàng')

@section('css')

<link href="{!!url('vendors/jquery-ui/jquery-ui.min.css')!!}" rel="stylesheet">
<link href="{!!url('vendors/nice-select/css/nice-select.css')!!}" rel="stylesheet">
@stop


@section('header')
@parent
@include('layouts.header')
@stop

@section('content')

<!--================End Main Header Area =================-->
<section class="banner_area">
    <div class="container">
        <div class="banner_text">
            <!--            <h3>Cart</h3>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li><a href="cart.html">Cart</a></li>
                        </ul>-->
        </div>
    </div>
</section>
<!--================End Main Header Area =================-->

<!--================Cart Table Area =================-->
<section class="cart_table_area p_100">
    <div class="container">
        <div class="table-responsive">
            <div class="main_title">
            <h1>Bánh Trung Thu Kinh Đô 2019</h1>
        </div>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Hình Sản Phẩm</th>
                        <th scope="col">Tên Sản Phẩm</th>
                        <th scope="col">Đơn Giá</th>
                        <th scope="col">Số Lượng</th>
                        <th scope="col">Tổng Cộng</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    @if(Session::has('cart'))
                    @foreach($product_cart as $product)
                    <tr>
                        <td>
                            <img width="150px" src="{!!url('images/upload/product/'.$product['item']['image'])!!}" alt="">
                        </td>
                        <td>{!!$product['item']['name']!!}</td>
                        <td>{!! number_format($product['item']['unit_price']) !!} <sup>đ</sup></td>
                        <td>{!!$product['qty']!!}</td>
                        <td>
                            <?php
                            echo number_format($product['item']['unit_price'] * $product['qty']).' <sup>đ</sup>';
                            ?>
                        </td>
                        <!--<td>@if(Session::has('cart')){{number_format($totalPrice)}} @else 0 @endif đồng</td>-->
                        <td><!--a class="icon--action" href="{!! route('capnhatgiohang',$product['item']['id']) !!}"><i class="fa fa-save"></i></a-->&nbsp;&nbsp;<a class="icon--action" href="{!! route('xoagiohang',$product['item']['id']) !!}"><i class="fa fa-trash"></i></a></td>
                    </tr>
                    @endforeach
                    @endif
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <a style="float:left" class="pest_btn" href="{!!route('dathang')!!}">Thanh Toán</a>
                            <!--<button type="submit" value="submit" class="btn pest_btn">Cập Nhật</button>-->
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
<!--        <div class="row cart_total_inner">
            <div class="col-lg-7"></div>
            <div class="col-lg-5">
                <div class="cart_total_text">
                    <div class="cart_head">
                        Cart Total
                    </div>
                    <div class="sub_total">
                        <h5>Sub Total <span>$25.00</span></h5>
                    </div>
                    <div class="total">
                        <h4>Total <span>$25.00</span></h4>
                    </div>
                    <div class="cart_footer">
                        <a class="pest_btn" href="#">Proceed to Checkout</a>
                    </div>
                </div>
            </div>
        </div>-->
    </div>
</section>
<!--================End Cart Table Area =================-->

@stop
@section('scripts')
<script src="{!!url('vendors/nice-select/js/jquery.nice-select.min.js')!!}"></script>
<script src="{!!url('vendors/jquery-ui/jquery-ui.min.js')!!}"></script>
<script src="{!!url('vendors/lightbox/simpleLightbox.min.js')!!}"></script>
@stop