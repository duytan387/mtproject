<?php
//echo "<pre>";
//print_r($listProduct);
//echo "</pre>";
//exit;
?>
@extends('master')
@section('title','bảng Giá')

@section('css')
@stop


@section('header')
@parent
@include('layouts.header')
@stop

@section('content')

<!--================End Main Header Area =================-->
<section class="banner_area">
    <div class="container">
        <div class="banner_text">
            <!--            <h3>Menu</h3>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li><a href="menu.html">Menu</a></li>
                        </ul>-->
        </div>
    </div>
</section>
<!--================End Main Header Area =================-->

<!--================Recipe Menu list Area =================-->
<section class="price_list_area p_100">
    <div class="container">
        <div class="our_bakery_text">
            <h1>{!!$getpost->title_vn!!}</h1>
            {!!$getpost->content_vn!!}
        </div>
    </div>
</section>
<!--================End Main Header Area =================-->
<section class="main_blog_area p_100">
    <div class="container">
        <div class="main_title">
            <h2>Tin Tức Liên Quan</h2>
        </div>
        <div class="blog_area_inner">
            <div class="main_blog_column row">
                @if($getpostrelated)
                @foreach($getpostrelated as $news)
                <div class="col-md-4">
                    <div class="blog_item">
                        <div class="blog_img">
                            <img class="img-fluid" src="{!!url('images/upload/post/'.$news->image)!!}" alt="{!!$news->title_vn!!}">
                        </div>
                        <div class="blog_text">
                            <a href="{!!url('tin-tuc/'.$news->slug_vn)!!}"><h4>{!!$news->title_vn!!}</h4></a>
                            <p>{!!$news->description_vn!!}</p>
                            <a class="pink_more" href="{!!url('tin-tuc/'.$news->slug_vn)!!}">Chi Tiết</a>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </div>
</section>

@stop
@section('scripts')

@stop