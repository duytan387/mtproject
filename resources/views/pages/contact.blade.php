<?php
//echo "<pre>";
//print_r($listProduct);
//echo "</pre>";
//exit;
?>
@extends('master')
@section('title',$getpage->title_vn)
@section('meta')
<meta name="csrf-token" content="{!!csrf_token()!!}" />
@stop
@section('css')
@stop


@section('header')
@parent
@include('layouts.header')
@stop

@section('content')
<!--================End Main Header Area =================-->
<section class="banner_area">
    <div class="container">
        <div class="banner_text">
            <!--            <h3>Contact Us</h3>
                        <ul>
                            <li><a href="index.html">Home</a></li>
                            <li><a href="single-blog.html">Contact Us</a></li>
                        </ul>-->
        </div>
    </div>
</section>
<!--================End Main Header Area =================-->

<!--================Contact Form Area =================-->
<section class="contact_form_area p_100">
    <div class="container">
        <div class="main_title">
            <h1>Hãy Liên Hệ Chúng Tôi Thông Qua Form Đăng Ký Bên Dưới</h1>
        </div>
        <div class="row">
            <div class="col-lg-7">
                <form class="row contact_us_form" action="{!!action('ContactController@create')!!}" method="post" id="contactForm" novalidate="novalidate">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <div class="form-group col-md-12">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Họ Tên">
                    </div>
                    <div class="form-group col-md-6">
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Điện Thoại">
                    </div>
                    <div class="form-group col-md-6">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                    </div>
                    <div class="form-group col-md-12">
                        <input type="text" class="form-control" id="subject" name="subject" placeholder="Tiêu Đề">
                    </div>
                    <div class="form-group col-md-12">
                        <textarea class="form-control" name="message" id="message" rows="1" placeholder="Nội Dụng"></textarea>
                    </div>
                    <div class="form-group col-md-12">
                        <button type="submit" value="submit" class="btn order_s_btn form-control">Gửi</button>
                    </div>
                    <div class="alert"></div>
                    <div class="error"></div>
                </form>
            </div>
            <div class="col-lg-4 offset-md-1">
                <div class="contact_details">
                    <div class="contact_d_item">
                        <h3>Địa Chỉ :</h3>
                        <p>Số 730/2F Hương Lộ 2, Phường Bình Trị Đông A, Quận Bình Tân, TP.HCM</p>
                    </div>
                    <div class="contact_d_item">
                        <h5>Hotline : <a href="tel:0917966067">091 796 6067</a> (Ms. Phụng)</h5>
						<h5>Hoặc : <a href="tel:0967931680">0967 931 680</a> (Ms. Nga)</h5>
                        <h5>Email : <a href="mailto:cty.longquockim@gmail.com">cty.longquockim@gmail.com</a>&nbsp;</h5>
                    </div>
                    <div class="contact_d_item">
                        <h3>Giờ Làm Việc</h3>
                        <p>8:00 AM – 7:00 PM</p>
                        <p>Thứ 2 đến Thứ 7</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End Contact Form Area =================-->

<!--================End Banner Area =================-->
<section class="map_area">
    <div id="mapBox">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1385.7616046751234!2d106.60796253164415!3d10.770989153535764!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752c3fc8301a99%3A0xb5f02918fad72d56!2zU-G7kSA3MzAsIDY4OC8yLzEvODIgSEwyLCBCw6xuaCBUcuG7iyDEkMO0bmcsIELDrG5oIFTDom4sIEjhu5MgQ2jDrSBNaW5oLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1563593202455!5m2!1svi!2s" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
</section>
<!--================End Banner Area =================-->

@stop
@section('scripts')
<!-- contact js -->
<!--<script src="{!!url('js/jquery.form.js')!!}"></script>-->
<!--<script src="{!!url('js/jquery.validate.min.js')!!}"></script>-->
<!--<script src="{!!url('js/contact.js')!!}"></script>-->
<script>
    var j = jQuery;
    j(document).ready(function () {
        j('#contactForm').submit(function (e) {
            j('.error').text('');
            j('.alert').text('');
            e.preventDefault();
            j.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': j('meta[name="csrf-token"]').attr('content')
                }
            });
            j.ajax({
                url: "{!!action('ContactController@create')!!}",
                method: 'post',
                data: {
                    name: j('#name').val(),
                    email: j('#email').val(),
                    phone: j('#phone').val(),
                    subject: j('#subject').val(),
                    message: j('#message').val()
                },
                success: function (data) {
                    j("#contactForm").trigger('reset');
                    if (j.type(data.result) === "object") {
                        j.each(data.result, function (i, v) {
                            j('.error').append('<p>*' + v + '</p>');
//                                alert(v);
//                                console.log(v);
                        });
                        if (data.hoten != '') {
                            j('#name').val(data.hoten);
                        }
                        if (data.phone != '') {
                            j('#phone').val(data.phone);
                        }
                        if (data.message != '') {
                            j('#message').val(data.message);
                        }
                        if (data.email != '') {
                            j('#email').val(data.email);
                        }
                        if (data.subject != '') {
                            j('#subject').val(data.subject);
                        }
                        j('.alert').html('');
                    } else {
                        j('.alert').html(data.result);
                        j('.error').html('');
                    }
                },
                fail: function (data) {
                    j('.alert').html(data.fail)
                }
            });
        });
    });
</script>

@stop