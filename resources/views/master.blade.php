<!DOCTYPE html>
<html lang="vi_VN">
    <head>
        <!-- PAGE TITLE HERE -->
        <title>@yield('title')</title>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="icon" href="{!!url('/img/favicon.jpg')!!}" type="image/x-icon" />

        <!-- MOBILE SPECIFIC -->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

        @section('meta')
        @show



        
        <!-- Icon css link -->
        <link href="{!!url('css/font-awesome.min.css')!!}" rel="stylesheet">
        <link href="{!!url('vendors/linearicons/style.css')!!}" rel="stylesheet">
        <link href="{!!url('vendors/flat-icon/flaticon.css')!!}" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="{!!url('css/bootstrap.min.css')!!}" rel="stylesheet">

        <!-- Rev slider css -->
        <link href="{!!url('vendors/revolution/css/settings.css')!!}" rel="stylesheet">
        <link href="{!!url('vendors/revolution/css/layers.css')!!}" rel="stylesheet">
        <link href="{!!url('vendors/revolution/css/navigation.css')!!}" rel="stylesheet">
        <link href="{!!url('vendors/animate-css/animate.css')!!}" rel="stylesheet">

        <!-- Extra plugin css -->
        <link href="{!!url('vendors/owl-carousel/owl.carousel.min.css')!!}" rel="stylesheet">
        <link href="{!!url('vendors/magnifc-popup/magnific-popup.css')!!}" rel="stylesheet">
        <link href="{!!url('css/style.css')!!}" rel="stylesheet">
        <link href="{!!url('css/custom.css')!!}" rel="stylesheet">
        <link href="{!!url('css/responsive.css')!!}" rel="stylesheet">

        @section('css')
        @show

        <!--*********************************************************************************************-->
        <!-- Developed by JTECK.net                                                                      -->
        <!-- Address: 69/26 Nguyễn Cửu Đàm, Phường Tân Sơn Nhì, Quận Tân Phú, Thành phố Hồ Chí Minh      -->
        <!-- Phone: (+84) 90 6289 587                                                                    -->
        <!--*********************************************************************************************-->


        <!-- META -->
        @section('tracking')
        @show

    </head>

    <body>
        @section('header')@show
        @section('banner')@show



        @yield('content')

        @include('layouts.footer')


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="{!!url('js/jquery-3.2.1.min.js')!!}"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="{!!url('js/popper.min.js')!!}"></script>
        <script src="{!!url('js/bootstrap.min.js')!!}"></script>
        <!-- Rev slider js -->
        <script src="{!!url('vendors/revolution/js/jquery.themepunch.tools.min.js')!!}"></script>
        <script src="{!!url('vendors/revolution/js/jquery.themepunch.revolution.min.js')!!}"></script>
        <script src="{!!url('vendors/revolution/js/extensions/revolution.extension.actions.min.js')!!}"></script>
        <script src="{!!url('vendors/revolution/js/extensions/revolution.extension.video.min.js')!!}"></script>
        <script src="{!!url('vendors/revolution/js/extensions/revolution.extension.slideanims.min.js')!!}"></script>
        <script src="{!!url('vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js')!!}"></script>
        <script src="{!!url('vendors/revolution/js/extensions/revolution.extension.navigation.min.js')!!}"></script>
        <!-- Extra plugin js -->
        <script src="{!!url('vendors/owl-carousel/owl.carousel.min.js')!!}"></script>
        <script src="{!!url('vendors/magnifc-popup/jquery.magnific-popup.min.js')!!}"></script>
        <script src="{!!url('vendors/datetime-picker/js/moment.min.js')!!}"></script>
        <script src="{!!url('vendors/datetime-picker/js/bootstrap-datetimepicker.min.js')!!}"></script>
        <script src="{!!url('vendors/nice-select/js/jquery.nice-select.min.js')!!}"></script>
        <script src="{!!url('vendors/jquery-ui/jquery-ui.min.js')!!}"></script>
        <script src="{!!url('vendors/lightbox/simpleLightbox.min.js')!!}"></script>

        @section('scripts')
        @show
        <script src="{!!url('js/theme.js')!!}"></script>
    </body>
</html>
