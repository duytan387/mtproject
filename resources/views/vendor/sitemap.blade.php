<?php 
echo '<?xml version="1.0" encoding="UTF-8"?>';
echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
    foreach($pages as $count => $page){
		echo '<url>';
        echo '<loc>'.url($page->slug_vn).'</loc>';
		echo '<lastmod>'.date("Y-m-d",strtotime($page->created_at)).'T'.date("h:i:s",strtotime($page->created_at)).'+07:00</lastmod>';
		echo '<changefreq>monthly</changefreq>';
		echo '<priority>';
		if($count == 0){
			echo '1.00';
		}else{
			echo '0.8';
		}
		echo '</priority>';
		echo '</url>';
    }
    foreach($posts as $post){
        echo '<url>';
        echo '<loc>'.url($post->slug_vn).'</loc>';
		echo '<lastmod>'.date("Y-m-d",strtotime($post->created_at)).'T'.date("h:i:s",strtotime($post->created_at)).'+07:00</lastmod>';
		echo '<changefreq>daily</changefreq>';
		echo '<priority>0.9</priority>';
		echo '</url>';
    }
echo '</urlset>';
?>