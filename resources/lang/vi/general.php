<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'lang_en' => 'Tiếng Anh',
    'lang_vi' => 'Tiếng Việt',
    'name'=>'Tên',
    'type'=>'Loại',
    'name_type'=>'Tên Loại',
    'sort'=>'Sắp xếp',
    'province'=>'Tỉnh thành'

];
